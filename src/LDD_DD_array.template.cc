

// PKG_ADD: autoload ("GSL_OCTAVE_NAME", which ("gsl_sf"));
DEFUN_DLD(GSL_OCTAVE_NAME, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {( z1, z2 ) =} GSL_OCTAVE_NAME (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {( z1, z2 ) =} GSL_OCTAVE_NAME (@dots{})\n\
\n\
GSL_FUNC_DOCSTRING
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
DEPRECATION_WARNING
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_FUNC

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);
  RowVector y2 (arg1+1);

  // Run the calculation
  GSL_FUNC_NAME (arg1, arg2, arg3, y1.fortran_vec (), y2.fortran_vec ());

  octave_value_list retval;
  retval(0) = octave_value (y1);
  retval(1) = octave_value (y2);
  return retval;

#else // HAVE_GSL_FUNC undefined

  error ("GSL function GSL_FUNC_NAME was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_FUNC
}
