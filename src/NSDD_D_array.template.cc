

// PKG_ADD: autoload ("GSL_OCTAVE_NAME", which ("gsl_sf"));
DEFUN_DLD(GSL_OCTAVE_NAME, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} GSL_OCTAVE_NAME (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {@var{z} =} GSL_OCTAVE_NAME (@dots{})\n\
\n\
GSL_FUNC_DOCSTRING
\n\
The argument @var{arg1} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_SF_LEGENDRE_SCHMIDT\n\
      Computation of the Schmidt semi-normalized associated Legendre\n\
      polynomials S_l^m(x).\n\
@item 1 = GSL_SF_LEGENDRE_SPHARM\n\
      Computation of the spherical harmonic associated Legendre\n\
      polynomials Y_l^m(x).\n\
@item 2 = GSL_SF_LEGENDRE_FULL\n\
      Computation of the fully normalized associated Legendre\n\
      polynomials N_l^m(x).\n\
@item 3 = GSL_SF_LEGENDRE_NONE\n\
      Computation of the unnormalized associated Legendre polynomials\n\
      P_l^m(x).\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
DEPRECATION_WARNING
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_FUNC

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the Legendre normalization parameter is valid
  double legnorm_tmp = args(0).double_value ();
  gsl_sf_legendre_t legnorm;
  if (legnorm_tmp == 0.0)
    legnorm = GSL_SF_LEGENDRE_SCHMIDT;
  else if (legnorm_tmp == 1.0)
    legnorm = GSL_SF_LEGENDRE_SPHARM;
  else if (legnorm_tmp == 2.0)
    legnorm = GSL_SF_LEGENDRE_FULL;
  else if (legnorm_tmp == 3.0)
    legnorm = GSL_SF_LEGENDRE_NONE;
  else
   {
      error ("The 'legnorm' argument must be 0, 1, 2 or 3.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<size_t>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type size_t: %lu.", std::numeric_limits<size_t>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  size_t arg2 = static_cast<size_t> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Get the value of input argument #4
  double arg4 = args(3).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (gsl_sf_legendre_array_n (arg2));

  // Run the calculation
  GSL_FUNC_NAME (legnorm, arg2, arg3, arg4, y1.fortran_vec ());

  y1.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  return octave_value (y1);

#else // HAVE_GSL_FUNC undefined

  error ("GSL function GSL_FUNC_NAME was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_FUNC
}
