## Copyright (C) 2016, 2017 Julien Bect <jbect@users.sourceforge.net>
## Copyright (C) 2016 Susi Lehtola
## Copyright (C) 2004 Teemu Ikonen <tpikonen@pcu.helsinki.fi>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.

RT="./replace_template.sh"

cp gsl_sf.header.cc gsl_sf.cc

# double to double ###################################################

export octave_name=gsl_sf_clausen
export    funcname=gsl_sf_clausen_e
cat << \EOF > docstring.txt
The Clausen function is defined by the following integral,

Cl_2(x) = - \int_0^x dt \log(2 \sin(t/2))

It is related to the dilogarithm by Cl_2(\theta) = \Im Li_2(\exp(i \theta)).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=clausen
export    funcname=gsl_sf_clausen_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_dawson
export    funcname=gsl_sf_dawson_e
cat << \EOF > docstring.txt
The Dawson integral is defined by \exp(-x^2) \int_0^x dt \exp(t^2).
A table of Dawson integral can be found in Abramowitz & Stegun, Table 7.5.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=dawson
export    funcname=gsl_sf_dawson_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_1
export    funcname=gsl_sf_debye_1_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=debye_1
export    funcname=gsl_sf_debye_1_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_2
export    funcname=gsl_sf_debye_2_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=debye_2
export    funcname=gsl_sf_debye_2_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_3
export    funcname=gsl_sf_debye_3_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=debye_3
export    funcname=gsl_sf_debye_3_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_4
export    funcname=gsl_sf_debye_4_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=debye_4
export    funcname=gsl_sf_debye_4_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_5
export    funcname=gsl_sf_debye_5_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_debye_6
export    funcname=gsl_sf_debye_6_e
cat << \EOF > docstring.txt
The Debye functions are defined by the integral

D_n(x) = n/x^n \int_0^x dt (t^n/(e^t - 1)).

For further information see Abramowitz & Stegun, Section 27.1.
EOF
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_dilog
export    funcname=gsl_sf_dilog_e
cat << \EOF > docstring.txt
Computes the dilogarithm for a real argument.
In Lewin’s notation this is Li_2(x), the real part of the
dilogarithm of a real x. It is defined by the integral
representation

 Li_2(x) = - \Re \int_0^x ds \log(1-s) / s.

Note that \Im(Li_2(x)) = 0 for x <= 1, and -\pi\log(x)
for x > 1.

Note that Abramowitz & Stegun refer to the Spence integral
S(x)=Li_2(1-x) as the dilogarithm rather than Li_2(x).
EOF
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_erf
export    funcname=gsl_sf_erf_e
cat << \EOF > docstring.txt
Computes the error function
erf(x) = (2/\sqrt(\pi)) \int_0^x dt \exp(-t^2).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=erf_gsl
export    funcname=gsl_sf_erf_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_erfc
export    funcname=gsl_sf_erfc_e
cat << \EOF > docstring.txt
Computes the complementary error function
erfc(x) = 1 - erf(x) = (2/\sqrt(\pi)) \int_x^\infty \exp(-t^2).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=erfc_gsl
export    funcname=gsl_sf_erfc_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_log_erfc
export    funcname=gsl_sf_log_erfc_e
cat << \EOF > docstring.txt
Computes the logarithm of the complementary error
function \log(\erfc(x)).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=log_erfc
export    funcname=gsl_sf_log_erfc_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_erf_Z
export    funcname=gsl_sf_erf_Z_e
cat << \EOF > docstring.txt
Computes the Gaussian probability function
Z(x) = (1/(2\pi)) \exp(-x^2/2).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=erf_Z
export    funcname=gsl_sf_erf_Z_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_erf_Q
export    funcname=gsl_sf_erf_Q_e
cat << \EOF > docstring.txt
Computes the upper tail of the Gaussian probability
function  Q(x) = (1/(2\pi)) \int_x^\infty dt \exp(-t^2/2).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=erf_Q
export    funcname=gsl_sf_erf_Q_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_hazard
export    funcname=gsl_sf_hazard_e
cat << \EOF > docstring.txt
The hazard function for the normal distrbution, also known as the
inverse Mill\'s ratio, is defined as
h(x) = Z(x)/Q(x) = \sqrt@{2/\pi \exp(-x^2 / 2) / \erfc(x/\sqrt 2)@}.
It decreases rapidly as x approaches -\infty and asymptotes to
h(x) \sim x as x approaches +\infty.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hazard
export    funcname=gsl_sf_hazard_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_expm1
export    funcname=gsl_sf_expm1_e
cat << \EOF > docstring.txt
Computes the quantity \exp(x)-1 using an algorithm that
is accurate for small x.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=expm1
export    funcname=gsl_sf_expm1_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_exprel
export    funcname=gsl_sf_exprel_e
cat << \EOF > docstring.txt
Computes the quantity (\exp(x)-1)/x using an algorithm
that is accurate for small x. For small x the algorithm is based on
the expansion (\exp(x)-1)/x = 1 + x/2 + x^2/(2*3) + x^3/(2*3*4) + \dots.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=exprel
export    funcname=gsl_sf_exprel_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_exprel_2
export    funcname=gsl_sf_exprel_2_e
cat << \EOF > docstring.txt
Computes the quantity 2(\exp(x)-1-x)/x^2 using an
algorithm that is accurate for small x. For small x the algorithm is
based on the expansion
2(\exp(x)-1-x)/x^2 = 1 + x/3 + x^2/(3*4) + x^3/(3*4*5) + \dots.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=exprel_2
export    funcname=gsl_sf_exprel_2_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_expint_E1
export    funcname=gsl_sf_expint_E1_e
cat << \EOF > docstring.txt
Computes the exponential integral E_1(x),

E_1(x) := Re \int_1^\infty dt \exp(-xt)/t.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=expint_E1
export    funcname=gsl_sf_expint_E1_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_expint_E2
export    funcname=gsl_sf_expint_E2_e
cat << \EOF > docstring.txt
Computes the second-order exponential integral E_2(x),

E_2(x) := \Re \int_1^\infty dt \exp(-xt)/t^2.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=expint_E2
export    funcname=gsl_sf_expint_E2_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_expint_Ei
export    funcname=gsl_sf_expint_Ei_e
cat << \EOF > docstring.txt
Computes the exponential integral E_i(x),

Ei(x) := - PV(\int_@{-x@}^\infty dt \exp(-t)/t)

where PV denotes the principal value of the integral.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=expint_Ei
export    funcname=gsl_sf_expint_Ei_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_Shi
export    funcname=gsl_sf_Shi_e
cat << \EOF > docstring.txt
Computes the integral Shi(x) = \int_0^x dt \sinh(t)/t.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=Shi
export    funcname=gsl_sf_Shi_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_Chi
export    funcname=gsl_sf_Chi_e
cat << \EOF > docstring.txt
Computes the integral 

Chi(x) := Re[ \gamma_E + \log(x) + \int_0^x dt (\cosh[t]-1)/t] ,

where \gamma_E is the Euler constant.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=Chi
export    funcname=gsl_sf_Chi_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_expint_3
export    funcname=gsl_sf_expint_3_e
cat << \EOF > docstring.txt
Computes the exponential integral
Ei_3(x) = \int_0^x dt \exp(-t^3) for x >= 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=expint_3
export    funcname=gsl_sf_expint_3_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_Si
export    funcname=gsl_sf_Si_e
cat << \EOF > docstring.txt
Computes the Sine integral Si(x) = \int_0^x dt \sin(t)/t.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=Si
export    funcname=gsl_sf_Si_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_Ci
export    funcname=gsl_sf_Ci_e
cat << \EOF > docstring.txt
Computes the Cosine integral
Ci(x) = -\int_x^\infty dt \cos(t)/t for x > 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=Ci
export    funcname=gsl_sf_Ci_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_atanint
export    funcname=gsl_sf_atanint_e
cat << \EOF > docstring.txt
Computes the Arctangent integral
AtanInt(x) = \int_0^x dt \arctan(t)/t.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=atanint
export    funcname=gsl_sf_atanint_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_fermi_dirac_mhalf
export    funcname=gsl_sf_fermi_dirac_mhalf_e
cat << \EOF > docstring.txt
Computes the complete Fermi-Dirac integral F_@{-1/2@}(x).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=fermi_dirac_mhalf
export    funcname=gsl_sf_fermi_dirac_mhalf_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_fermi_dirac_half
export    funcname=gsl_sf_fermi_dirac_half_e
cat << \EOF > docstring.txt
Computes the complete Fermi-Dirac integral F_@{1/2@}(x).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=fermi_dirac_half
export    funcname=gsl_sf_fermi_dirac_half_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsm_sf_fermi_dirac_3half
export    funcname=gsl_sf_fermi_dirac_3half_e
cat << \EOF > docstring.txt
Computes the complete Fermi-Dirac integral F_@{3/2@}(x).
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=fermi_dirac_3half
export    funcname=gsl_sf_fermi_dirac_3half_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gamma
export    funcname=gsl_sf_gamma_e
cat << \EOF > docstring.txt
Computes the Gamma function \Gamma(x), subject to x not
being a negative integer. The function is computed using the real
Lanczos method. The maximum value of x such that \Gamma(x) is not
considered an overflow is given by the macro GSL_SF_GAMMA_XMAX and is 171.0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gamma_gsl
export    funcname=gsl_sf_gamma_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lngamma
export    funcname=gsl_sf_lngamma_e
cat << \EOF > docstring.txt
Computes the logarithm of the Gamma function,
\log(\Gamma(x)), subject to x not a being negative integer.
For x<0 the real part of \log(\Gamma(x)) is returned, which is
equivalent to \log(|\Gamma(x)|). The function is computed using
the real Lanczos method.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lngamma_gsl
export    funcname=gsl_sf_lngamma_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gammastar
export    funcname=gsl_sf_gammastar_e
cat << \EOF > docstring.txt
Computes the regulated Gamma Function \Gamma^*(x)
for x > 0. The regulated gamma function is given by,

\Gamma^*(x) = \Gamma(x)/(\sqrt@{2\pi@} x^@{(x-1/2)@} \exp(-x))
            = (1 + (1/12x) + ...)  for x \to \infty

and is a useful suggestion of Temme.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gammastar
export    funcname=gsl_sf_gammastar_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gammainv
export    funcname=gsl_sf_gammainv_e
cat << \EOF > docstring.txt
Computes the reciprocal of the gamma function, 1/\Gamma(x) using the real Lanczos method.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gammainv_gsl
export    funcname=gsl_sf_gammainv_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lambert_W0
export    funcname=gsl_sf_lambert_W0_e
cat << \EOF > docstring.txt
These compute the principal branch of the Lambert W function, W_0(x).

Lambert\'s W functions, W(x), are defined to be solutions of the
export octave_name=gammastar
export    funcname=gsl_sf_gammastar_e
equation W(x) \exp(W(x)) = x. This function has multiple branches
for x < 0; however, it has only two real-valued branches.
We define W_0(x) to be the principal branch, where W > -1 for x < 0,
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lambert_W0
export    funcname=gsl_sf_lambert_W0_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lambert_Wm1
export    funcname=gsl_sf_lambert_Wm1_e
cat << \EOF > docstring.txt
These compute the secondary real-valued branch of the Lambert
W function, W_@{-1@}(x).

Lambert\'s W functions, W(x), are defined to be solutions of the
equation W(x) \exp(W(x)) = x. This function has multiple branches
for x < 0; however, it has only two real-valued branches.
We define W_0(x) to be the principal branch, where W > -1 for x < 0,
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lambert_Wm1
export    funcname=gsl_sf_lambert_Wm1_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_log_1plusx
export    funcname=gsl_sf_log_1plusx_e
cat << \EOF > docstring.txt
Computes \log(1 + x) for x > -1 using an algorithm that
is accurate for small x.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=log_1plusx
export    funcname=gsl_sf_log_1plusx_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_log_1plusx_mx
export    funcname=gsl_sf_log_1plusx_mx_e
cat << \EOF > docstring.txt
Computes \log(1 + x) - x for x > -1 using an algorithm
that is accurate for small x.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=log_1plusx_mx
export    funcname=gsl_sf_log_1plusx_mx_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_psi
export    funcname=gsl_sf_psi_e
cat << \EOF > docstring.txt
Computes the digamma function \psi(x) for general x,
x \ne 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=psi
export    funcname=gsl_sf_psi_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_psi_1piy
export    funcname=gsl_sf_psi_1piy_e
cat << \EOF > docstring.txt
Computes the real part of the digamma function on
the line 1+i y, Re[\psi(1 + i y)].
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=psi_1piy
export    funcname=gsl_sf_psi_1piy_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_synchrotron_1
export    funcname=gsl_sf_synchrotron_1_e
cat << \EOF > docstring.txt
Computes the first synchrotron function
x \int_x^\infty dt K_@{5/3@}(t) for x >= 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=synchrotron_1
export    funcname=gsl_sf_synchrotron_1_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_synchrotron_2
export    funcname=gsl_sf_synchrotron_2_e
cat << \EOF > docstring.txt
Computes the second synchrotron function
x K_@{2/3@}(x) for x >= 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=synchrotron_2
export    funcname=gsl_sf_synchrotron_2_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_transport_2
export    funcname=gsl_sf_transport_2_e
cat << \EOF > docstring.txt
Computes the transport function J(2,x).

The transport functions J(n,x) are defined by the integral
representations J(n,x) := \int_0^x dt t^n e^t /(e^t - 1)^2.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=transport_2
export    funcname=gsl_sf_transport_2_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_transport_3
export    funcname=gsl_sf_transport_3_e
cat << \EOF > docstring.txt
Computes the transport function J(3,x).

The transport functions J(n,x) are defined by the integral
representations J(n,x) := \int_0^x dt t^n e^t /(e^t - 1)^2.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=transport_3
export    funcname=gsl_sf_transport_3_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_transport_4
export    funcname=gsl_sf_transport_4_e
cat << \EOF > docstring.txt
Computes the transport function J(4,x).

The transport functions J(n,x) are defined by the integral
representations J(n,x) := \int_0^x dt t^n e^t /(e^t - 1)^2.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=transport_4
export    funcname=gsl_sf_transport_4_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_transport_5
export    funcname=gsl_sf_transport_5_e
cat << \EOF > docstring.txt
Computes the transport function J(5,x).

The transport functions J(n,x) are defined by the integral
representations J(n,x) := \int_0^x dt t^n e^t /(e^t - 1)^2.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=transport_5
export    funcname=gsl_sf_transport_5_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_sinc
export    funcname=gsl_sf_sinc_e
cat << \EOF > docstring.txt
Computes \sinc(x) = \sin(\pi x) / (\pi x) for any value of x.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=sinc_gsl
export    funcname=gsl_sf_sinc_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lnsinh
export    funcname=gsl_sf_lnsinh_e
cat << \EOF > docstring.txt
Computes \log(\sinh(x)) for x > 0.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lnsinh
export    funcname=gsl_sf_lnsinh_e
${RT} D_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lncosh
export    funcname=gsl_sf_lncosh_e
cat << \EOF > docstring.txt
Computes \log(\cosh(x)) for any x.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lncosh
export    funcname=gsl_sf_lncosh_e
${RT} D_D >> gsl_sf.cc


# (int, double) to double ############################################

export octave_name=gsl_sf_bessel_Jn
export    funcname=gsl_sf_bessel_Jn_e
cat << \EOF > docstring.txt
Computes the regular cylindrical Bessel function of
order n, J_n(x).
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Jn
export    funcname=gsl_sf_bessel_Jn_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Yn
export    funcname=gsl_sf_bessel_Yn_e
cat << \EOF > docstring.txt
Computes the irregular cylindrical Bessel function of
order n, Y_n(x), for x>0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Yn
export    funcname=gsl_sf_bessel_Yn_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_In
export    funcname=gsl_sf_bessel_In_e
cat << \EOF > docstring.txt
Computes the regular modified cylindrical Bessel
function of order n, I_n(x).
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_In
export    funcname=gsl_sf_bessel_In_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_In_scaled
export    funcname=gsl_sf_bessel_In_scaled_e
cat << \EOF > docstring.txt
Computes the scaled regular modified cylindrical Bessel
function of order n, \exp(-|x|) I_n(x)
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_In_scaled
export    funcname=gsl_sf_bessel_In_scaled_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Kn
export    funcname=gsl_sf_bessel_Kn_e
cat << \EOF > docstring.txt
Computes the irregular modified cylindrical Bessel
function of order n, K_n(x), for x > 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Kn
export    funcname=gsl_sf_bessel_Kn_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Kn_scaled
export    funcname=gsl_sf_bessel_Kn_scaled_e
cat << \EOF > docstring.txt

EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Kn_scaled
export    funcname=gsl_sf_bessel_Kn_scaled_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_jl
export    funcname=gsl_sf_bessel_jl_e
cat << \EOF > docstring.txt
Computes the regular spherical Bessel function of
order l, j_l(x), for l >= 0 and x >= 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_jl
export    funcname=gsl_sf_bessel_jl_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_yl
export    funcname=gsl_sf_bessel_yl_e
cat << \EOF > docstring.txt
Computes the irregular spherical Bessel function of
order l, y_l(x), for l >= 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_yl
export    funcname=gsl_sf_bessel_yl_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_il_scaled
export    funcname=gsl_sf_bessel_il_scaled_e
cat << \EOF > docstring.txt
Computes the scaled regular modified spherical Bessel
function of order l, \exp(-|x|) i_l(x)
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_il_scaled
export    funcname=gsl_sf_bessel_il_scaled_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_kl_scaled
export    funcname=gsl_sf_bessel_kl_scaled_e
cat << \EOF > docstring.txt
Computes the scaled irregular modified spherical Bessel
function of order l, \exp(x) k_l(x), for x>0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_kl_scaled
export    funcname=gsl_sf_bessel_kl_scaled_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_exprel_n
export    funcname=gsl_sf_exprel_n_e
cat << \EOF > docstring.txt
Computes the N-relative exponential, which is the n-th
generalization of the functions gsl_sf_exprel and gsl_sf_exprel2. The
N-relative exponential is given by,

exprel_N(x) = N!/x^N (\exp(x) - \sum_@{k=0@}^@{N-1@} x^k/k!)
            = 1 + x/(N+1) + x^2/((N+1)(N+2)) + ...
            = 1F1 (1,1+N,x)
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=exprel_n
export    funcname=gsl_sf_exprel_n_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_fermi_dirac_int
export    funcname=gsl_sf_fermi_dirac_int_e
cat << \EOF > docstring.txt
Computes the complete Fermi-Dirac integral with an
integer index of j, F_j(x) = (1/\Gamma(j+1)) \int_0^\infty dt (t^j
/(\exp(t-x)+1)).
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=fermi_dirac_int
export    funcname=gsl_sf_fermi_dirac_int_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_taylorcoeff
export    funcname=gsl_sf_taylorcoeff_e
cat << \EOF > docstring.txt
Computes the Taylor coefficient x^n / n!
for x >= 0, n >= 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=taylorcoeff
export    funcname=gsl_sf_taylorcoeff_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_Pl
export    funcname=gsl_sf_legendre_Pl_e
cat << \EOF > docstring.txt
These functions evaluate the Legendre polynomial P_l(x) for a specific
value of l, x subject to l >= 0, |x| <= 1
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=legendre_Pl
export    funcname=gsl_sf_legendre_Pl_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_Ql
export    funcname=gsl_sf_legendre_Ql_e
cat << \EOF > docstring.txt
Computes the Legendre function Q_l(x) for x > -1, x != 1
and l >= 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=legendre_Ql
export    funcname=gsl_sf_legendre_Ql_e
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_a
export    funcname=gsl_sf_mathieu_a_e
cat << \EOF > docstring.txt
Computes the characteristic values a_n(q) of the
Mathieu function ce_n(q,x).
EOF
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_b
export    funcname=gsl_sf_mathieu_b_e
cat << \EOF > docstring.txt
Computes the characteristic values b_n(q) of the
Mathieu function se_n(q,x).
EOF
${RT} ID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_psi_n
export    funcname=gsl_sf_psi_n_e
cat << \EOF > docstring.txt
Computes the polygamma function \psi^@{(m)@}(x)
for m >= 0, x > 0.
EOF
${RT} ID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=psi_n
export    funcname=gsl_sf_psi_n_e
${RT} ID_D >> gsl_sf.cc


# (double, double) to double #########################################

export octave_name=gsl_sf_bessel_Jnu
export    funcname=gsl_sf_bessel_Jnu_e
cat << \EOF > docstring.txt
Computes the regular cylindrical Bessel function of
fractional order nu, J_\nu(x).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Jnu
export    funcname=gsl_sf_bessel_Jnu_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Ynu
export    funcname=gsl_sf_bessel_Ynu_e
cat << \EOF > docstring.txt
Computes the irregular cylindrical Bessel function of
fractional order nu, Y_\nu(x).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Ynu
export    funcname=gsl_sf_bessel_Ynu_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Inu
export    funcname=gsl_sf_bessel_Inu_e
cat << \EOF > docstring.txt
Computes the regular modified Bessel function of
fractional order nu, I_\nu(x) for x>0, \nu>0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Inu
export    funcname=gsl_sf_bessel_Inu_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Inu_scaled
export    funcname=gsl_sf_bessel_Inu_scaled_e
cat << \EOF > docstring.txt
Computes the scaled regular modified Bessel function of
fractional order nu, \exp(-|x|)I_\nu(x) for x>0, \nu>0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Inu_scaled
export    funcname=gsl_sf_bessel_Inu_scaled_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Knu
export    funcname=gsl_sf_bessel_Knu_e
cat << \EOF > docstring.txt
Computes the irregular modified Bessel function of
fractional order nu, K_\nu(x) for x>0, \nu>0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Knu
export    funcname=gsl_sf_bessel_Knu_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_lnKnu
export    funcname=gsl_sf_bessel_lnKnu_e
cat << \EOF > docstring.txt
Computes the logarithm of the irregular modified Bessel
function of fractional order nu, \ln(K_\nu(x)) for x>0, \nu>0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_lnKnu
export    funcname=gsl_sf_bessel_lnKnu_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Knu_scaled
export    funcname=gsl_sf_bessel_Knu_scaled_e
cat << \EOF > docstring.txt
Computes the scaled irregular modified Bessel function
of fractional order nu, \exp(+|x|) K_\nu(x) for x>0, \nu>0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_Knu_scaled
export    funcname=gsl_sf_bessel_Knu_scaled_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_exp_mult
export    funcname=gsl_sf_exp_mult_e
cat << \EOF > docstring.txt
These routines exponentiate x and multiply by the factor y to return
the product y \exp(x).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=exp_mult
export    funcname=gsl_sf_exp_mult_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_fermi_dirac_inc_0
export    funcname=gsl_sf_fermi_dirac_inc_0_e
cat << \EOF > docstring.txt
Computes the incomplete Fermi-Dirac integral with an
index of zero, F_0(x,b) = \ln(1 + e^@{b-x@}) - (b-x).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=fermi_dirac_inc_0
export    funcname=gsl_sf_fermi_dirac_inc_0_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_poch
export    funcname=gsl_sf_poch_e
cat << \EOF > docstring.txt
Computes the Pochhammer symbol

(a)_x := \Gamma(a + x)/\Gamma(a),

subject to a and a+x not being negative integers. The Pochhammer
symbol is also known as the Apell symbol.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=poch
export    funcname=gsl_sf_poch_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lnpoch
export    funcname=gsl_sf_lnpoch_e
cat << \EOF > docstring.txt
Computes the logarithm of the Pochhammer symbol,
\log((a)_x) = \log(\Gamma(a + x)/\Gamma(a)) for a > 0, a+x > 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lnpoch
export    funcname=gsl_sf_lnpoch_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_pochrel
export    funcname=gsl_sf_pochrel_e
cat << \EOF > docstring.txt
Computes the relative Pochhammer symbol ((a,x) - 1)/x
where (a,x) = (a)_x := \Gamma(a + x)/\Gamma(a).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=pochrel
export    funcname=gsl_sf_pochrel_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gamma_inc_Q
export    funcname=gsl_sf_gamma_inc_Q_e
cat << \EOF > docstring.txt
Computes the normalized incomplete Gamma Function
Q(a,x) = 1/\Gamma(a) \int_x\infty dt t^@{a-1@} \exp(-t) for a > 0, x >= 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gamma_inc_Q
export    funcname=gsl_sf_gamma_inc_Q_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gamma_inc_P
export    funcname=gsl_sf_gamma_inc_P_e
cat << \EOF > docstring.txt
Computes the complementary normalized incomplete Gamma
Function P(a,x) = 1/\Gamma(a) \int_0^x dt t^@{a-1@} \exp(-t)
for a > 0, x >= 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gamma_inc_P
export    funcname=gsl_sf_gamma_inc_P_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gamma_inc
export    funcname=gsl_sf_gamma_inc_e
cat << \EOF > docstring.txt
These functions compute the incomplete Gamma Function the
normalization factor included in the previously defined functions:
\Gamma(a,x) = \int_x\infty dt t^@{a-1@} \exp(-t) for a real and x >= 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=gamma_inc
export    funcname=gsl_sf_gamma_inc_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_beta
export    funcname=gsl_sf_beta_e
cat << \EOF > docstring.txt
Computes the Beta Function,
B(a,b) = \Gamma(a)\Gamma(b)/\Gamma(a+b) for a > 0, b > 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=beta_gsl
export    funcname=gsl_sf_beta_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lnbeta
export    funcname=gsl_sf_lnbeta_e
cat << \EOF > docstring.txt
Computes the logarithm of the Beta Function,
\log(B(a,b)) for a > 0, b > 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=lnbeta
export    funcname=gsl_sf_lnbeta_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_hyperg_0F1
export    funcname=gsl_sf_hyperg_0F1_e
cat << \EOF > docstring.txt
Computes the hypergeometric function 0F1(c,x).
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hyperg_0F1
export    funcname=gsl_sf_hyperg_0F1_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_conicalP_half
export    funcname=gsl_sf_conicalP_half_e
cat << \EOF > docstring.txt
Computes the irregular Spherical Conical Function
P^@{1/2@}_@{-1/2 + i \lambda@}(x) for x > -1.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=conicalP_half
export    funcname=gsl_sf_conicalP_half_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_conicalP_mhalf
export    funcname=gsl_sf_conicalP_mhalf_e
cat << \EOF > docstring.txt
Computes the regular Spherical Conical Function
P^@{-1/2@}_@{-1/2 + i \lambda@}(x) for x > -1.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=conicalP_mhalf
export    funcname=gsl_sf_conicalP_mhalf_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_conicalP_0
export    funcname=gsl_sf_conicalP_0_e
cat << \EOF > docstring.txt
Computes the conical function P^0_@{-1/2 + i \lambda@}(x)
for x > -1.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=conicalP_0
export    funcname=gsl_sf_conicalP_0_e
${RT} DD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_conicalP_1
export    funcname=gsl_sf_conicalP_1_e
cat << \EOF > docstring.txt
Computes the conical function P^1_@{-1/2 + i \lambda@}(x)
for x > -1.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=conicalP_1
export    funcname=gsl_sf_conicalP_1_e
${RT} DD_D >> gsl_sf.cc


# (double, mode) to double ###########################################

export octave_name=gsl_sf_airy_Ai
export    funcname=gsl_sf_airy_Ai_e
cat << \EOF > docstring.txt
Computes the Airy function Ai(x) with an accuracy
specified by mode.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Ai
export    funcname=gsl_sf_airy_Ai_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Bi
export    funcname=gsl_sf_airy_Bi_e
cat << \EOF > docstring.txt
Computes the Airy function Bi(x) with an accuracy
specified by mode.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Bi
export    funcname=gsl_sf_airy_Bi_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Ai_scaled
export    funcname=gsl_sf_airy_Ai_scaled_e
cat << \EOF > docstring.txt
Computes a scaled version of the Airy function
S_A(x) Ai(x). For x>0 the scaling factor S_A(x) is \exp(+(2/3) x^(3/2)), and
is 1 for x<0.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Ai_scaled
export    funcname=gsl_sf_airy_Ai_scaled_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Bi_scaled
export    funcname=gsl_sf_airy_Bi_scaled_e
cat << \EOF > docstring.txt
Computes a scaled version of the Airy function
S_B(x) Bi(x). For x>0 the scaling factor S_B(x) is exp(-(2/3) x^(3/2)), and
is 1 for x<0.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Bi_scaled
export    funcname=gsl_sf_airy_Bi_scaled_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Ai_deriv
export    funcname=gsl_sf_airy_Ai_deriv_e
cat << \EOF > docstring.txt
Computes the Airy function derivative Ai'(x) with an
accuracy specified by mode.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Ai_deriv
export    funcname=gsl_sf_airy_Ai_deriv_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Bi_deriv
export    funcname=gsl_sf_airy_Bi_deriv_e
cat << \EOF > docstring.txt
Computes the Airy function derivative Bi'(x) with an
accuracy specified by mode.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Bi_deriv
export    funcname=gsl_sf_airy_Bi_deriv_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Ai_deriv_scaled
export    funcname=gsl_sf_airy_Ai_deriv_scaled_e
cat << \EOF > docstring.txt
Computes the derivative of the scaled Airy function
S_A(x) Ai(x).
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Ai_deriv_scaled
export    funcname=gsl_sf_airy_Ai_deriv_scaled_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_Bi_deriv_scaled
export    funcname=gsl_sf_airy_Bi_deriv_scaled_e
cat << \EOF > docstring.txt
Computes the derivative of the scaled Airy function
S_B(x) Bi(x).
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_Bi_deriv_scaled
export    funcname=gsl_sf_airy_Bi_deriv_scaled_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_Kcomp
export    funcname=gsl_sf_ellint_Kcomp_e
cat << \EOF > docstring.txt
Computes the complete elliptic integral K(k)
@tex
\beforedisplay
$$
\eqalign{
K(k)   &= \int_0^{\pi/2}  {dt \over \sqrt{(1 - k^2 \sin^2(t))}}  \cr
}
$$
\afterdisplay
See also:
@end tex
@ifinfo
@group
@example
                              pi
                              ---
                               2
                             /
                             |             1
          ellint_Kcomp(k)  = |    ------------------- dt
                             |              2   2
                             /    sqrt(1 - k sin (t))
                              0

@end example
@end group
@end ifinfo
@ifhtml
@group
@example
                              pi
                              ---
                               2
                             /
                             |             1
          ellint_Kcomp(k)  = |    ------------------- dt
                             |              2   2
                             /    sqrt(1 - k sin (t))
                              0

@end example
@end group
@end ifhtml

@seealso{ellipj, ellipke}

The notation used here is based on Carlson, @cite{Numerische
Mathematik} 33 (1979) and differs slightly from that used by
Abramowitz & Stegun, where the functions are given in terms of the
parameter @math{m = k^2}.

EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=ellint_Kcomp
export    funcname=gsl_sf_ellint_Kcomp_e
${RT} DM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_Ecomp
export    funcname=gsl_sf_ellint_Ecomp_e
cat << \EOF > docstring.txt
Computes the complete elliptic integral E(k) to the
accuracy specified by the mode variable mode.

@tex
\beforedisplay
$$
\eqalign{
E(k)   &= \int_0^{\pi/2}    \sqrt{(1 - k^2 \sin^2(t))} dt \cr
}
$$
\afterdisplay
See also:

@end tex
@ifinfo
@group
@example
                               pi
                              ---
                               2
                             /
                             |              2    2
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt
                             |
                             /
                              0

@end example
@end group
@end ifinfo
@ifhtml
@group
@example
                               pi
                              ---
                               2
                             /
                             |              2    2
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt
                             |
                             /
                              0

@end example
@end group
@end ifhtml

@seealso{ellipj, ellipke}

The notation used here is based on Carlson, @cite{Numerische
Mathematik} 33 (1979) and differs slightly from that used by
Abramowitz & Stegun, where the functions are given in terms of the
parameter @math{m = k^2}.
EOF
${RT} DM_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=ellint_Ecomp
export    funcname=gsl_sf_ellint_Ecomp_e
${RT} DM_D >> gsl_sf.cc


# (double, double, mode) to double ###################################

export octave_name=gsl_sf_ellint_E
export    funcname=gsl_sf_ellint_E_e
cat << \EOF > docstring.txt
This routine computes the elliptic integral E(\phi,k) to the accuracy
specified by the mode variable mode. Note that Abramowitz & Stegun
define this function in terms of the parameter m = k^2.
EOF
${RT} DDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_F
export    funcname=gsl_sf_ellint_F_e
cat << \EOF > docstring.txt
This routine computes the elliptic integral F(\phi,k) to the accuracy
specified by the mode variable mode. Note that Abramowitz & Stegun
define this function in terms of the parameter m = k^2.
EOF
${RT} DDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_Pcomp
export    funcname=gsl_sf_ellint_Pcomp_e
cat << \EOF > docstring.txt
Computes the complete elliptic integral \Pi(k,n) to the
accuracy specified by the mode variable mode. Note that Abramowitz &
Stegun define this function in terms of the parameters m = k^2 and
\sin^2(\alpha) = k^2, with the change of sign n \to -n.
EOF
${RT} DDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_RC
export    funcname=gsl_sf_ellint_RC_e
cat << \EOF > docstring.txt
This routine computes the incomplete elliptic integral RC(x,y) to the
accuracy specified by the mode variable mode.
EOF
${RT} DDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_D
export    funcname=gsl_sf_ellint_D_e
cat << \EOF > docstring.txt
This function computes the incomplete elliptic integral
D(\phi,k) which is defined through the Carlson form
RD(x,y,z) by the following relation,

D(\phi,k) = (1/3)(\sin(\phi))^3
	  x RD (1-\sin^2(\phi), 1-k^2 \sin^2(\phi), 1).
EOF
echo "#if GSL_MAJOR_VERSION < 2" >> gsl_sf.cc
${RT} DDDM_D >> gsl_sf.cc
echo "#else" >> gsl_sf.cc
${RT} DDM_D >> gsl_sf.cc
echo "#endif" >> gsl_sf.cc


# (double, double, double, mode) to double ###########################

export octave_name=gsl_sf_ellint_P
export    funcname=gsl_sf_ellint_P_e
cat << \EOF > docstring.txt
This routine computes the incomplete elliptic integral \Pi(\phi,k,n)
to the accuracy specified by the mode variable mode. Note that
Abramowitz & Stegun define this function in terms of the parameters
m = k^2 and \sin^2(\alpha) = k^2, with the change of sign n \to -n.
EOF
${RT} DDDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_RD
export    funcname=gsl_sf_ellint_RD_e
cat << \EOF > docstring.txt
This routine computes the incomplete elliptic integral RD(x,y,z) to the
accuracy specified by the mode variable mode.
EOF
${RT} DDDM_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_ellint_RF
export    funcname=gsl_sf_ellint_RF_e
cat << \EOF > docstring.txt
This routine computes the incomplete elliptic integral RF(x,y,z) to the
accuracy specified by the mode variable mode.
EOF
${RT} DDDM_D >> gsl_sf.cc


# (double, double, double, double, mode) to double ###################

export octave_name=gsl_sf_ellint_RJ
export    funcname=gsl_sf_ellint_RJ_e
cat << \EOF > docstring.txt
This routine computes the incomplete elliptic integral RJ(x,y,z,p) to the
accuracy specified by the mode variable mode.
EOF
${RT} DDDDM_D >> gsl_sf.cc


# int to double ######################################################

export octave_name=gsl_sf_airy_zero_Ai
export    funcname=gsl_sf_airy_zero_Ai_e
cat << \EOF > docstring.txt
Computes the location of the s-th zero of the Airy
function Ai(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_zero_Ai
export    funcname=gsl_sf_airy_zero_Ai_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_zero_Bi
export    funcname=gsl_sf_airy_zero_Bi_e
cat << \EOF > docstring.txt
Computes the location of the s-th zero of the Airy
function Bi(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_zero_Bi
export    funcname=gsl_sf_airy_zero_Bi_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_zero_Ai_deriv
export    funcname=gsl_sf_airy_zero_Ai_deriv_e
cat << \EOF > docstring.txt
Computes the location of the s-th zero of the Airy
function derivative Ai(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_zero_Ai_deriv
export    funcname=gsl_sf_airy_zero_Ai_deriv_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_airy_zero_Bi_deriv
export    funcname=gsl_sf_airy_zero_Bi_deriv_e
cat << \EOF > docstring.txt
Computes the location of the s-th zero of the Airy
function derivative Bi(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=airy_zero_Bi_deriv
export    funcname=gsl_sf_airy_zero_Bi_deriv_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_zero_J0
export    funcname=gsl_sf_bessel_zero_J0_e
cat << \EOF > docstring.txt
Computes the location of the s-th positive zero of the
Bessel function J_0(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_zero_J0
export    funcname=gsl_sf_bessel_zero_J0_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_zero_J1
export    funcname=gsl_sf_bessel_zero_J1_e
cat << \EOF > docstring.txt
Computes the location of the s-th positive zero of the
Bessel function J_1(x).
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=bessel_zero_J1
export    funcname=gsl_sf_bessel_zero_J1_e
${RT} I_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_psi_1_int
export    funcname=gsl_sf_psi_1_int_e
cat << \EOF > docstring.txt
Computes the Trigamma function \psi(n) for positive
integer n.
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=psi_1_int
export    funcname=gsl_sf_psi_1_int_e
${RT} I_D >> gsl_sf.cc


# (int, double, double) to double ####################################

export octave_name=gsl_sf_conicalP_cyl_reg
export    funcname=gsl_sf_conicalP_cyl_reg_e
cat << \EOF > docstring.txt
Computes the Regular Cylindrical Conical Function
@math{P^{-m}_{-1/2 + i \lambda}(x)}, for @math{x > -1}, @math{m} @geq{} @math{-1}.
EOF
${RT} IDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_conicalP_sph_reg
export    funcname=gsl_sf_conicalP_sph_reg_e
cat << \EOF > docstring.txt
Computes the Regular Spherical Conical Function
@math{P^{-1/2-l}_{-1/2 + i \lambda}(x)}, for @math{x > -1}, @math{l} @geq{} @math{-1}.
EOF
${RT} IDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_gegenpoly_n
export    funcname=gsl_sf_gegenpoly_n_e
cat << \EOF > docstring.txt
These functions evaluate the Gegenbauer polynomial @math{C^{(\lambda)}_n(x)}
for n, lambda, x subject to @math{\lambda > -1/2}, @math{n} @geq{} @math{0}.
EOF
${RT} IDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_laguerre_n
export    funcname=gsl_sf_laguerre_n_e
cat << \EOF > docstring.txt
Computes the generalized Laguerre polynomial @math{L^a_n(x)} for
@math{a > -1} and @math{n} @geq{} @math{0}.
EOF
${RT} IDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_ce
export    funcname=gsl_sf_mathieu_ce_e
cat << \EOF > docstring.txt
This routine computes the angular Mathieu function ce_n(q,x).
EOF
${RT} IDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_se
export    funcname=gsl_sf_mathieu_se_e
cat << \EOF > docstring.txt
This routine computes the angular Mathieu function se_n(q,x).
EOF
${RT} IDD_D >> gsl_sf.cc

# (int, int, double) to double

######################################################################

export octave_name=gsl_sf_hyperg_U_int
export    funcname=gsl_sf_hyperg_U_int_e
cat << \EOF > docstring.txt
Secondary Confluent Hypergoemetric U function A&E 13.1.3
m and n are integers.
EOF
${RT} IID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_hyperg_1F1_int
export    funcname=gsl_sf_hyperg_1F1_int_e
cat << \EOF > docstring.txt
Primary Confluent Hypergoemetric U function A&E 13.1.3
m and n are integers.
EOF
${RT} IID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_Plm
export    funcname=gsl_sf_legendre_Plm_e
cat << \EOF > docstring.txt
Computes the associated Legendre polynomial P_l^m(x)
for m >= 0, l >= m, |x| <= 1.
EOF
${RT} IID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=legendre_Plm
export    funcname=gsl_sf_legendre_Plm_e
${RT} IID_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_sphPlm
export    funcname=gsl_sf_legendre_sphPlm_e
cat << \EOF > docstring.txt
Computes the normalized associated Legendre polynomial
$\sqrt@{(2l+1)/(4\pi)@} \sqrt@{(l-m)!/(l+m)!@} P_l^m(x)$ suitable for use
in spherical harmonics. The parameters must satisfy m >= 0, l >= m,
|x| <= 1. Theses routines avoid the overflows that occur for the
standard normalization of P_l^m(x).
EOF
${RT} IID_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=legendre_sphPlm
export    funcname=gsl_sf_legendre_sphPlm_e
${RT} IID_D >> gsl_sf.cc


# (int, int, double, double) to double ###############################

export octave_name=gsl_sf_hydrogenicR
export    funcname=gsl_sf_hydrogenicR_e
cat << \EOF > docstring.txt
This routine computes the n-th normalized hydrogenic bound state
radial wavefunction,
@tex
R_n := 2 (Z^{3/2}/n^2) \sqrt{(n-l-1)!/(n+l)!} \exp(-Z r/n)
         (2Zr/n)^l L^{2l+1}_{n-l-1}(2Zr/n).
@end tex
@ifnottex

@example
@group
          Z^(3/2)     ----------------    -Z r/n          l    2l+1
R_n := 2 --------  \ / (n-l-1)!/(n+l)! . e       . (2Zr/n)  . L      (2Zr/n).
            n^2     v                                          n-l-1
@end group
@end example

@end ifnottex
where @math{L^a_b(x)} is the generalized Laguerre polynomial.
The normalization is chosen such that the wavefunction \psi
is given by @math{\psi(n,l,r) = R_n Y_{lm}}.
EOF
${RT} IIDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_Mc
export    funcname=gsl_sf_mathieu_Mc_e
cat << \EOF > docstring.txt
This routine computes the radial j-th kind Mathieu function
@math{Mc_n^{(j)}(q,x)} of order n.
EOF
${RT} IIDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_mathieu_Ms
export    funcname=gsl_sf_mathieu_Ms_e
cat << \EOF > docstring.txt
This routine computes the radial j-th kind Mathieu function
@math{Ms_n^{(j)}(q,x)} of order n.
EOF
${RT} IIDD_D >> gsl_sf.cc


# (double, int) to double ############################################

export octave_name=gsl_sf_bessel_zero_Jnu
export    funcname=gsl_sf_bessel_zero_Jnu_e
cat << \EOF > docstring.txt
Computes the location of the n-th positive zero of the
Bessel function J_x().
EOF
${RT} DI_D >> gsl_sf.cc


# (double, double, double) to double #################################

export octave_name=gsl_sf_hyperg_U
export    funcname=gsl_sf_hyperg_U_e
cat << \EOF > docstring.txt
Secondary Confluent Hypergoemetric U function A&E 13.1.3
All inputs are double as is the output.
EOF
${RT} DDD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hyperg_U
export    funcname=gsl_sf_hyperg_U_e
${RT} DDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_hyperg_1F1
export    funcname=gsl_sf_hyperg_1F1_e
cat << \EOF > docstring.txt
Primary Confluent Hypergoemetric U function A&E 13.1.3
All inputs are double as is the output.
EOF
${RT} DDD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hyperg_1F1
export    funcname=gsl_sf_hyperg_1F1_e
${RT} DDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_hyperg_2F0
export    funcname=gsl_sf_hyperg_2F0_e
cat << \EOF > docstring.txt
Computes the hypergeometric function 2F0(a,b,x).
The series representation is a divergent hypergeometric series.
However, for x < 0 we have 2F0(a,b,x) = (-1/x)^a U(a,1+a-b,-1/x)
EOF
${RT} DDD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hyperg_2F0
export    funcname=gsl_sf_hyperg_2F0_e
${RT} DDD_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_beta_inc
export    funcname=gsl_sf_beta_inc_e
cat << \EOF > docstring.txt
Computes the normalized incomplete Beta function

  I_x(a,b)=B_x(a,b)/B(a,b)

where @math{B_x(a,b) = \int_0^x t^(a-1) (1-t)^(b-1) dt}
for @math{0 \le x \le 1}.

For a > 0, b > 0 the value is computed using a continued fraction
expansion. For all other values it is computed using the relation
  I_x(a,b,x) = (1/a) x^a 2F1(a,1-b,a+1,x)/B(a,b).
EOF
${RT} DDD_D >> gsl_sf.cc


# (double, double, double, double) to double #########################

export octave_name=gsl_sf_hyperg_2F1
export    funcname=gsl_sf_hyperg_2F1_e
cat << \EOF > docstring.txt
Computes the Gauss hypergeometric function
2F1(a,b,c,x) = F(a,b,c,x) for |x| < 1.
If the arguments (a,b,c,x) are too close to a singularity then
the function can return the error code GSL_EMAXITER when the
series approximation converges too slowly.
This occurs in the region of x=1, c - a - b = m for integer m.
EOF
${RT} DDDD_D >> gsl_sf.cc


# unsigned int to double #############################################

export octave_name=gsl_sf_fact
export    funcname=gsl_sf_fact_e
cat << \EOF > docstring.txt
Computes the factorial n!. The factorial is related to the Gamma
function by n! = \Gamma(n+1). The maximum value of n such that n! is
not considered an overflow is 170.
EOF
${RT} U_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_doublefact
export    funcname=gsl_sf_doublefact_e
cat << \EOF > docstring.txt
Compute the double factorial n!! = n(n-2)(n-4)\dots. The maximum value
of n such that n!! is not considered an overflow is 297.
EOF
${RT} U_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lnfact
export    funcname=gsl_sf_lnfact_e
cat << \EOF > docstring.txt
Computes the logarithm of the factorial of n,
\log(n!). The algorithm is faster than computing \ln(\Gamma(n+1)) via
gsl_sf_lngamma for n < 170, but defers for larger n.
EOF
${RT} U_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lndoublefact
export    funcname=gsl_sf_lndoublefact_e
cat << \EOF > docstring.txt
Computes the logarithm of the double factorial of n, \log(n!!).
EOF
${RT} U_D >> gsl_sf.cc


# (unsigned int, unsigned int) to double #############################

export octave_name=gsl_sf_choose
export    funcname=gsl_sf_choose_e
cat << \EOF > docstring.txt
Computes the combinatorial factor n choose m = n!/(m!(n-m)!).
EOF
${RT} UU_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_lnchoose
export    funcname=gsl_sf_lnchoose_e
cat << \EOF > docstring.txt
Computes the logarithm of n choose m. This is equivalent to
\log(n!) - \log(m!) - \log((n-m)!).
EOF
${RT} UU_D >> gsl_sf.cc


# (int, int, int, int, int, int) to double ###########################

export octave_name=gsl_sf_coupling_3j
export    funcname=gsl_sf_coupling_3j_e
cat << \EOF > docstring.txt
computes the Wigner 3-j coefficient,

@example
@group
(ja jb jc
 ma mb mc)
@end group
@end example

where the arguments are given in half-integer units,
@code{ja = two_ja/2}, @code{ma = two_ma/2}, etc.
EOF
${RT} IIIIII_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=coupling_3j
export    funcname=gsl_sf_coupling_3j_e
${RT} IIIIII_D >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_coupling_6j
export    funcname=gsl_sf_coupling_6j_e
cat << \EOF > docstring.txt
computes the Wigner 6-j coefficient,

@example
@group
@{ja jb jc
 jd je jf@}
@end group
@end example

where the arguments are given in half-integer units,
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.
EOF
${RT} IIIIII_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=coupling_6j
export    funcname=gsl_sf_coupling_6j_e
${RT} IIIIII_D >> gsl_sf.cc


# (int, int, int, int, int, int, int, int, int) to double ############

export octave_name=gsl_sf_coupling_9j
export    funcname=gsl_sf_coupling_9j_e
cat << \EOF > docstring.txt
computes the Wigner 9-j coefficient,

@example
@group
@{ja jb jc
 jd je jf
 jg jh ji@}
@end group
@end example

where the arguments are given in half-integer units,
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.
EOF
${RT} IIIIIIIII_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=coupling_9j
export    funcname=gsl_sf_coupling_9j_e
${RT} IIIIIIIII_D >> gsl_sf.cc


#############################
## gsl_sf_*_array function ##
#############################

# (length, double) ###################################################

export octave_name=gsl_sf_bessel_jl_array
export    funcname=gsl_sf_bessel_jl_array
cat << \EOF > docstring.txt
Computes the values of the regular spherical Bessel functions j_l(x)
for l from 0 to lmax inclusive for lmax >= 0 and x >= 0. The values
are computed using recurrence relations for efficiency, and therefore
may differ slightly from the exact values.
EOF
${RT} LD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_jl_steed_array
export    funcname=gsl_sf_bessel_jl_steed_array
cat << \EOF > docstring.txt
This routine uses Steed’s method to compute the values of the regular
spherical Bessel functions j_l(x) for l from 0 to lmax inclusive for
lmax >= 0 and x >= 0.  The Steed/Barnett algorithm is described in
Comp. Phys. Comm. 21, 297 (1981). Steed’s method is more stable than
the recurrence used in the other functions but is also slower.
EOF
${RT} LD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_il_scaled_array
export    funcname=gsl_sf_bessel_il_scaled_array
cat << \EOF > docstring.txt
This routine computes the values of the scaled regular modified
spherical Bessel functions \exp(-|x|) i_l(x) for l from 0 to lmax
inclusive for lmax >= 0. The values are computed using recurrence
relations for efficiency, and therefore may differ slightly from the
exact values.
EOF
${RT} LD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_kl_scaled_array
export    funcname=gsl_sf_bessel_kl_scaled_array
cat << \EOF > docstring.txt
This routine computes the values of the scaled irregular modified
spherical Bessel functions \exp(x) k_l(x) for l from 0 to lmax
inclusive for lmax >= 0 and x>0. The values are computed using
recurrence relations for efficiency, and therefore may differ slightly
from the exact values.
EOF
${RT} LD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_yl_array
export    funcname=gsl_sf_bessel_yl_array
cat << \EOF > docstring.txt
This routine computes the values of the irregular spherical Bessel
functions y_l(x) for l from 0 to lmax inclusive for lmax >= 0. The
values are computed using recurrence relations for efficiency, and
therefore may differ slightly from the exact values.
EOF
${RT} LD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_Pl_array
export    funcname=gsl_sf_legendre_Pl_array
cat << \EOF > docstring.txt
These functions compute arrays of Legendre polynomials P_l(x) and
derivatives dP_l(x)/dx, for l = 0, \dots, lmax, |x| <= 1.
EOF
${RT} LD_D_array >> gsl_sf.cc


# (length, double, double) ###########################################

export octave_name=gsl_sf_gegenpoly_array
export    funcname=gsl_sf_gegenpoly_array
cat << \EOF > docstring.txt
This function computes an array of Gegenbauer polynomials
@math{C^{(\lambda)}_n(x)} for n = 0, 1, 2, \dots, nmax, subject to
@math{\lambda > -1/2}, @math{nmax} @geq{} @math{0}.
EOF
${RT} LDD_D_array >> gsl_sf.cc


# (min, max, double) #################################################

export octave_name=gsl_sf_bessel_In_array
export    funcname=gsl_sf_bessel_In_array
cat << \EOF > docstring.txt
his routine computes the values of the regular modified cylindrical
Bessel functions I_n(x) for n from nmin to nmax inclusive. The start
of the range nmin must be positive or zero. The values are computed
using recurrence relations for efficiency, and therefore may differ
slightly from the exact values.
EOF
${RT} LLD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_In_scaled_array
export    funcname=gsl_sf_bessel_In_scaled_array
cat << \EOF > docstring.txt
This routine computes the values of the scaled regular cylindrical
Bessel functions \exp(-|x|) I_n(x) for n from nmin to nmax
inclusive. The start of the range nmin must be positive or zero. The
values are computed using recurrence relations for efficiency, and
therefore may differ slightly from the exact values.
EOF
${RT} LLD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Jn_array
export    funcname=gsl_sf_bessel_Jn_array
cat << \EOF > docstring.txt
This routine computes the values of the regular cylindrical Bessel
functions J_n(x) for n from nmin to nmax inclusive. The values are
computed using recurrence relations for efficiency, and therefore may
differ slightly from the exact values.
EOF
${RT} LLD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Kn_array
export    funcname=gsl_sf_bessel_Kn_array
cat << \EOF > docstring.txt
This routine computes the values of the irregular modified cylindrical
Bessel functions K_n(x) for n from nmin to nmax inclusive. The start
of the range nmin must be positive or zero. The domain of the function
is x>0. The values are computed using recurrence relations for
efficiency, and therefore may differ slightly from the exact values.
EOF
${RT} LLD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Kn_scaled_array
export    funcname=gsl_sf_bessel_Kn_scaled_array
cat << \EOF > docstring.txt
This routine computes the values of the scaled irregular cylindrical
Bessel functions \exp(x) K_n(x) for n from nmin to nmax inclusive. The
start of the range nmin must be positive or zero. The domain of the
function is x>0. The values are computed using recurrence relations
for efficiency, and therefore may differ slightly from the exact
values.
EOF
${RT} LLD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_bessel_Yn_array
export    funcname=gsl_sf_bessel_Yn_array
cat << \EOF > docstring.txt
This routine computes the values of the irregular cylindrical Bessel
functions Y_n(x) for n from nmin to nmax inclusive. The domain of the
function is x>0. The values are computed using recurrence relations
for efficiency, and therefore may differ slightly from the exact
values.
EOF
${RT} LLD_D_array >> gsl_sf.cc


# (length, int, double) to double ####################################

# DEPRECATED: gsl_sf_legendre_Plm_array 2.0
export octave_name=gsl_sf_legendre_Plm_array
export    funcname=gsl_sf_legendre_Plm_array
cat << \EOF > docstring.txt
Compute arrays of Legendre polynomials P_l^m(x) for m >= 0, l = |m|,
..., lmax, |x| <= 1.
EOF
${RT} LID_D_array >> gsl_sf.cc

######################################################################

# DEPRECATED: gsl_sf_legendre_Plm_deriv_array 2.0
export octave_name=gsl_sf_legendre_Plm_deriv_array
export    funcname=gsl_sf_legendre_Plm_deriv_array
cat << \EOF > docstring.txt
Compute arrays of Legendre polynomials P_l^m(x) and derivatives
dP_l^m(x)/dx for m >= 0, l = |m|, ..., lmax, |x| <= 1.
EOF
${RT} LID_DD_array >> gsl_sf.cc

######################################################################

# DEPRECATED: gsl_sf_legendre_sphPlm_array 2.0
export octave_name=gsl_sf_legendre_sphPlm_array
export    funcname=gsl_sf_legendre_sphPlm_array
cat << \EOF > docstring.txt
Computes an array of normalized associated Legendre functions
@iftex
@tex
$\\sqrt{(2l+1)/(4\\pi)} \\sqrt{(l-m)!/(l+m)!} P_l^m(x)$
for $m >= 0, l = |m|, ..., lmax, |x| <= 1.0$
@end tex
@end iftex
@ifinfo
sqrt((2l+1)/(4*pi)) * sqrt((l-m)!/(l+m)!) Plm (x)
for m >= 0, l = |m|, ..., lmax, |x| <= 1.0
@end ifinfo
EOF
${RT} LID_D_array >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=legendre_sphPlm_array
export    funcname=gsl_sf_legendre_sphPlm_array
${RT} LID_D_array >> gsl_sf.cc

######################################################################

# DEPRECATED: gsl_sf_legendre_sphPlm_deriv_array 2.0
export octave_name=gsl_sf_legendre_sphPlm_deriv_array
export    funcname=gsl_sf_legendre_sphPlm_deriv_array
cat << \EOF > docstring.txt
Computes an array of normalized associated Legendre functions
@iftex
@tex
$\\sqrt{(2l+1)/(4\\pi)} \\sqrt{(l-m)!/(l+m)!} P_l^m(x)$
for $m \geq 0, l = |m|, ..., lmax, |x| <= 1.0$
@end tex
@end iftex
@ifinfo
sqrt((2l+1)/(4*pi)) * sqrt((l-m)!/(l+m)!) Plm (x)
for m >= 0, l = |m|, ..., lmax, |x| <= 1.0
@end ifinfo
and their derivatives.
EOF
${RT} LID_DD_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_array
export    funcname=gsl_sf_legendre_array_e
cat << \EOF > docstring.txt
Calculate all normalized associated Legendre polynomials for 0 <= l <=
lmax and 0 <= m <= l for |x| <= 1. The norm parameter specifies which
normalization is used. The array index of P_l^m is given by
l(l+1)/2+m. To include or exclude the Condon-Shortley phase factor of
(-1)^m, set the fourth parameter to either -1 or 1, respectively.

EOF
${RT} NSDD_D_array >> gsl_sf.cc

######################################################################

export octave_name=gsl_sf_legendre_deriv_array
export    funcname=gsl_sf_legendre_deriv_array_e
cat << \EOF > docstring.txt
Calculate all normalized associated Legendre polynomials and their
first derivatives for 0 <= l <= lmax and 0 <= m <= l for |x| <= 1. The
norm parameter specifies which normalization is used. The array index
of P_l^m is given by l(l+1)/2+m. To include or exclude the
Condon-Shortley phase factor of (-1)^m, set the fourth parameter to
either -1 or 1, respectively.
EOF
${RT} NSDD_DD_array >> gsl_sf.cc

######################################################################

export octave_name=legendre_deriv2_array
export    funcname=gsl_sf_legendre_deriv2_array_e
cat << \EOF > docstring.txt
Calculate all normalized associated Legendre polynomials and their
first and second derivatives for 0 <= l <= lmax and 0 <= m <= l for |x| <= 1. The
norm parameter specifies which normalization is used. The array index
of P_l^m is given by l(l+1)/2+m. To include or exclude the
Condon-Shortley phase factor of (-1)^m, set the fourth parameter to
either -1 or 1, respectively.
EOF
${RT} NSDD_DDD_array >> gsl_sf.cc


######################################################################
##                   Zeta and related functions                     ##
######################################################################


## Riemann zeta function

export octave_name=gsl_sf_zeta
export    funcname=gsl_sf_zeta_e
cat << \EOF > docstring.txt
Computes the Riemann zeta function \zeta(s) for
arbitrary s, s \ne 1.

The Riemann zeta function is defined by the infinite sum
\zeta(s) = \sum_@{k=1@}^\infty k^@{-s@}.
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme (package release 2.0.0)
export octave_name=gsl_zt_zeta
export    funcname=gsl_sf_zeta_e
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme (package release < 2.0.0)
export octave_name=zeta
export    funcname=gsl_sf_zeta_e
${RT} D_D >> gsl_sf.cc

export octave_name=gsl_sf_zeta_int
export    funcname=gsl_sf_zeta_int_e
cat << \EOF > docstring.txt
Computes the Riemann zeta function \zeta(n) for
integer n, n \ne 1.
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme (package release < 2.0.0)
export octave_name=zeta_int
export    funcname=gsl_sf_zeta_int_e
${RT} I_D >> gsl_sf.cc


## Riemann zeta function minus one

export octave_name=gsl_sf_zetam1
export    funcname=gsl_sf_zetam1_e
cat << \EOF > docstring.txt
Computes \zeta(s) - 1 for arbitrary s, s \ne 1, where \zeta
denotes the Riemann zeta function.

@seealso{gsl_sf_zeta}
EOF
${RT} D_D >> gsl_sf.cc

export octave_name=gsl_sf_zetam1_int
export    funcname=gsl_sf_zetam1_int_e
cat << \EOF > docstring.txt
Computes \zeta(s) - 1 for integer n, n \ne 1, where \zeta
denotes the Riemann zeta function.

@seealso{gsl_sf_zetam1, gsl_sf_zeta, gsl_sf_zeta_int}
EOF
${RT} I_D >> gsl_sf.cc


## Dirichlet eta function 

export octave_name=gsl_sf_eta
export    funcname=gsl_sf_eta_e
cat << \EOF > docstring.txt
Computes the eta function \eta(s) for arbitrary s.

The eta function is defined by \eta(s) = (1-2^@{1-s@}) \zeta
EOF
${RT} D_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=eta
export    funcname=gsl_sf_eta_e
${RT} D_D >> gsl_sf.cc

export octave_name=gsl_sf_eta_int
export    funcname=gsl_sf_eta_int_e
cat << \EOF > docstring.txt
Computes the eta function \eta(n) for integer n.
EOF
${RT} I_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=eta_int
export    funcname=gsl_sf_eta_int_e
${RT} I_D >> gsl_sf.cc


## Hurwitz zeta function

export octave_name=gsl_sf_hzeta
export    funcname=gsl_sf_hzeta_e
cat << \EOF > docstring.txt
Computes the Hurwitz zeta function \zeta(s,q)
for s > 1, q > 0.
EOF
${RT} DD_D >> gsl_sf.cc

## Deprecated naming scheme
export octave_name=hzeta
export    funcname=gsl_sf_hzeta_e
${RT} DD_D >> gsl_sf.cc
