/*
 Copyright (C) 2004   Teemu Ikonen   <tpikonen@pcu.helsinki.fi>
 Copyright (C) 2016   Susi Lehtola
 Copyright (C) 2016   Julien Bect    <jbect@users.sourceforge.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include <limits>
#include <octave/oct.h>
#include <octave/parse.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_version.h>

#ifdef OCTAVE_HAS_OV_ISREAL_METHOD
#define ISREAL(x) ((x).isreal ())
#else
#define ISREAL(x) ((x).is_real_type ())
#endif

void octave_gsl_errorhandler (const char * reason, const char * file,
			      int line, int gsl_errno)
{
    error("GSL error %d at %s, line %d: %s\n", gsl_errno, file, line, reason);
}

DEFUN_DLD (gsl_sf, args, nargout,
  "-*- texinfo -*-\n\
@deftypefn {Loadable Function} gsl_sf ()\n\
\n\
gsl_sf is an oct-file containing Octave bindings to the \
special functions of the GNU Scientific Library (GSL).\n\
\n\
@end deftypefn\n")
{
#ifdef OCTAVE_HAS_FEVAL_IN_OCTAVE_NAMESPACE
  octave::feval ("help", octave_value ("gsl_sf"));
#else
  feval ("help", octave_value ("gsl_sf"));
#endif

  return octave_value();
}


template <typename A>
bool check_arg_dim
(
 A arg,
 dim_vector &dim,
 octave_idx_type &numel,
 bool &conformant
)
{
  dim_vector arg_dim = arg.dims ();
  octave_idx_type arg_numel = arg.numel ();

  // If this is a scalar argument, nothing more to do.
  // The return value indicates that this is a scalar argument.
  if (arg_numel == 1)
    return true;

  if (numel == 1)
  {
    dim = arg_dim;
    numel = arg_numel;
  }
  else if (arg_dim != dim)
  {
    conformant = false;
  }
  return false;
}


// PKG_ADD: autoload ("gsl_sf_clausen", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_clausen, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_clausen (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_clausen (@dots{})\n\
\n\
The Clausen function is defined by the following integral,\n\
\n\
Cl_2(x) = - \\int_0^x dt \\log(2 \\sin(t/2))\n\
\n\
It is related to the dilogarithm by Cl_2(\\theta) = \\Im Li_2(\\exp(i \\theta)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CLAUSEN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_clausen_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_clausen_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CLAUSEN_E undefined

  error ("GSL function gsl_sf_clausen_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CLAUSEN_E
}


// PKG_ADD: autoload ("clausen", which ("gsl_sf"));
DEFUN_DLD(clausen, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} clausen (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} clausen (@dots{})\n\
\n\
The Clausen function is defined by the following integral,\n\
\n\
Cl_2(x) = - \\int_0^x dt \\log(2 \\sin(t/2))\n\
\n\
It is related to the dilogarithm by Cl_2(\\theta) = \\Im Li_2(\\exp(i \\theta)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CLAUSEN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_clausen_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_clausen_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CLAUSEN_E undefined

  error ("GSL function gsl_sf_clausen_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CLAUSEN_E
}


// PKG_ADD: autoload ("gsl_sf_dawson", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_dawson, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_dawson (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_dawson (@dots{})\n\
\n\
The Dawson integral is defined by \\exp(-x^2) \\int_0^x dt \\exp(t^2).\n\
A table of Dawson integral can be found in Abramowitz & Stegun, Table 7.5.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DAWSON_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dawson_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dawson_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DAWSON_E undefined

  error ("GSL function gsl_sf_dawson_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DAWSON_E
}


// PKG_ADD: autoload ("dawson", which ("gsl_sf"));
DEFUN_DLD(dawson, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} dawson (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} dawson (@dots{})\n\
\n\
The Dawson integral is defined by \\exp(-x^2) \\int_0^x dt \\exp(t^2).\n\
A table of Dawson integral can be found in Abramowitz & Stegun, Table 7.5.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DAWSON_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dawson_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dawson_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DAWSON_E undefined

  error ("GSL function gsl_sf_dawson_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DAWSON_E
}


// PKG_ADD: autoload ("gsl_sf_debye_1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_1 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_1_E undefined

  error ("GSL function gsl_sf_debye_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_1_E
}


// PKG_ADD: autoload ("debye_1", which ("gsl_sf"));
DEFUN_DLD(debye_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} debye_1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} debye_1 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_1_E undefined

  error ("GSL function gsl_sf_debye_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_1_E
}


// PKG_ADD: autoload ("gsl_sf_debye_2", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_2 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_2_E undefined

  error ("GSL function gsl_sf_debye_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_2_E
}


// PKG_ADD: autoload ("debye_2", which ("gsl_sf"));
DEFUN_DLD(debye_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} debye_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} debye_2 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_2_E undefined

  error ("GSL function gsl_sf_debye_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_2_E
}


// PKG_ADD: autoload ("gsl_sf_debye_3", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_3 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_3_E undefined

  error ("GSL function gsl_sf_debye_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_3_E
}


// PKG_ADD: autoload ("debye_3", which ("gsl_sf"));
DEFUN_DLD(debye_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} debye_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} debye_3 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_3_E undefined

  error ("GSL function gsl_sf_debye_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_3_E
}


// PKG_ADD: autoload ("gsl_sf_debye_4", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_4, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_4 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_4 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_4_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_4_E undefined

  error ("GSL function gsl_sf_debye_4_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_4_E
}


// PKG_ADD: autoload ("debye_4", which ("gsl_sf"));
DEFUN_DLD(debye_4, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} debye_4 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} debye_4 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_4_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_4_E undefined

  error ("GSL function gsl_sf_debye_4_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_4_E
}


// PKG_ADD: autoload ("gsl_sf_debye_5", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_5, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_5 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_5 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_5_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_5_E undefined

  error ("GSL function gsl_sf_debye_5_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_5_E
}


// PKG_ADD: autoload ("gsl_sf_debye_6", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_debye_6, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_debye_6 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_debye_6 (@dots{})\n\
\n\
The Debye functions are defined by the integral\n\
\n\
D_n(x) = n/x^n \\int_0^x dt (t^n/(e^t - 1)).\n\
\n\
For further information see Abramowitz & Stegun, Section 27.1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DEBYE_6_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_6_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_debye_6_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DEBYE_6_E undefined

  error ("GSL function gsl_sf_debye_6_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DEBYE_6_E
}


// PKG_ADD: autoload ("gsl_sf_dilog", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_dilog, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_dilog (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_dilog (@dots{})\n\
\n\
Computes the dilogarithm for a real argument.\n\
In Lewin’s notation this is Li_2(x), the real part of the\n\
dilogarithm of a real x. It is defined by the integral\n\
representation\n\
\n\
 Li_2(x) = - \\Re \\int_0^x ds \\log(1-s) / s.\n\
\n\
Note that \\Im(Li_2(x)) = 0 for x <= 1, and -\\pi\\log(x)\n\
for x > 1.\n\
\n\
Note that Abramowitz & Stegun refer to the Spence integral\n\
S(x)=Li_2(1-x) as the dilogarithm rather than Li_2(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DILOG_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dilog_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_dilog_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DILOG_E undefined

  error ("GSL function gsl_sf_dilog_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DILOG_E
}


// PKG_ADD: autoload ("gsl_sf_erf", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_erf, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_erf (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_erf (@dots{})\n\
\n\
Computes the error function\n\
erf(x) = (2/\\sqrt(\\pi)) \\int_0^x dt \\exp(-t^2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_E undefined

  error ("GSL function gsl_sf_erf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_E
}


// PKG_ADD: autoload ("erf_gsl", which ("gsl_sf"));
DEFUN_DLD(erf_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} erf_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} erf_gsl (@dots{})\n\
\n\
Computes the error function\n\
erf(x) = (2/\\sqrt(\\pi)) \\int_0^x dt \\exp(-t^2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_E undefined

  error ("GSL function gsl_sf_erf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_E
}


// PKG_ADD: autoload ("gsl_sf_erfc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_erfc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_erfc (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_erfc (@dots{})\n\
\n\
Computes the complementary error function\n\
erfc(x) = 1 - erf(x) = (2/\\sqrt(\\pi)) \\int_x^\\infty \\exp(-t^2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERFC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERFC_E undefined

  error ("GSL function gsl_sf_erfc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERFC_E
}


// PKG_ADD: autoload ("erfc_gsl", which ("gsl_sf"));
DEFUN_DLD(erfc_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} erfc_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} erfc_gsl (@dots{})\n\
\n\
Computes the complementary error function\n\
erfc(x) = 1 - erf(x) = (2/\\sqrt(\\pi)) \\int_x^\\infty \\exp(-t^2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERFC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERFC_E undefined

  error ("GSL function gsl_sf_erfc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERFC_E
}


// PKG_ADD: autoload ("gsl_sf_log_erfc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_log_erfc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_log_erfc (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_log_erfc (@dots{})\n\
\n\
Computes the logarithm of the complementary error\n\
function \\log(\\erfc(x)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_ERFC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_ERFC_E undefined

  error ("GSL function gsl_sf_log_erfc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_ERFC_E
}


// PKG_ADD: autoload ("log_erfc", which ("gsl_sf"));
DEFUN_DLD(log_erfc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} log_erfc (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} log_erfc (@dots{})\n\
\n\
Computes the logarithm of the complementary error\n\
function \\log(\\erfc(x)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_ERFC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_erfc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_ERFC_E undefined

  error ("GSL function gsl_sf_log_erfc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_ERFC_E
}


// PKG_ADD: autoload ("gsl_sf_erf_Z", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_erf_Z, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_erf_Z (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_erf_Z (@dots{})\n\
\n\
Computes the Gaussian probability function\n\
Z(x) = (1/(2\\pi)) \\exp(-x^2/2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_Z_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Z_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Z_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_Z_E undefined

  error ("GSL function gsl_sf_erf_Z_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_Z_E
}


// PKG_ADD: autoload ("erf_Z", which ("gsl_sf"));
DEFUN_DLD(erf_Z, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} erf_Z (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} erf_Z (@dots{})\n\
\n\
Computes the Gaussian probability function\n\
Z(x) = (1/(2\\pi)) \\exp(-x^2/2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_Z_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Z_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Z_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_Z_E undefined

  error ("GSL function gsl_sf_erf_Z_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_Z_E
}


// PKG_ADD: autoload ("gsl_sf_erf_Q", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_erf_Q, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_erf_Q (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_erf_Q (@dots{})\n\
\n\
Computes the upper tail of the Gaussian probability\n\
function  Q(x) = (1/(2\\pi)) \\int_x^\\infty dt \\exp(-t^2/2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_Q_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Q_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Q_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_Q_E undefined

  error ("GSL function gsl_sf_erf_Q_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_Q_E
}


// PKG_ADD: autoload ("erf_Q", which ("gsl_sf"));
DEFUN_DLD(erf_Q, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} erf_Q (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} erf_Q (@dots{})\n\
\n\
Computes the upper tail of the Gaussian probability\n\
function  Q(x) = (1/(2\\pi)) \\int_x^\\infty dt \\exp(-t^2/2).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ERF_Q_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Q_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_erf_Q_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ERF_Q_E undefined

  error ("GSL function gsl_sf_erf_Q_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ERF_Q_E
}


// PKG_ADD: autoload ("gsl_sf_hazard", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hazard, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hazard (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hazard (@dots{})\n\
\n\
The hazard function for the normal distrbution, also known as the\n\
inverse Mill\\'s ratio, is defined as\n\
h(x) = Z(x)/Q(x) = \\sqrt@{2/\\pi \\exp(-x^2 / 2) / \\erfc(x/\\sqrt 2)@}.\n\
It decreases rapidly as x approaches -\\infty and asymptotes to\n\
h(x) \\sim x as x approaches +\\infty.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HAZARD_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_hazard_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_hazard_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HAZARD_E undefined

  error ("GSL function gsl_sf_hazard_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HAZARD_E
}


// PKG_ADD: autoload ("hazard", which ("gsl_sf"));
DEFUN_DLD(hazard, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hazard (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hazard (@dots{})\n\
\n\
The hazard function for the normal distrbution, also known as the\n\
inverse Mill\\'s ratio, is defined as\n\
h(x) = Z(x)/Q(x) = \\sqrt@{2/\\pi \\exp(-x^2 / 2) / \\erfc(x/\\sqrt 2)@}.\n\
It decreases rapidly as x approaches -\\infty and asymptotes to\n\
h(x) \\sim x as x approaches +\\infty.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HAZARD_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_hazard_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_hazard_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HAZARD_E undefined

  error ("GSL function gsl_sf_hazard_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HAZARD_E
}


// PKG_ADD: autoload ("gsl_sf_expm1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_expm1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_expm1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_expm1 (@dots{})\n\
\n\
Computes the quantity \\exp(x)-1 using an algorithm that\n\
is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPM1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPM1_E undefined

  error ("GSL function gsl_sf_expm1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPM1_E
}


// PKG_ADD: autoload ("expm1", which ("gsl_sf"));
DEFUN_DLD(expm1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} expm1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} expm1 (@dots{})\n\
\n\
Computes the quantity \\exp(x)-1 using an algorithm that\n\
is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPM1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPM1_E undefined

  error ("GSL function gsl_sf_expm1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPM1_E
}


// PKG_ADD: autoload ("gsl_sf_exprel", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_exprel, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_exprel (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_exprel (@dots{})\n\
\n\
Computes the quantity (\\exp(x)-1)/x using an algorithm\n\
that is accurate for small x. For small x the algorithm is based on\n\
the expansion (\\exp(x)-1)/x = 1 + x/2 + x^2/(2*3) + x^3/(2*3*4) + \\dots.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_E undefined

  error ("GSL function gsl_sf_exprel_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_E
}


// PKG_ADD: autoload ("exprel", which ("gsl_sf"));
DEFUN_DLD(exprel, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} exprel (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} exprel (@dots{})\n\
\n\
Computes the quantity (\\exp(x)-1)/x using an algorithm\n\
that is accurate for small x. For small x the algorithm is based on\n\
the expansion (\\exp(x)-1)/x = 1 + x/2 + x^2/(2*3) + x^3/(2*3*4) + \\dots.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_E undefined

  error ("GSL function gsl_sf_exprel_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_E
}


// PKG_ADD: autoload ("gsl_sf_exprel_2", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_exprel_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_exprel_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_exprel_2 (@dots{})\n\
\n\
Computes the quantity 2(\\exp(x)-1-x)/x^2 using an\n\
algorithm that is accurate for small x. For small x the algorithm is\n\
based on the expansion\n\
2(\\exp(x)-1-x)/x^2 = 1 + x/3 + x^2/(3*4) + x^3/(3*4*5) + \\dots.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_2_E undefined

  error ("GSL function gsl_sf_exprel_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_2_E
}


// PKG_ADD: autoload ("exprel_2", which ("gsl_sf"));
DEFUN_DLD(exprel_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} exprel_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} exprel_2 (@dots{})\n\
\n\
Computes the quantity 2(\\exp(x)-1-x)/x^2 using an\n\
algorithm that is accurate for small x. For small x the algorithm is\n\
based on the expansion\n\
2(\\exp(x)-1-x)/x^2 = 1 + x/3 + x^2/(3*4) + x^3/(3*4*5) + \\dots.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_exprel_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_2_E undefined

  error ("GSL function gsl_sf_exprel_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_2_E
}


// PKG_ADD: autoload ("gsl_sf_expint_E1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_expint_E1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_expint_E1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_expint_E1 (@dots{})\n\
\n\
Computes the exponential integral E_1(x),\n\
\n\
E_1(x) := Re \\int_1^\\infty dt \\exp(-xt)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_E1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_E1_E undefined

  error ("GSL function gsl_sf_expint_E1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_E1_E
}


// PKG_ADD: autoload ("expint_E1", which ("gsl_sf"));
DEFUN_DLD(expint_E1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} expint_E1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} expint_E1 (@dots{})\n\
\n\
Computes the exponential integral E_1(x),\n\
\n\
E_1(x) := Re \\int_1^\\infty dt \\exp(-xt)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_E1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_E1_E undefined

  error ("GSL function gsl_sf_expint_E1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_E1_E
}


// PKG_ADD: autoload ("gsl_sf_expint_E2", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_expint_E2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_expint_E2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_expint_E2 (@dots{})\n\
\n\
Computes the second-order exponential integral E_2(x),\n\
\n\
E_2(x) := \\Re \\int_1^\\infty dt \\exp(-xt)/t^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_E2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_E2_E undefined

  error ("GSL function gsl_sf_expint_E2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_E2_E
}


// PKG_ADD: autoload ("expint_E2", which ("gsl_sf"));
DEFUN_DLD(expint_E2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} expint_E2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} expint_E2 (@dots{})\n\
\n\
Computes the second-order exponential integral E_2(x),\n\
\n\
E_2(x) := \\Re \\int_1^\\infty dt \\exp(-xt)/t^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_E2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_E2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_E2_E undefined

  error ("GSL function gsl_sf_expint_E2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_E2_E
}


// PKG_ADD: autoload ("gsl_sf_expint_Ei", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_expint_Ei, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_expint_Ei (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_expint_Ei (@dots{})\n\
\n\
Computes the exponential integral E_i(x),\n\
\n\
Ei(x) := - PV(\\int_@{-x@}^\\infty dt \\exp(-t)/t)\n\
\n\
where PV denotes the principal value of the integral.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_EI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_Ei_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_Ei_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_EI_E undefined

  error ("GSL function gsl_sf_expint_Ei_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_EI_E
}


// PKG_ADD: autoload ("expint_Ei", which ("gsl_sf"));
DEFUN_DLD(expint_Ei, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} expint_Ei (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} expint_Ei (@dots{})\n\
\n\
Computes the exponential integral E_i(x),\n\
\n\
Ei(x) := - PV(\\int_@{-x@}^\\infty dt \\exp(-t)/t)\n\
\n\
where PV denotes the principal value of the integral.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_EI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_Ei_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_Ei_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_EI_E undefined

  error ("GSL function gsl_sf_expint_Ei_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_EI_E
}


// PKG_ADD: autoload ("gsl_sf_Shi", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_Shi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_Shi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_Shi (@dots{})\n\
\n\
Computes the integral Shi(x) = \\int_0^x dt \\sinh(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SHI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Shi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Shi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SHI_E undefined

  error ("GSL function gsl_sf_Shi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SHI_E
}


// PKG_ADD: autoload ("Shi", which ("gsl_sf"));
DEFUN_DLD(Shi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} Shi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} Shi (@dots{})\n\
\n\
Computes the integral Shi(x) = \\int_0^x dt \\sinh(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SHI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Shi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Shi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SHI_E undefined

  error ("GSL function gsl_sf_Shi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SHI_E
}


// PKG_ADD: autoload ("gsl_sf_Chi", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_Chi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_Chi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_Chi (@dots{})\n\
\n\
Computes the integral \n\
\n\
Chi(x) := Re[ \\gamma_E + \\log(x) + \\int_0^x dt (\\cosh[t]-1)/t] ,\n\
\n\
where \\gamma_E is the Euler constant.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CHI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Chi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Chi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CHI_E undefined

  error ("GSL function gsl_sf_Chi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CHI_E
}


// PKG_ADD: autoload ("Chi", which ("gsl_sf"));
DEFUN_DLD(Chi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} Chi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} Chi (@dots{})\n\
\n\
Computes the integral \n\
\n\
Chi(x) := Re[ \\gamma_E + \\log(x) + \\int_0^x dt (\\cosh[t]-1)/t] ,\n\
\n\
where \\gamma_E is the Euler constant.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CHI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Chi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Chi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CHI_E undefined

  error ("GSL function gsl_sf_Chi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CHI_E
}


// PKG_ADD: autoload ("gsl_sf_expint_3", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_expint_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_expint_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_expint_3 (@dots{})\n\
\n\
Computes the exponential integral\n\
Ei_3(x) = \\int_0^x dt \\exp(-t^3) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_3_E undefined

  error ("GSL function gsl_sf_expint_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_3_E
}


// PKG_ADD: autoload ("expint_3", which ("gsl_sf"));
DEFUN_DLD(expint_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} expint_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} expint_3 (@dots{})\n\
\n\
Computes the exponential integral\n\
Ei_3(x) = \\int_0^x dt \\exp(-t^3) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPINT_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_expint_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPINT_3_E undefined

  error ("GSL function gsl_sf_expint_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPINT_3_E
}


// PKG_ADD: autoload ("gsl_sf_Si", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_Si, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_Si (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_Si (@dots{})\n\
\n\
Computes the Sine integral Si(x) = \\int_0^x dt \\sin(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Si_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Si_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SI_E undefined

  error ("GSL function gsl_sf_Si_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SI_E
}


// PKG_ADD: autoload ("Si", which ("gsl_sf"));
DEFUN_DLD(Si, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} Si (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} Si (@dots{})\n\
\n\
Computes the Sine integral Si(x) = \\int_0^x dt \\sin(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Si_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Si_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SI_E undefined

  error ("GSL function gsl_sf_Si_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SI_E
}


// PKG_ADD: autoload ("gsl_sf_Ci", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_Ci, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_Ci (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_Ci (@dots{})\n\
\n\
Computes the Cosine integral\n\
Ci(x) = -\\int_x^\\infty dt \\cos(t)/t for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Ci_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Ci_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CI_E undefined

  error ("GSL function gsl_sf_Ci_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CI_E
}


// PKG_ADD: autoload ("Ci", which ("gsl_sf"));
DEFUN_DLD(Ci, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} Ci (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} Ci (@dots{})\n\
\n\
Computes the Cosine integral\n\
Ci(x) = -\\int_x^\\infty dt \\cos(t)/t for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Ci_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_Ci_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CI_E undefined

  error ("GSL function gsl_sf_Ci_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CI_E
}


// PKG_ADD: autoload ("gsl_sf_atanint", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_atanint, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_atanint (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_atanint (@dots{})\n\
\n\
Computes the Arctangent integral\n\
AtanInt(x) = \\int_0^x dt \\arctan(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ATANINT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_atanint_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_atanint_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ATANINT_E undefined

  error ("GSL function gsl_sf_atanint_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ATANINT_E
}


// PKG_ADD: autoload ("atanint", which ("gsl_sf"));
DEFUN_DLD(atanint, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} atanint (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} atanint (@dots{})\n\
\n\
Computes the Arctangent integral\n\
AtanInt(x) = \\int_0^x dt \\arctan(t)/t.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ATANINT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_atanint_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_atanint_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ATANINT_E undefined

  error ("GSL function gsl_sf_atanint_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ATANINT_E
}


// PKG_ADD: autoload ("gsl_sf_fermi_dirac_mhalf", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_fermi_dirac_mhalf, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_fermi_dirac_mhalf (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_fermi_dirac_mhalf (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{-1/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_MHALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_mhalf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_mhalf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_MHALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_mhalf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_MHALF_E
}


// PKG_ADD: autoload ("fermi_dirac_mhalf", which ("gsl_sf"));
DEFUN_DLD(fermi_dirac_mhalf, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} fermi_dirac_mhalf (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} fermi_dirac_mhalf (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{-1/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_MHALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_mhalf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_mhalf_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_MHALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_mhalf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_MHALF_E
}


// PKG_ADD: autoload ("gsl_sf_fermi_dirac_half", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_fermi_dirac_half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_fermi_dirac_half (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_fermi_dirac_half (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{1/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_HALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_HALF_E
}


// PKG_ADD: autoload ("fermi_dirac_half", which ("gsl_sf"));
DEFUN_DLD(fermi_dirac_half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} fermi_dirac_half (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} fermi_dirac_half (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{1/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_HALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_HALF_E
}


// PKG_ADD: autoload ("gsm_sf_fermi_dirac_3half", which ("gsl_sf"));
DEFUN_DLD(gsm_sf_fermi_dirac_3half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsm_sf_fermi_dirac_3half (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsm_sf_fermi_dirac_3half (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{3/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_3HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_3half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_3half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_3HALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_3half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_3HALF_E
}


// PKG_ADD: autoload ("fermi_dirac_3half", which ("gsl_sf"));
DEFUN_DLD(fermi_dirac_3half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} fermi_dirac_3half (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} fermi_dirac_3half (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral F_@{3/2@}(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_3HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_3half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_fermi_dirac_3half_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_3HALF_E undefined

  error ("GSL function gsl_sf_fermi_dirac_3half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_3HALF_E
}


// PKG_ADD: autoload ("gsl_sf_gamma", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gamma, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gamma (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gamma (@dots{})\n\
\n\
Computes the Gamma function \\Gamma(x), subject to x not\n\
being a negative integer. The function is computed using the real\n\
Lanczos method. The maximum value of x such that \\Gamma(x) is not\n\
considered an overflow is given by the macro GSL_SF_GAMMA_XMAX and is 171.0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_E undefined

  error ("GSL function gsl_sf_gamma_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_E
}


// PKG_ADD: autoload ("gamma_gsl", which ("gsl_sf"));
DEFUN_DLD(gamma_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gamma_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gamma_gsl (@dots{})\n\
\n\
Computes the Gamma function \\Gamma(x), subject to x not\n\
being a negative integer. The function is computed using the real\n\
Lanczos method. The maximum value of x such that \\Gamma(x) is not\n\
considered an overflow is given by the macro GSL_SF_GAMMA_XMAX and is 171.0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_E undefined

  error ("GSL function gsl_sf_gamma_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_E
}


// PKG_ADD: autoload ("gsl_sf_lngamma", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lngamma, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lngamma (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lngamma (@dots{})\n\
\n\
Computes the logarithm of the Gamma function,\n\
\\log(\\Gamma(x)), subject to x not a being negative integer.\n\
For x<0 the real part of \\log(\\Gamma(x)) is returned, which is\n\
equivalent to \\log(|\\Gamma(x)|). The function is computed using\n\
the real Lanczos method.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNGAMMA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lngamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lngamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNGAMMA_E undefined

  error ("GSL function gsl_sf_lngamma_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNGAMMA_E
}


// PKG_ADD: autoload ("lngamma_gsl", which ("gsl_sf"));
DEFUN_DLD(lngamma_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lngamma_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lngamma_gsl (@dots{})\n\
\n\
Computes the logarithm of the Gamma function,\n\
\\log(\\Gamma(x)), subject to x not a being negative integer.\n\
For x<0 the real part of \\log(\\Gamma(x)) is returned, which is\n\
equivalent to \\log(|\\Gamma(x)|). The function is computed using\n\
the real Lanczos method.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNGAMMA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lngamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lngamma_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNGAMMA_E undefined

  error ("GSL function gsl_sf_lngamma_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNGAMMA_E
}


// PKG_ADD: autoload ("gsl_sf_gammastar", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gammastar, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gammastar (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gammastar (@dots{})\n\
\n\
Computes the regulated Gamma Function \\Gamma^*(x)\n\
for x > 0. The regulated gamma function is given by,\n\
\n\
\\Gamma^*(x) = \\Gamma(x)/(\\sqrt@{2\\pi@} x^@{(x-1/2)@} \\exp(-x))\n\
            = (1 + (1/12x) + ...)  for x \\to \\infty\n\
\n\
and is a useful suggestion of Temme.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMASTAR_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammastar_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammastar_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMASTAR_E undefined

  error ("GSL function gsl_sf_gammastar_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMASTAR_E
}


// PKG_ADD: autoload ("gammastar", which ("gsl_sf"));
DEFUN_DLD(gammastar, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gammastar (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gammastar (@dots{})\n\
\n\
Computes the regulated Gamma Function \\Gamma^*(x)\n\
for x > 0. The regulated gamma function is given by,\n\
\n\
\\Gamma^*(x) = \\Gamma(x)/(\\sqrt@{2\\pi@} x^@{(x-1/2)@} \\exp(-x))\n\
            = (1 + (1/12x) + ...)  for x \\to \\infty\n\
\n\
and is a useful suggestion of Temme.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMASTAR_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammastar_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammastar_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMASTAR_E undefined

  error ("GSL function gsl_sf_gammastar_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMASTAR_E
}


// PKG_ADD: autoload ("gsl_sf_gammainv", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gammainv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gammainv (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gammainv (@dots{})\n\
\n\
Computes the reciprocal of the gamma function, 1/\\Gamma(x) using the real Lanczos method.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMAINV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammainv_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammainv_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMAINV_E undefined

  error ("GSL function gsl_sf_gammainv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMAINV_E
}


// PKG_ADD: autoload ("gammainv_gsl", which ("gsl_sf"));
DEFUN_DLD(gammainv_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gammainv_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gammainv_gsl (@dots{})\n\
\n\
Computes the reciprocal of the gamma function, 1/\\Gamma(x) using the real Lanczos method.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMAINV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammainv_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_gammainv_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMAINV_E undefined

  error ("GSL function gsl_sf_gammainv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMAINV_E
}


// PKG_ADD: autoload ("gsl_sf_lambert_W0", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lambert_W0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lambert_W0 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lambert_W0 (@dots{})\n\
\n\
These compute the principal branch of the Lambert W function, W_0(x).\n\
\n\
Lambert\\'s W functions, W(x), are defined to be solutions of the\n\
export octave_name=gammastar\n\
export    funcname=gsl_sf_gammastar_e\n\
equation W(x) \\exp(W(x)) = x. This function has multiple branches\n\
for x < 0; however, it has only two real-valued branches.\n\
We define W_0(x) to be the principal branch, where W > -1 for x < 0,\n\
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LAMBERT_W0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_W0_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_W0_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LAMBERT_W0_E undefined

  error ("GSL function gsl_sf_lambert_W0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LAMBERT_W0_E
}


// PKG_ADD: autoload ("lambert_W0", which ("gsl_sf"));
DEFUN_DLD(lambert_W0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lambert_W0 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lambert_W0 (@dots{})\n\
\n\
These compute the principal branch of the Lambert W function, W_0(x).\n\
\n\
Lambert\\'s W functions, W(x), are defined to be solutions of the\n\
export octave_name=gammastar\n\
export    funcname=gsl_sf_gammastar_e\n\
equation W(x) \\exp(W(x)) = x. This function has multiple branches\n\
for x < 0; however, it has only two real-valued branches.\n\
We define W_0(x) to be the principal branch, where W > -1 for x < 0,\n\
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LAMBERT_W0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_W0_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_W0_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LAMBERT_W0_E undefined

  error ("GSL function gsl_sf_lambert_W0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LAMBERT_W0_E
}


// PKG_ADD: autoload ("gsl_sf_lambert_Wm1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lambert_Wm1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lambert_Wm1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lambert_Wm1 (@dots{})\n\
\n\
These compute the secondary real-valued branch of the Lambert\n\
W function, W_@{-1@}(x).\n\
\n\
Lambert\\'s W functions, W(x), are defined to be solutions of the\n\
equation W(x) \\exp(W(x)) = x. This function has multiple branches\n\
for x < 0; however, it has only two real-valued branches.\n\
We define W_0(x) to be the principal branch, where W > -1 for x < 0,\n\
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LAMBERT_WM1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_Wm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_Wm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LAMBERT_WM1_E undefined

  error ("GSL function gsl_sf_lambert_Wm1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LAMBERT_WM1_E
}


// PKG_ADD: autoload ("lambert_Wm1", which ("gsl_sf"));
DEFUN_DLD(lambert_Wm1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lambert_Wm1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lambert_Wm1 (@dots{})\n\
\n\
These compute the secondary real-valued branch of the Lambert\n\
W function, W_@{-1@}(x).\n\
\n\
Lambert\\'s W functions, W(x), are defined to be solutions of the\n\
equation W(x) \\exp(W(x)) = x. This function has multiple branches\n\
for x < 0; however, it has only two real-valued branches.\n\
We define W_0(x) to be the principal branch, where W > -1 for x < 0,\n\
and W_@{-1@}(x) to be the other real branch, where W < -1 for x < 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LAMBERT_WM1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_Wm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lambert_Wm1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LAMBERT_WM1_E undefined

  error ("GSL function gsl_sf_lambert_Wm1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LAMBERT_WM1_E
}


// PKG_ADD: autoload ("gsl_sf_log_1plusx", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_log_1plusx, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_log_1plusx (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_log_1plusx (@dots{})\n\
\n\
Computes \\log(1 + x) for x > -1 using an algorithm that\n\
is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_1PLUSX_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_1PLUSX_E undefined

  error ("GSL function gsl_sf_log_1plusx_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_1PLUSX_E
}


// PKG_ADD: autoload ("log_1plusx", which ("gsl_sf"));
DEFUN_DLD(log_1plusx, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} log_1plusx (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} log_1plusx (@dots{})\n\
\n\
Computes \\log(1 + x) for x > -1 using an algorithm that\n\
is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_1PLUSX_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_1PLUSX_E undefined

  error ("GSL function gsl_sf_log_1plusx_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_1PLUSX_E
}


// PKG_ADD: autoload ("gsl_sf_log_1plusx_mx", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_log_1plusx_mx, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_log_1plusx_mx (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_log_1plusx_mx (@dots{})\n\
\n\
Computes \\log(1 + x) - x for x > -1 using an algorithm\n\
that is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_1PLUSX_MX_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_mx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_mx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_1PLUSX_MX_E undefined

  error ("GSL function gsl_sf_log_1plusx_mx_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_1PLUSX_MX_E
}


// PKG_ADD: autoload ("log_1plusx_mx", which ("gsl_sf"));
DEFUN_DLD(log_1plusx_mx, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} log_1plusx_mx (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} log_1plusx_mx (@dots{})\n\
\n\
Computes \\log(1 + x) - x for x > -1 using an algorithm\n\
that is accurate for small x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LOG_1PLUSX_MX_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_mx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_log_1plusx_mx_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LOG_1PLUSX_MX_E undefined

  error ("GSL function gsl_sf_log_1plusx_mx_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LOG_1PLUSX_MX_E
}


// PKG_ADD: autoload ("gsl_sf_psi", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_psi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_psi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_psi (@dots{})\n\
\n\
Computes the digamma function \\psi(x) for general x,\n\
x \\ne 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_E undefined

  error ("GSL function gsl_sf_psi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_E
}


// PKG_ADD: autoload ("psi", which ("gsl_sf"));
DEFUN_DLD(psi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} psi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} psi (@dots{})\n\
\n\
Computes the digamma function \\psi(x) for general x,\n\
x \\ne 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_E undefined

  error ("GSL function gsl_sf_psi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_E
}


// PKG_ADD: autoload ("gsl_sf_psi_1piy", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_psi_1piy, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_psi_1piy (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_psi_1piy (@dots{})\n\
\n\
Computes the real part of the digamma function on\n\
the line 1+i y, Re[\\psi(1 + i y)].\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_1PIY_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_1piy_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_1piy_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_1PIY_E undefined

  error ("GSL function gsl_sf_psi_1piy_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_1PIY_E
}


// PKG_ADD: autoload ("psi_1piy", which ("gsl_sf"));
DEFUN_DLD(psi_1piy, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} psi_1piy (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} psi_1piy (@dots{})\n\
\n\
Computes the real part of the digamma function on\n\
the line 1+i y, Re[\\psi(1 + i y)].\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_1PIY_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_1piy_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_psi_1piy_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_1PIY_E undefined

  error ("GSL function gsl_sf_psi_1piy_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_1PIY_E
}


// PKG_ADD: autoload ("gsl_sf_synchrotron_1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_synchrotron_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_synchrotron_1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_synchrotron_1 (@dots{})\n\
\n\
Computes the first synchrotron function\n\
x \\int_x^\\infty dt K_@{5/3@}(t) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SYNCHROTRON_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SYNCHROTRON_1_E undefined

  error ("GSL function gsl_sf_synchrotron_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SYNCHROTRON_1_E
}


// PKG_ADD: autoload ("synchrotron_1", which ("gsl_sf"));
DEFUN_DLD(synchrotron_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} synchrotron_1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} synchrotron_1 (@dots{})\n\
\n\
Computes the first synchrotron function\n\
x \\int_x^\\infty dt K_@{5/3@}(t) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SYNCHROTRON_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SYNCHROTRON_1_E undefined

  error ("GSL function gsl_sf_synchrotron_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SYNCHROTRON_1_E
}


// PKG_ADD: autoload ("gsl_sf_synchrotron_2", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_synchrotron_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_synchrotron_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_synchrotron_2 (@dots{})\n\
\n\
Computes the second synchrotron function\n\
x K_@{2/3@}(x) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SYNCHROTRON_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SYNCHROTRON_2_E undefined

  error ("GSL function gsl_sf_synchrotron_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SYNCHROTRON_2_E
}


// PKG_ADD: autoload ("synchrotron_2", which ("gsl_sf"));
DEFUN_DLD(synchrotron_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} synchrotron_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} synchrotron_2 (@dots{})\n\
\n\
Computes the second synchrotron function\n\
x K_@{2/3@}(x) for x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SYNCHROTRON_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_synchrotron_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SYNCHROTRON_2_E undefined

  error ("GSL function gsl_sf_synchrotron_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SYNCHROTRON_2_E
}


// PKG_ADD: autoload ("gsl_sf_transport_2", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_transport_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_transport_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_transport_2 (@dots{})\n\
\n\
Computes the transport function J(2,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_2_E undefined

  error ("GSL function gsl_sf_transport_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_2_E
}


// PKG_ADD: autoload ("transport_2", which ("gsl_sf"));
DEFUN_DLD(transport_2, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} transport_2 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} transport_2 (@dots{})\n\
\n\
Computes the transport function J(2,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_2_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_2_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_2_E undefined

  error ("GSL function gsl_sf_transport_2_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_2_E
}


// PKG_ADD: autoload ("gsl_sf_transport_3", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_transport_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_transport_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_transport_3 (@dots{})\n\
\n\
Computes the transport function J(3,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_3_E undefined

  error ("GSL function gsl_sf_transport_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_3_E
}


// PKG_ADD: autoload ("transport_3", which ("gsl_sf"));
DEFUN_DLD(transport_3, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} transport_3 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} transport_3 (@dots{})\n\
\n\
Computes the transport function J(3,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_3_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_3_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_3_E undefined

  error ("GSL function gsl_sf_transport_3_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_3_E
}


// PKG_ADD: autoload ("gsl_sf_transport_4", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_transport_4, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_transport_4 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_transport_4 (@dots{})\n\
\n\
Computes the transport function J(4,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_4_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_4_E undefined

  error ("GSL function gsl_sf_transport_4_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_4_E
}


// PKG_ADD: autoload ("transport_4", which ("gsl_sf"));
DEFUN_DLD(transport_4, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} transport_4 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} transport_4 (@dots{})\n\
\n\
Computes the transport function J(4,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_4_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_4_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_4_E undefined

  error ("GSL function gsl_sf_transport_4_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_4_E
}


// PKG_ADD: autoload ("gsl_sf_transport_5", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_transport_5, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_transport_5 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_transport_5 (@dots{})\n\
\n\
Computes the transport function J(5,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_5_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_5_E undefined

  error ("GSL function gsl_sf_transport_5_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_5_E
}


// PKG_ADD: autoload ("transport_5", which ("gsl_sf"));
DEFUN_DLD(transport_5, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} transport_5 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} transport_5 (@dots{})\n\
\n\
Computes the transport function J(5,x).\n\
\n\
The transport functions J(n,x) are defined by the integral\n\
representations J(n,x) := \\int_0^x dt t^n e^t /(e^t - 1)^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TRANSPORT_5_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_transport_5_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TRANSPORT_5_E undefined

  error ("GSL function gsl_sf_transport_5_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TRANSPORT_5_E
}


// PKG_ADD: autoload ("gsl_sf_sinc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_sinc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_sinc (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_sinc (@dots{})\n\
\n\
Computes \\sinc(x) = \\sin(\\pi x) / (\\pi x) for any value of x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SINC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_sinc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_sinc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SINC_E undefined

  error ("GSL function gsl_sf_sinc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SINC_E
}


// PKG_ADD: autoload ("sinc_gsl", which ("gsl_sf"));
DEFUN_DLD(sinc_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} sinc_gsl (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} sinc_gsl (@dots{})\n\
\n\
Computes \\sinc(x) = \\sin(\\pi x) / (\\pi x) for any value of x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_SINC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_sinc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_sinc_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_SINC_E undefined

  error ("GSL function gsl_sf_sinc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_SINC_E
}


// PKG_ADD: autoload ("gsl_sf_lnsinh", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lnsinh, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lnsinh (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lnsinh (@dots{})\n\
\n\
Computes \\log(\\sinh(x)) for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNSINH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lnsinh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lnsinh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNSINH_E undefined

  error ("GSL function gsl_sf_lnsinh_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNSINH_E
}


// PKG_ADD: autoload ("lnsinh", which ("gsl_sf"));
DEFUN_DLD(lnsinh, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lnsinh (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lnsinh (@dots{})\n\
\n\
Computes \\log(\\sinh(x)) for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNSINH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lnsinh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lnsinh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNSINH_E undefined

  error ("GSL function gsl_sf_lnsinh_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNSINH_E
}


// PKG_ADD: autoload ("gsl_sf_lncosh", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lncosh, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lncosh (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lncosh (@dots{})\n\
\n\
Computes \\log(\\cosh(x)) for any x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNCOSH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lncosh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lncosh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNCOSH_E undefined

  error ("GSL function gsl_sf_lncosh_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNCOSH_E
}


// PKG_ADD: autoload ("lncosh", which ("gsl_sf"));
DEFUN_DLD(lncosh, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lncosh (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lncosh (@dots{})\n\
\n\
Computes \\log(\\cosh(x)) for any x.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNCOSH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lncosh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_lncosh_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNCOSH_E undefined

  error ("GSL function gsl_sf_lncosh_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNCOSH_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Jn", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Jn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Jn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Jn (@dots{})\n\
\n\
Computes the regular cylindrical Bessel function of\n\
order n, J_n(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Jn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Jn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JN_E undefined

  error ("GSL function gsl_sf_bessel_Jn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JN_E
}


// PKG_ADD: autoload ("bessel_Jn", which ("gsl_sf"));
DEFUN_DLD(bessel_Jn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Jn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Jn (@dots{})\n\
\n\
Computes the regular cylindrical Bessel function of\n\
order n, J_n(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Jn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Jn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JN_E undefined

  error ("GSL function gsl_sf_bessel_Jn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JN_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Yn", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Yn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Yn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Yn (@dots{})\n\
\n\
Computes the irregular cylindrical Bessel function of\n\
order n, Y_n(x), for x>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Yn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Yn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YN_E undefined

  error ("GSL function gsl_sf_bessel_Yn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YN_E
}


// PKG_ADD: autoload ("bessel_Yn", which ("gsl_sf"));
DEFUN_DLD(bessel_Yn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Yn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Yn (@dots{})\n\
\n\
Computes the irregular cylindrical Bessel function of\n\
order n, Y_n(x), for x>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Yn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Yn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YN_E undefined

  error ("GSL function gsl_sf_bessel_Yn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YN_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_In", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_In, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_In (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_In (@dots{})\n\
\n\
Computes the regular modified cylindrical Bessel\n\
function of order n, I_n(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IN_E undefined

  error ("GSL function gsl_sf_bessel_In_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_E
}


// PKG_ADD: autoload ("bessel_In", which ("gsl_sf"));
DEFUN_DLD(bessel_In, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_In (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_In (@dots{})\n\
\n\
Computes the regular modified cylindrical Bessel\n\
function of order n, I_n(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IN_E undefined

  error ("GSL function gsl_sf_bessel_In_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_In_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_In_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_In_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_In_scaled (@dots{})\n\
\n\
Computes the scaled regular modified cylindrical Bessel\n\
function of order n, \\exp(-|x|) I_n(x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IN_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_In_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_SCALED_E
}


// PKG_ADD: autoload ("bessel_In_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_In_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_In_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_In_scaled (@dots{})\n\
\n\
Computes the scaled regular modified cylindrical Bessel\n\
function of order n, \\exp(-|x|) I_n(x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_In_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IN_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_In_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Kn", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Kn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Kn (@dots{})\n\
\n\
Computes the irregular modified cylindrical Bessel\n\
function of order n, K_n(x), for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KN_E undefined

  error ("GSL function gsl_sf_bessel_Kn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_E
}


// PKG_ADD: autoload ("bessel_Kn", which ("gsl_sf"));
DEFUN_DLD(bessel_Kn, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Kn (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Kn (@dots{})\n\
\n\
Computes the irregular modified cylindrical Bessel\n\
function of order n, K_n(x), for x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KN_E undefined

  error ("GSL function gsl_sf_bessel_Kn_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Kn_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Kn_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Kn_scaled (@dots{})\n\
\n\
\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KN_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Kn_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_SCALED_E
}


// PKG_ADD: autoload ("bessel_Kn_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_Kn_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Kn_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Kn_scaled (@dots{})\n\
\n\
\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_Kn_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KN_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Kn_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_jl", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_jl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_jl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_jl (@dots{})\n\
\n\
Computes the regular spherical Bessel function of\n\
order l, j_l(x), for l >= 0 and x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_jl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_jl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JL_E undefined

  error ("GSL function gsl_sf_bessel_jl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JL_E
}


// PKG_ADD: autoload ("bessel_jl", which ("gsl_sf"));
DEFUN_DLD(bessel_jl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_jl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_jl (@dots{})\n\
\n\
Computes the regular spherical Bessel function of\n\
order l, j_l(x), for l >= 0 and x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_jl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_jl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JL_E undefined

  error ("GSL function gsl_sf_bessel_jl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JL_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_yl", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_yl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_yl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_yl (@dots{})\n\
\n\
Computes the irregular spherical Bessel function of\n\
order l, y_l(x), for l >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_yl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_yl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YL_E undefined

  error ("GSL function gsl_sf_bessel_yl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YL_E
}


// PKG_ADD: autoload ("bessel_yl", which ("gsl_sf"));
DEFUN_DLD(bessel_yl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_yl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_yl (@dots{})\n\
\n\
Computes the irregular spherical Bessel function of\n\
order l, y_l(x), for l >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_yl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_yl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YL_E undefined

  error ("GSL function gsl_sf_bessel_yl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YL_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_il_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_il_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_il_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_il_scaled (@dots{})\n\
\n\
Computes the scaled regular modified spherical Bessel\n\
function of order l, \\exp(-|x|) i_l(x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IL_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_il_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_il_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IL_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_il_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IL_SCALED_E
}


// PKG_ADD: autoload ("bessel_il_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_il_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_il_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_il_scaled (@dots{})\n\
\n\
Computes the scaled regular modified spherical Bessel\n\
function of order l, \\exp(-|x|) i_l(x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_IL_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_il_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_il_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_IL_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_il_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IL_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_kl_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_kl_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_kl_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_kl_scaled (@dots{})\n\
\n\
Computes the scaled irregular modified spherical Bessel\n\
function of order l, \\exp(x) k_l(x), for x>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KL_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_kl_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_kl_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KL_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_kl_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KL_SCALED_E
}


// PKG_ADD: autoload ("bessel_kl_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_kl_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_kl_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_kl_scaled (@dots{})\n\
\n\
Computes the scaled irregular modified spherical Bessel\n\
function of order l, \\exp(x) k_l(x), for x>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KL_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_kl_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_kl_scaled_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KL_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_kl_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KL_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_exprel_n", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_exprel_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_exprel_n (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_exprel_n (@dots{})\n\
\n\
Computes the N-relative exponential, which is the n-th\n\
generalization of the functions gsl_sf_exprel and gsl_sf_exprel2. The\n\
N-relative exponential is given by,\n\
\n\
exprel_N(x) = N!/x^N (\\exp(x) - \\sum_@{k=0@}^@{N-1@} x^k/k!)\n\
            = 1 + x/(N+1) + x^2/((N+1)(N+2)) + ...\n\
            = 1F1 (1,1+N,x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_exprel_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_exprel_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_N_E undefined

  error ("GSL function gsl_sf_exprel_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_N_E
}


// PKG_ADD: autoload ("exprel_n", which ("gsl_sf"));
DEFUN_DLD(exprel_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} exprel_n (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} exprel_n (@dots{})\n\
\n\
Computes the N-relative exponential, which is the n-th\n\
generalization of the functions gsl_sf_exprel and gsl_sf_exprel2. The\n\
N-relative exponential is given by,\n\
\n\
exprel_N(x) = N!/x^N (\\exp(x) - \\sum_@{k=0@}^@{N-1@} x^k/k!)\n\
            = 1 + x/(N+1) + x^2/((N+1)(N+2)) + ...\n\
            = 1F1 (1,1+N,x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXPREL_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_exprel_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_exprel_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXPREL_N_E undefined

  error ("GSL function gsl_sf_exprel_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXPREL_N_E
}


// PKG_ADD: autoload ("gsl_sf_fermi_dirac_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_fermi_dirac_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_fermi_dirac_int (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_fermi_dirac_int (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral with an\n\
integer index of j, F_j(x) = (1/\\Gamma(j+1)) \\int_0^\\infty dt (t^j\n\
/(\\exp(t-x)+1)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fermi_dirac_int_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fermi_dirac_int_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_INT_E undefined

  error ("GSL function gsl_sf_fermi_dirac_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_INT_E
}


// PKG_ADD: autoload ("fermi_dirac_int", which ("gsl_sf"));
DEFUN_DLD(fermi_dirac_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} fermi_dirac_int (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} fermi_dirac_int (@dots{})\n\
\n\
Computes the complete Fermi-Dirac integral with an\n\
integer index of j, F_j(x) = (1/\\Gamma(j+1)) \\int_0^\\infty dt (t^j\n\
/(\\exp(t-x)+1)).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fermi_dirac_int_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fermi_dirac_int_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_INT_E undefined

  error ("GSL function gsl_sf_fermi_dirac_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_INT_E
}


// PKG_ADD: autoload ("gsl_sf_taylorcoeff", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_taylorcoeff, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_taylorcoeff (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_taylorcoeff (@dots{})\n\
\n\
Computes the Taylor coefficient x^n / n!\n\
for x >= 0, n >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TAYLORCOEFF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_taylorcoeff_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_taylorcoeff_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TAYLORCOEFF_E undefined

  error ("GSL function gsl_sf_taylorcoeff_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TAYLORCOEFF_E
}


// PKG_ADD: autoload ("taylorcoeff", which ("gsl_sf"));
DEFUN_DLD(taylorcoeff, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} taylorcoeff (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} taylorcoeff (@dots{})\n\
\n\
Computes the Taylor coefficient x^n / n!\n\
for x >= 0, n >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_TAYLORCOEFF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_taylorcoeff_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_taylorcoeff_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_TAYLORCOEFF_E undefined

  error ("GSL function gsl_sf_taylorcoeff_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_TAYLORCOEFF_E
}


// PKG_ADD: autoload ("gsl_sf_legendre_Pl", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Pl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_Pl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_legendre_Pl (@dots{})\n\
\n\
These functions evaluate the Legendre polynomial P_l(x) for a specific\n\
value of l, x subject to l >= 0, |x| <= 1\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Pl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Pl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_PL_E undefined

  error ("GSL function gsl_sf_legendre_Pl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PL_E
}


// PKG_ADD: autoload ("legendre_Pl", which ("gsl_sf"));
DEFUN_DLD(legendre_Pl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} legendre_Pl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} legendre_Pl (@dots{})\n\
\n\
These functions evaluate the Legendre polynomial P_l(x) for a specific\n\
value of l, x subject to l >= 0, |x| <= 1\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Pl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Pl_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_PL_E undefined

  error ("GSL function gsl_sf_legendre_Pl_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PL_E
}


// PKG_ADD: autoload ("gsl_sf_legendre_Ql", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Ql, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_Ql (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_legendre_Ql (@dots{})\n\
\n\
Computes the Legendre function Q_l(x) for x > -1, x != 1\n\
and l >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_QL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Ql_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Ql_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_QL_E undefined

  error ("GSL function gsl_sf_legendre_Ql_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_QL_E
}


// PKG_ADD: autoload ("legendre_Ql", which ("gsl_sf"));
DEFUN_DLD(legendre_Ql, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} legendre_Ql (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} legendre_Ql (@dots{})\n\
\n\
Computes the Legendre function Q_l(x) for x > -1, x != 1\n\
and l >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_QL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Ql_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Ql_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_QL_E undefined

  error ("GSL function gsl_sf_legendre_Ql_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_QL_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_a", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_a, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_a (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_a (@dots{})\n\
\n\
Computes the characteristic values a_n(q) of the\n\
Mathieu function ce_n(q,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_A_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_a_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_a_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_A_E undefined

  error ("GSL function gsl_sf_mathieu_a_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_A_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_b", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_b, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_b (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_b (@dots{})\n\
\n\
Computes the characteristic values b_n(q) of the\n\
Mathieu function se_n(q,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_B_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_b_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_b_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_B_E undefined

  error ("GSL function gsl_sf_mathieu_b_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_B_E
}


// PKG_ADD: autoload ("gsl_sf_psi_n", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_psi_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_psi_n (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_psi_n (@dots{})\n\
\n\
Computes the polygamma function \\psi^@{(m)@}(x)\n\
for m >= 0, x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_N_E undefined

  error ("GSL function gsl_sf_psi_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_N_E
}


// PKG_ADD: autoload ("psi_n", which ("gsl_sf"));
DEFUN_DLD(psi_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} psi_n (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} psi_n (@dots{})\n\
\n\
Computes the polygamma function \\psi^@{(m)@}(x)\n\
for m >= 0, x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_n_e (x1, arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_N_E undefined

  error ("GSL function gsl_sf_psi_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_N_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Jnu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Jnu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Jnu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Jnu (@dots{})\n\
\n\
Computes the regular cylindrical Bessel function of\n\
fractional order nu, J_\\nu(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Jnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Jnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JNU_E undefined

  error ("GSL function gsl_sf_bessel_Jnu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JNU_E
}


// PKG_ADD: autoload ("bessel_Jnu", which ("gsl_sf"));
DEFUN_DLD(bessel_Jnu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Jnu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Jnu (@dots{})\n\
\n\
Computes the regular cylindrical Bessel function of\n\
fractional order nu, J_\\nu(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_JNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Jnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Jnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_JNU_E undefined

  error ("GSL function gsl_sf_bessel_Jnu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JNU_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Ynu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Ynu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Ynu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Ynu (@dots{})\n\
\n\
Computes the irregular cylindrical Bessel function of\n\
fractional order nu, Y_\\nu(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Ynu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Ynu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YNU_E undefined

  error ("GSL function gsl_sf_bessel_Ynu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YNU_E
}


// PKG_ADD: autoload ("bessel_Ynu", which ("gsl_sf"));
DEFUN_DLD(bessel_Ynu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Ynu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Ynu (@dots{})\n\
\n\
Computes the irregular cylindrical Bessel function of\n\
fractional order nu, Y_\\nu(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_YNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Ynu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Ynu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_YNU_E undefined

  error ("GSL function gsl_sf_bessel_Ynu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YNU_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Inu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Inu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Inu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Inu (@dots{})\n\
\n\
Computes the regular modified Bessel function of\n\
fractional order nu, I_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_INU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_INU_E undefined

  error ("GSL function gsl_sf_bessel_Inu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_INU_E
}


// PKG_ADD: autoload ("bessel_Inu", which ("gsl_sf"));
DEFUN_DLD(bessel_Inu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Inu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Inu (@dots{})\n\
\n\
Computes the regular modified Bessel function of\n\
fractional order nu, I_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_INU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_INU_E undefined

  error ("GSL function gsl_sf_bessel_Inu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_INU_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Inu_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Inu_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Inu_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Inu_scaled (@dots{})\n\
\n\
Computes the scaled regular modified Bessel function of\n\
fractional order nu, \\exp(-|x|)I_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_INU_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_INU_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Inu_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_INU_SCALED_E
}


// PKG_ADD: autoload ("bessel_Inu_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_Inu_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Inu_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Inu_scaled (@dots{})\n\
\n\
Computes the scaled regular modified Bessel function of\n\
fractional order nu, \\exp(-|x|)I_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_INU_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Inu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_INU_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Inu_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_INU_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Knu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Knu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Knu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Knu (@dots{})\n\
\n\
Computes the irregular modified Bessel function of\n\
fractional order nu, K_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KNU_E undefined

  error ("GSL function gsl_sf_bessel_Knu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KNU_E
}


// PKG_ADD: autoload ("bessel_Knu", which ("gsl_sf"));
DEFUN_DLD(bessel_Knu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Knu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Knu (@dots{})\n\
\n\
Computes the irregular modified Bessel function of\n\
fractional order nu, K_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KNU_E undefined

  error ("GSL function gsl_sf_bessel_Knu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KNU_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_lnKnu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_lnKnu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_lnKnu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_lnKnu (@dots{})\n\
\n\
Computes the logarithm of the irregular modified Bessel\n\
function of fractional order nu, \\ln(K_\\nu(x)) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_LNKNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_lnKnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_lnKnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_LNKNU_E undefined

  error ("GSL function gsl_sf_bessel_lnKnu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_LNKNU_E
}


// PKG_ADD: autoload ("bessel_lnKnu", which ("gsl_sf"));
DEFUN_DLD(bessel_lnKnu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_lnKnu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_lnKnu (@dots{})\n\
\n\
Computes the logarithm of the irregular modified Bessel\n\
function of fractional order nu, \\ln(K_\\nu(x)) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_LNKNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_lnKnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_lnKnu_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_LNKNU_E undefined

  error ("GSL function gsl_sf_bessel_lnKnu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_LNKNU_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_Knu_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Knu_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Knu_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_Knu_scaled (@dots{})\n\
\n\
Computes the scaled irregular modified Bessel function\n\
of fractional order nu, \\exp(+|x|) K_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KNU_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KNU_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Knu_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KNU_SCALED_E
}


// PKG_ADD: autoload ("bessel_Knu_scaled", which ("gsl_sf"));
DEFUN_DLD(bessel_Knu_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_Knu_scaled (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_Knu_scaled (@dots{})\n\
\n\
Computes the scaled irregular modified Bessel function\n\
of fractional order nu, \\exp(+|x|) K_\\nu(x) for x>0, \\nu>0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_KNU_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_bessel_Knu_scaled_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_KNU_SCALED_E undefined

  error ("GSL function gsl_sf_bessel_Knu_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KNU_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_exp_mult", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_exp_mult, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_exp_mult (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_exp_mult (@dots{})\n\
\n\
These routines exponentiate x and multiply by the factor y to return\n\
the product y \\exp(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXP_MULT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_exp_mult_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_exp_mult_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXP_MULT_E undefined

  error ("GSL function gsl_sf_exp_mult_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXP_MULT_E
}


// PKG_ADD: autoload ("exp_mult", which ("gsl_sf"));
DEFUN_DLD(exp_mult, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} exp_mult (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} exp_mult (@dots{})\n\
\n\
These routines exponentiate x and multiply by the factor y to return\n\
the product y \\exp(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_EXP_MULT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_exp_mult_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_exp_mult_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_EXP_MULT_E undefined

  error ("GSL function gsl_sf_exp_mult_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_EXP_MULT_E
}


// PKG_ADD: autoload ("gsl_sf_fermi_dirac_inc_0", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_fermi_dirac_inc_0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_fermi_dirac_inc_0 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_fermi_dirac_inc_0 (@dots{})\n\
\n\
Computes the incomplete Fermi-Dirac integral with an\n\
index of zero, F_0(x,b) = \\ln(1 + e^@{b-x@}) - (b-x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_INC_0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_fermi_dirac_inc_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_fermi_dirac_inc_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_INC_0_E undefined

  error ("GSL function gsl_sf_fermi_dirac_inc_0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_INC_0_E
}


// PKG_ADD: autoload ("fermi_dirac_inc_0", which ("gsl_sf"));
DEFUN_DLD(fermi_dirac_inc_0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} fermi_dirac_inc_0 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} fermi_dirac_inc_0 (@dots{})\n\
\n\
Computes the incomplete Fermi-Dirac integral with an\n\
index of zero, F_0(x,b) = \\ln(1 + e^@{b-x@}) - (b-x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FERMI_DIRAC_INC_0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_fermi_dirac_inc_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_fermi_dirac_inc_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FERMI_DIRAC_INC_0_E undefined

  error ("GSL function gsl_sf_fermi_dirac_inc_0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FERMI_DIRAC_INC_0_E
}


// PKG_ADD: autoload ("gsl_sf_poch", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_poch, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_poch (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_poch (@dots{})\n\
\n\
Computes the Pochhammer symbol\n\
\n\
(a)_x := \\Gamma(a + x)/\\Gamma(a),\n\
\n\
subject to a and a+x not being negative integers. The Pochhammer\n\
symbol is also known as the Apell symbol.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_POCH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_poch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_poch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_POCH_E undefined

  error ("GSL function gsl_sf_poch_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_POCH_E
}


// PKG_ADD: autoload ("poch", which ("gsl_sf"));
DEFUN_DLD(poch, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} poch (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} poch (@dots{})\n\
\n\
Computes the Pochhammer symbol\n\
\n\
(a)_x := \\Gamma(a + x)/\\Gamma(a),\n\
\n\
subject to a and a+x not being negative integers. The Pochhammer\n\
symbol is also known as the Apell symbol.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_POCH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_poch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_poch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_POCH_E undefined

  error ("GSL function gsl_sf_poch_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_POCH_E
}


// PKG_ADD: autoload ("gsl_sf_lnpoch", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lnpoch, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lnpoch (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lnpoch (@dots{})\n\
\n\
Computes the logarithm of the Pochhammer symbol,\n\
\\log((a)_x) = \\log(\\Gamma(a + x)/\\Gamma(a)) for a > 0, a+x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNPOCH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnpoch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnpoch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNPOCH_E undefined

  error ("GSL function gsl_sf_lnpoch_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNPOCH_E
}


// PKG_ADD: autoload ("lnpoch", which ("gsl_sf"));
DEFUN_DLD(lnpoch, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lnpoch (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lnpoch (@dots{})\n\
\n\
Computes the logarithm of the Pochhammer symbol,\n\
\\log((a)_x) = \\log(\\Gamma(a + x)/\\Gamma(a)) for a > 0, a+x > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNPOCH_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnpoch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnpoch_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNPOCH_E undefined

  error ("GSL function gsl_sf_lnpoch_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNPOCH_E
}


// PKG_ADD: autoload ("gsl_sf_pochrel", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_pochrel, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_pochrel (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_pochrel (@dots{})\n\
\n\
Computes the relative Pochhammer symbol ((a,x) - 1)/x\n\
where (a,x) = (a)_x := \\Gamma(a + x)/\\Gamma(a).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_POCHREL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_pochrel_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_pochrel_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_POCHREL_E undefined

  error ("GSL function gsl_sf_pochrel_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_POCHREL_E
}


// PKG_ADD: autoload ("pochrel", which ("gsl_sf"));
DEFUN_DLD(pochrel, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} pochrel (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} pochrel (@dots{})\n\
\n\
Computes the relative Pochhammer symbol ((a,x) - 1)/x\n\
where (a,x) = (a)_x := \\Gamma(a + x)/\\Gamma(a).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_POCHREL_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_pochrel_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_pochrel_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_POCHREL_E undefined

  error ("GSL function gsl_sf_pochrel_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_POCHREL_E
}


// PKG_ADD: autoload ("gsl_sf_gamma_inc_Q", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gamma_inc_Q, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gamma_inc_Q (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gamma_inc_Q (@dots{})\n\
\n\
Computes the normalized incomplete Gamma Function\n\
Q(a,x) = 1/\\Gamma(a) \\int_x\\infty dt t^@{a-1@} \\exp(-t) for a > 0, x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_Q_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_Q_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_Q_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_Q_E undefined

  error ("GSL function gsl_sf_gamma_inc_Q_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_Q_E
}


// PKG_ADD: autoload ("gamma_inc_Q", which ("gsl_sf"));
DEFUN_DLD(gamma_inc_Q, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gamma_inc_Q (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gamma_inc_Q (@dots{})\n\
\n\
Computes the normalized incomplete Gamma Function\n\
Q(a,x) = 1/\\Gamma(a) \\int_x\\infty dt t^@{a-1@} \\exp(-t) for a > 0, x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_Q_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_Q_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_Q_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_Q_E undefined

  error ("GSL function gsl_sf_gamma_inc_Q_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_Q_E
}


// PKG_ADD: autoload ("gsl_sf_gamma_inc_P", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gamma_inc_P, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gamma_inc_P (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gamma_inc_P (@dots{})\n\
\n\
Computes the complementary normalized incomplete Gamma\n\
Function P(a,x) = 1/\\Gamma(a) \\int_0^x dt t^@{a-1@} \\exp(-t)\n\
for a > 0, x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_P_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_P_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_P_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_P_E undefined

  error ("GSL function gsl_sf_gamma_inc_P_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_P_E
}


// PKG_ADD: autoload ("gamma_inc_P", which ("gsl_sf"));
DEFUN_DLD(gamma_inc_P, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gamma_inc_P (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gamma_inc_P (@dots{})\n\
\n\
Computes the complementary normalized incomplete Gamma\n\
Function P(a,x) = 1/\\Gamma(a) \\int_0^x dt t^@{a-1@} \\exp(-t)\n\
for a > 0, x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_P_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_P_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_P_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_P_E undefined

  error ("GSL function gsl_sf_gamma_inc_P_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_P_E
}


// PKG_ADD: autoload ("gsl_sf_gamma_inc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gamma_inc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gamma_inc (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gamma_inc (@dots{})\n\
\n\
These functions compute the incomplete Gamma Function the\n\
normalization factor included in the previously defined functions:\n\
\\Gamma(a,x) = \\int_x\\infty dt t^@{a-1@} \\exp(-t) for a real and x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_E undefined

  error ("GSL function gsl_sf_gamma_inc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_E
}


// PKG_ADD: autoload ("gamma_inc", which ("gsl_sf"));
DEFUN_DLD(gamma_inc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gamma_inc (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gamma_inc (@dots{})\n\
\n\
These functions compute the incomplete Gamma Function the\n\
normalization factor included in the previously defined functions:\n\
\\Gamma(a,x) = \\int_x\\infty dt t^@{a-1@} \\exp(-t) for a real and x >= 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GAMMA_INC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_gamma_inc_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GAMMA_INC_E undefined

  error ("GSL function gsl_sf_gamma_inc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GAMMA_INC_E
}


// PKG_ADD: autoload ("gsl_sf_beta", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_beta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_beta (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_beta (@dots{})\n\
\n\
Computes the Beta Function,\n\
B(a,b) = \\Gamma(a)\\Gamma(b)/\\Gamma(a+b) for a > 0, b > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_beta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_beta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BETA_E undefined

  error ("GSL function gsl_sf_beta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BETA_E
}


// PKG_ADD: autoload ("beta_gsl", which ("gsl_sf"));
DEFUN_DLD(beta_gsl, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} beta_gsl (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} beta_gsl (@dots{})\n\
\n\
Computes the Beta Function,\n\
B(a,b) = \\Gamma(a)\\Gamma(b)/\\Gamma(a+b) for a > 0, b > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_beta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_beta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BETA_E undefined

  error ("GSL function gsl_sf_beta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BETA_E
}


// PKG_ADD: autoload ("gsl_sf_lnbeta", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lnbeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lnbeta (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lnbeta (@dots{})\n\
\n\
Computes the logarithm of the Beta Function,\n\
\\log(B(a,b)) for a > 0, b > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNBETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnbeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnbeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNBETA_E undefined

  error ("GSL function gsl_sf_lnbeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNBETA_E
}


// PKG_ADD: autoload ("lnbeta", which ("gsl_sf"));
DEFUN_DLD(lnbeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} lnbeta (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} lnbeta (@dots{})\n\
\n\
Computes the logarithm of the Beta Function,\n\
\\log(B(a,b)) for a > 0, b > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNBETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnbeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_lnbeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNBETA_E undefined

  error ("GSL function gsl_sf_lnbeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNBETA_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_0F1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_0F1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_0F1 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_0F1 (@dots{})\n\
\n\
Computes the hypergeometric function 0F1(c,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_0F1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hyperg_0F1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hyperg_0F1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_0F1_E undefined

  error ("GSL function gsl_sf_hyperg_0F1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_0F1_E
}


// PKG_ADD: autoload ("hyperg_0F1", which ("gsl_sf"));
DEFUN_DLD(hyperg_0F1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hyperg_0F1 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hyperg_0F1 (@dots{})\n\
\n\
Computes the hypergeometric function 0F1(c,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_0F1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hyperg_0F1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hyperg_0F1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_0F1_E undefined

  error ("GSL function gsl_sf_hyperg_0F1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_0F1_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_half", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_half (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_half (@dots{})\n\
\n\
Computes the irregular Spherical Conical Function\n\
P^@{1/2@}_@{-1/2 + i \\lambda@}(x) for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_half_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_half_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_HALF_E undefined

  error ("GSL function gsl_sf_conicalP_half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_HALF_E
}


// PKG_ADD: autoload ("conicalP_half", which ("gsl_sf"));
DEFUN_DLD(conicalP_half, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} conicalP_half (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} conicalP_half (@dots{})\n\
\n\
Computes the irregular Spherical Conical Function\n\
P^@{1/2@}_@{-1/2 + i \\lambda@}(x) for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_HALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_half_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_half_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_HALF_E undefined

  error ("GSL function gsl_sf_conicalP_half_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_HALF_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_mhalf", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_mhalf, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_mhalf (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_mhalf (@dots{})\n\
\n\
Computes the regular Spherical Conical Function\n\
P^@{-1/2@}_@{-1/2 + i \\lambda@}(x) for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_MHALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_mhalf_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_mhalf_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_MHALF_E undefined

  error ("GSL function gsl_sf_conicalP_mhalf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_MHALF_E
}


// PKG_ADD: autoload ("conicalP_mhalf", which ("gsl_sf"));
DEFUN_DLD(conicalP_mhalf, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} conicalP_mhalf (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} conicalP_mhalf (@dots{})\n\
\n\
Computes the regular Spherical Conical Function\n\
P^@{-1/2@}_@{-1/2 + i \\lambda@}(x) for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_MHALF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_mhalf_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_mhalf_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_MHALF_E undefined

  error ("GSL function gsl_sf_conicalP_mhalf_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_MHALF_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_0", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_0 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_0 (@dots{})\n\
\n\
Computes the conical function P^0_@{-1/2 + i \\lambda@}(x)\n\
for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_0_E undefined

  error ("GSL function gsl_sf_conicalP_0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_0_E
}


// PKG_ADD: autoload ("conicalP_0", which ("gsl_sf"));
DEFUN_DLD(conicalP_0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} conicalP_0 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} conicalP_0 (@dots{})\n\
\n\
Computes the conical function P^0_@{-1/2 + i \\lambda@}(x)\n\
for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_0_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_0_E undefined

  error ("GSL function gsl_sf_conicalP_0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_0_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_1 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_1 (@dots{})\n\
\n\
Computes the conical function P^1_@{-1/2 + i \\lambda@}(x)\n\
for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_1_E undefined

  error ("GSL function gsl_sf_conicalP_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_1_E
}


// PKG_ADD: autoload ("conicalP_1", which ("gsl_sf"));
DEFUN_DLD(conicalP_1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} conicalP_1 (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} conicalP_1 (@dots{})\n\
\n\
Computes the conical function P^1_@{-1/2 + i \\lambda@}(x)\n\
for x > -1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_conicalP_1_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_1_E undefined

  error ("GSL function gsl_sf_conicalP_1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_1_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Ai", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Ai, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Ai (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Ai (@dots{})\n\
\n\
Computes the Airy function Ai(x) with an accuracy\n\
specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_E undefined

  error ("GSL function gsl_sf_airy_Ai_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_E
}


// PKG_ADD: autoload ("airy_Ai", which ("gsl_sf"));
DEFUN_DLD(airy_Ai, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Ai (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Ai (@dots{})\n\
\n\
Computes the Airy function Ai(x) with an accuracy\n\
specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_E undefined

  error ("GSL function gsl_sf_airy_Ai_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Bi", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Bi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Bi (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Bi (@dots{})\n\
\n\
Computes the Airy function Bi(x) with an accuracy\n\
specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_E undefined

  error ("GSL function gsl_sf_airy_Bi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_E
}


// PKG_ADD: autoload ("airy_Bi", which ("gsl_sf"));
DEFUN_DLD(airy_Bi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Bi (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Bi (@dots{})\n\
\n\
Computes the Airy function Bi(x) with an accuracy\n\
specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_E undefined

  error ("GSL function gsl_sf_airy_Bi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Ai_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Ai_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Ai_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Ai_scaled (@dots{})\n\
\n\
Computes a scaled version of the Airy function\n\
S_A(x) Ai(x). For x>0 the scaling factor S_A(x) is \\exp(+(2/3) x^(3/2)), and\n\
is 1 for x<0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Ai_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_SCALED_E
}


// PKG_ADD: autoload ("airy_Ai_scaled", which ("gsl_sf"));
DEFUN_DLD(airy_Ai_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Ai_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Ai_scaled (@dots{})\n\
\n\
Computes a scaled version of the Airy function\n\
S_A(x) Ai(x). For x>0 the scaling factor S_A(x) is \\exp(+(2/3) x^(3/2)), and\n\
is 1 for x<0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Ai_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Bi_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Bi_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Bi_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Bi_scaled (@dots{})\n\
\n\
Computes a scaled version of the Airy function\n\
S_B(x) Bi(x). For x>0 the scaling factor S_B(x) is exp(-(2/3) x^(3/2)), and\n\
is 1 for x<0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Bi_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_SCALED_E
}


// PKG_ADD: autoload ("airy_Bi_scaled", which ("gsl_sf"));
DEFUN_DLD(airy_Bi_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Bi_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Bi_scaled (@dots{})\n\
\n\
Computes a scaled version of the Airy function\n\
S_B(x) Bi(x). For x>0 the scaling factor S_B(x) is exp(-(2/3) x^(3/2)), and\n\
is 1 for x<0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Bi_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Ai_deriv", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Ai_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Ai_deriv (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Ai_deriv (@dots{})\n\
\n\
Computes the Airy function derivative Ai'(x) with an\n\
accuracy specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_Ai_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_DERIV_E
}


// PKG_ADD: autoload ("airy_Ai_deriv", which ("gsl_sf"));
DEFUN_DLD(airy_Ai_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Ai_deriv (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Ai_deriv (@dots{})\n\
\n\
Computes the Airy function derivative Ai'(x) with an\n\
accuracy specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_Ai_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_DERIV_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Bi_deriv", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Bi_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Bi_deriv (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Bi_deriv (@dots{})\n\
\n\
Computes the Airy function derivative Bi'(x) with an\n\
accuracy specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_Bi_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_DERIV_E
}


// PKG_ADD: autoload ("airy_Bi_deriv", which ("gsl_sf"));
DEFUN_DLD(airy_Bi_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Bi_deriv (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Bi_deriv (@dots{})\n\
\n\
Computes the Airy function derivative Bi'(x) with an\n\
accuracy specified by mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_Bi_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_DERIV_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Ai_deriv_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Ai_deriv_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Ai_deriv_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Ai_deriv_scaled (@dots{})\n\
\n\
Computes the derivative of the scaled Airy function\n\
S_A(x) Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Ai_deriv_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E
}


// PKG_ADD: autoload ("airy_Ai_deriv_scaled", which ("gsl_sf"));
DEFUN_DLD(airy_Ai_deriv_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Ai_deriv_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Ai_deriv_scaled (@dots{})\n\
\n\
Computes the derivative of the scaled Airy function\n\
S_A(x) Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Ai_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Ai_deriv_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_AI_DERIV_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_airy_Bi_deriv_scaled", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_Bi_deriv_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_Bi_deriv_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_Bi_deriv_scaled (@dots{})\n\
\n\
Computes the derivative of the scaled Airy function\n\
S_B(x) Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Bi_deriv_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E
}


// PKG_ADD: autoload ("airy_Bi_deriv_scaled", which ("gsl_sf"));
DEFUN_DLD(airy_Bi_deriv_scaled, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_Bi_deriv_scaled (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_Bi_deriv_scaled (@dots{})\n\
\n\
Computes the derivative of the scaled Airy function\n\
S_B(x) Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_airy_Bi_deriv_scaled_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E undefined

  error ("GSL function gsl_sf_airy_Bi_deriv_scaled_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_BI_DERIV_SCALED_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_Kcomp", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_Kcomp, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_Kcomp (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_Kcomp (@dots{})\n\
\n\
Computes the complete elliptic integral K(k)\n\
@tex\n\
\\beforedisplay\n\
$$\n\
\\eqalign{\n\
K(k)   &= \\int_0^{\\pi/2}  {dt \\over \\sqrt{(1 - k^2 \\sin^2(t))}}  \\cr\n\
}\n\
$$\n\
\\afterdisplay\n\
See also:\n\
@end tex\n\
@ifinfo\n\
@group\n\
@example\n\
                              pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |             1\n\
          ellint_Kcomp(k)  = |    ------------------- dt\n\
                             |              2   2\n\
                             /    sqrt(1 - k sin (t))\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifinfo\n\
@ifhtml\n\
@group\n\
@example\n\
                              pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |             1\n\
          ellint_Kcomp(k)  = |    ------------------- dt\n\
                             |              2   2\n\
                             /    sqrt(1 - k sin (t))\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifhtml\n\
\n\
@seealso{ellipj, ellipke}\n\
\n\
The notation used here is based on Carlson, @cite{Numerische\n\
Mathematik} 33 (1979) and differs slightly from that used by\n\
Abramowitz & Stegun, where the functions are given in terms of the\n\
parameter @math{m = k^2}.\n\
\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_KCOMP_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Kcomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Kcomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_KCOMP_E undefined

  error ("GSL function gsl_sf_ellint_Kcomp_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_KCOMP_E
}


// PKG_ADD: autoload ("ellint_Kcomp", which ("gsl_sf"));
DEFUN_DLD(ellint_Kcomp, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} ellint_Kcomp (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} ellint_Kcomp (@dots{})\n\
\n\
Computes the complete elliptic integral K(k)\n\
@tex\n\
\\beforedisplay\n\
$$\n\
\\eqalign{\n\
K(k)   &= \\int_0^{\\pi/2}  {dt \\over \\sqrt{(1 - k^2 \\sin^2(t))}}  \\cr\n\
}\n\
$$\n\
\\afterdisplay\n\
See also:\n\
@end tex\n\
@ifinfo\n\
@group\n\
@example\n\
                              pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |             1\n\
          ellint_Kcomp(k)  = |    ------------------- dt\n\
                             |              2   2\n\
                             /    sqrt(1 - k sin (t))\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifinfo\n\
@ifhtml\n\
@group\n\
@example\n\
                              pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |             1\n\
          ellint_Kcomp(k)  = |    ------------------- dt\n\
                             |              2   2\n\
                             /    sqrt(1 - k sin (t))\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifhtml\n\
\n\
@seealso{ellipj, ellipke}\n\
\n\
The notation used here is based on Carlson, @cite{Numerische\n\
Mathematik} 33 (1979) and differs slightly from that used by\n\
Abramowitz & Stegun, where the functions are given in terms of the\n\
parameter @math{m = k^2}.\n\
\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_KCOMP_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Kcomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Kcomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_KCOMP_E undefined

  error ("GSL function gsl_sf_ellint_Kcomp_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_KCOMP_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_Ecomp", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_Ecomp, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_Ecomp (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_Ecomp (@dots{})\n\
\n\
Computes the complete elliptic integral E(k) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@tex\n\
\\beforedisplay\n\
$$\n\
\\eqalign{\n\
E(k)   &= \\int_0^{\\pi/2}    \\sqrt{(1 - k^2 \\sin^2(t))} dt \\cr\n\
}\n\
$$\n\
\\afterdisplay\n\
See also:\n\
\n\
@end tex\n\
@ifinfo\n\
@group\n\
@example\n\
                               pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |              2    2\n\
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt\n\
                             |\n\
                             /\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifinfo\n\
@ifhtml\n\
@group\n\
@example\n\
                               pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |              2    2\n\
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt\n\
                             |\n\
                             /\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifhtml\n\
\n\
@seealso{ellipj, ellipke}\n\
\n\
The notation used here is based on Carlson, @cite{Numerische\n\
Mathematik} 33 (1979) and differs slightly from that used by\n\
Abramowitz & Stegun, where the functions are given in terms of the\n\
parameter @math{m = k^2}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_ECOMP_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Ecomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Ecomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_ECOMP_E undefined

  error ("GSL function gsl_sf_ellint_Ecomp_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_ECOMP_E
}


// PKG_ADD: autoload ("ellint_Ecomp", which ("gsl_sf"));
DEFUN_DLD(ellint_Ecomp, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} ellint_Ecomp (@var{arg1}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} ellint_Ecomp (@dots{})\n\
\n\
Computes the complete elliptic integral E(k) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@tex\n\
\\beforedisplay\n\
$$\n\
\\eqalign{\n\
E(k)   &= \\int_0^{\\pi/2}    \\sqrt{(1 - k^2 \\sin^2(t))} dt \\cr\n\
}\n\
$$\n\
\\afterdisplay\n\
See also:\n\
\n\
@end tex\n\
@ifinfo\n\
@group\n\
@example\n\
                               pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |              2    2\n\
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt\n\
                             |\n\
                             /\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifinfo\n\
@ifhtml\n\
@group\n\
@example\n\
                               pi\n\
                              ---\n\
                               2\n\
                             /\n\
                             |              2    2\n\
         ellint_Ecomp(k)  =  |    sqrt(1 - k sin (t)) dt\n\
                             |\n\
                             /\n\
                              0\n\
\n\
@end example\n\
@end group\n\
@end ifhtml\n\
\n\
@seealso{ellipj, ellipke}\n\
\n\
The notation used here is based on Carlson, @cite{Numerische\n\
Mathematik} 33 (1979) and differs slightly from that used by\n\
Abramowitz & Stegun, where the functions are given in terms of the\n\
parameter @math{m = k^2}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_ECOMP_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(1).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(1).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Ecomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_ellint_Ecomp_e (arg1_data[i], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_ECOMP_E undefined

  error ("GSL function gsl_sf_ellint_Ecomp_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_ECOMP_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_E", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_E, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_E (@var{arg1}, @var{arg2}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_E (@dots{})\n\
\n\
This routine computes the elliptic integral E(\\phi,k) to the accuracy\n\
specified by the mode variable mode. Note that Abramowitz & Stegun\n\
define this function in terms of the parameter m = k^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_E_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(2).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(2).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_E_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_E_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_E_E undefined

  error ("GSL function gsl_sf_ellint_E_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_E_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_F", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_F, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_F (@var{arg1}, @var{arg2}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_F (@dots{})\n\
\n\
This routine computes the elliptic integral F(\\phi,k) to the accuracy\n\
specified by the mode variable mode. Note that Abramowitz & Stegun\n\
define this function in terms of the parameter m = k^2.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_F_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(2).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(2).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_F_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_F_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_F_E undefined

  error ("GSL function gsl_sf_ellint_F_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_F_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_Pcomp", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_Pcomp, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_Pcomp (@var{arg1}, @var{arg2}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_Pcomp (@dots{})\n\
\n\
Computes the complete elliptic integral \\Pi(k,n) to the\n\
accuracy specified by the mode variable mode. Note that Abramowitz &\n\
Stegun define this function in terms of the parameters m = k^2 and\n\
\\sin^2(\\alpha) = k^2, with the change of sign n \\to -n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_PCOMP_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(2).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(2).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_Pcomp_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_Pcomp_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_PCOMP_E undefined

  error ("GSL function gsl_sf_ellint_Pcomp_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_PCOMP_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_RC", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_RC, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_RC (@var{arg1}, @var{arg2}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_RC (@dots{})\n\
\n\
This routine computes the incomplete elliptic integral RC(x,y) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_RC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(2).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(2).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_RC_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_RC_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_RC_E undefined

  error ("GSL function gsl_sf_ellint_RC_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_RC_E
}
#if GSL_MAJOR_VERSION < 2


// PKG_ADD: autoload ("gsl_sf_ellint_D", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_D, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_D (@var{arg1}, @var{arg2}, @var{arg3}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_D (@dots{})\n\
\n\
This function computes the incomplete elliptic integral\n\
D(\\phi,k) which is defined through the Carlson form\n\
RD(x,y,z) by the following relation,\n\
\n\
D(\\phi,k) = (1/3)(\\sin(\\phi))^3\n\
	  x RD (1-\\sin^2(\\phi), 1-k^2 \\sin^2(\\phi), 1).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_D_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(3).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(3).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_D_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_D_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_D_E undefined

  error ("GSL function gsl_sf_ellint_D_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_D_E
}
#else


// PKG_ADD: autoload ("gsl_sf_ellint_D", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_D, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_D (@var{arg1}, @var{arg2}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_D (@dots{})\n\
\n\
This function computes the incomplete elliptic integral\n\
D(\\phi,k) which is defined through the Carlson form\n\
RD(x,y,z) by the following relation,\n\
\n\
D(\\phi,k) = (1/3)(\\sin(\\phi))^3\n\
	  x RD (1-\\sin^2(\\phi), 1-k^2 \\sin^2(\\phi), 1).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_D_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(2).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(2).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_D_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_ellint_D_e (arg1_data[i1], arg2_data[i2], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_D_E undefined

  error ("GSL function gsl_sf_ellint_D_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_D_E
}
#endif


// PKG_ADD: autoload ("gsl_sf_ellint_P", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_P, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_P (@var{arg1}, @var{arg2}, @var{arg3}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_P (@dots{})\n\
\n\
This routine computes the incomplete elliptic integral \\Pi(\\phi,k,n)\n\
to the accuracy specified by the mode variable mode. Note that\n\
Abramowitz & Stegun define this function in terms of the parameters\n\
m = k^2 and \\sin^2(\\alpha) = k^2, with the change of sign n \\to -n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_P_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(3).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(3).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_P_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_P_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_P_E undefined

  error ("GSL function gsl_sf_ellint_P_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_P_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_RD", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_RD, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_RD (@var{arg1}, @var{arg2}, @var{arg3}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_RD (@dots{})\n\
\n\
This routine computes the incomplete elliptic integral RD(x,y,z) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_RD_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(3).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(3).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_RD_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_RD_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_RD_E undefined

  error ("GSL function gsl_sf_ellint_RD_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_RD_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_RF", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_RF, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_RF (@var{arg1}, @var{arg2}, @var{arg3}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_RF (@dots{})\n\
\n\
This routine computes the incomplete elliptic integral RF(x,y,z) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_RF_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(3).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(3).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_RF_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_ellint_RF_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_RF_E undefined

  error ("GSL function gsl_sf_ellint_RF_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_RF_E
}


// PKG_ADD: autoload ("gsl_sf_ellint_RJ", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_ellint_RJ, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_ellint_RJ (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{mode})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_ellint_RJ (@dots{})\n\
\n\
This routine computes the incomplete elliptic integral RJ(x,y,z,p) to the\n\
accuracy specified by the mode variable mode.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
The argument @var{mode} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_PREC_DOUBLE\n\
    Double-precision, a relative accuracy of approximately @code{2e-16}.\n\
@item 1 = GSL_PREC_SINGLE\n\
    Single-precision, a relative accuracy of approximately @code{1e-7}.\n\
@item 2 = GSL_PREC_APPROX\n\
    Approximate values, a relative accuracy of approximately @code{5e-4}.\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ELLINT_RJ_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 5;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the mode argument is scalar
  if (! args(4).is_scalar_type ())
    {
      error ("The mode argument must be scalar.");
      print_usage ();
      return octave_value ();
    }

  // Check that the mode argument has a valid value
  double tmp_mode_dbl = args(4).double_value ();
  gsl_mode_t mode;
  if (tmp_mode_dbl == 0.0)
    mode = GSL_PREC_DOUBLE;
  else if (tmp_mode_dbl == 1.0)
    mode = GSL_PREC_SINGLE;
  else if (tmp_mode_dbl == 2.0)
    mode = GSL_PREC_APPROX;
  else
    {
      error ("The 'mode' argument must be 0, 1, or 2.");
      print_usage ();
      return octave_value ();
    }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          gsl_sf_ellint_RJ_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], arg4_data[i4], mode, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          gsl_sf_ellint_RJ_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], arg4_data[i4], mode, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ELLINT_RJ_E undefined

  error ("GSL function gsl_sf_ellint_RJ_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ELLINT_RJ_E
}


// PKG_ADD: autoload ("gsl_sf_airy_zero_Ai", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_zero_Ai, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_zero_Ai (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_zero_Ai (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_AI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_AI_E undefined

  error ("GSL function gsl_sf_airy_zero_Ai_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_AI_E
}


// PKG_ADD: autoload ("airy_zero_Ai", which ("gsl_sf"));
DEFUN_DLD(airy_zero_Ai, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_zero_Ai (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_zero_Ai (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_AI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_AI_E undefined

  error ("GSL function gsl_sf_airy_zero_Ai_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_AI_E
}


// PKG_ADD: autoload ("gsl_sf_airy_zero_Bi", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_zero_Bi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_zero_Bi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_zero_Bi (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_BI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_BI_E undefined

  error ("GSL function gsl_sf_airy_zero_Bi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_BI_E
}


// PKG_ADD: autoload ("airy_zero_Bi", which ("gsl_sf"));
DEFUN_DLD(airy_zero_Bi, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_zero_Bi (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_zero_Bi (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_BI_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_BI_E undefined

  error ("GSL function gsl_sf_airy_zero_Bi_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_BI_E
}


// PKG_ADD: autoload ("gsl_sf_airy_zero_Ai_deriv", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_zero_Ai_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_zero_Ai_deriv (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_zero_Ai_deriv (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function derivative Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_deriv_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_deriv_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_zero_Ai_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E
}


// PKG_ADD: autoload ("airy_zero_Ai_deriv", which ("gsl_sf"));
DEFUN_DLD(airy_zero_Ai_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_zero_Ai_deriv (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_zero_Ai_deriv (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function derivative Ai(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_deriv_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Ai_deriv_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_zero_Ai_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_AI_DERIV_E
}


// PKG_ADD: autoload ("gsl_sf_airy_zero_Bi_deriv", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_airy_zero_Bi_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_airy_zero_Bi_deriv (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_airy_zero_Bi_deriv (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function derivative Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_deriv_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_deriv_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_zero_Bi_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E
}


// PKG_ADD: autoload ("airy_zero_Bi_deriv", which ("gsl_sf"));
DEFUN_DLD(airy_zero_Bi_deriv, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} airy_zero_Bi_deriv (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} airy_zero_Bi_deriv (@dots{})\n\
\n\
Computes the location of the s-th zero of the Airy\n\
function derivative Bi(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_deriv_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_airy_zero_Bi_deriv_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E undefined

  error ("GSL function gsl_sf_airy_zero_Bi_deriv_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_AIRY_ZERO_BI_DERIV_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_zero_J0", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_zero_J0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_zero_J0 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_zero_J0 (@dots{})\n\
\n\
Computes the location of the s-th positive zero of the\n\
Bessel function J_0(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_ZERO_J0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J0_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J0_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_ZERO_J0_E undefined

  error ("GSL function gsl_sf_bessel_zero_J0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_ZERO_J0_E
}


// PKG_ADD: autoload ("bessel_zero_J0", which ("gsl_sf"));
DEFUN_DLD(bessel_zero_J0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_zero_J0 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_zero_J0 (@dots{})\n\
\n\
Computes the location of the s-th positive zero of the\n\
Bessel function J_0(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_ZERO_J0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J0_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J0_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_ZERO_J0_E undefined

  error ("GSL function gsl_sf_bessel_zero_J0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_ZERO_J0_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_zero_J1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_zero_J1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_zero_J1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_zero_J1 (@dots{})\n\
\n\
Computes the location of the s-th positive zero of the\n\
Bessel function J_1(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_ZERO_J1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J1_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J1_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_ZERO_J1_E undefined

  error ("GSL function gsl_sf_bessel_zero_J1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_ZERO_J1_E
}


// PKG_ADD: autoload ("bessel_zero_J1", which ("gsl_sf"));
DEFUN_DLD(bessel_zero_J1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} bessel_zero_J1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} bessel_zero_J1 (@dots{})\n\
\n\
Computes the location of the s-th positive zero of the\n\
Bessel function J_1(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_ZERO_J1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J1_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_J1_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_ZERO_J1_E undefined

  error ("GSL function gsl_sf_bessel_zero_J1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_ZERO_J1_E
}


// PKG_ADD: autoload ("gsl_sf_psi_1_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_psi_1_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_psi_1_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_psi_1_int (@dots{})\n\
\n\
Computes the Trigamma function \\psi(n) for positive\n\
integer n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_1_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_1_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_1_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_1_INT_E undefined

  error ("GSL function gsl_sf_psi_1_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_1_INT_E
}


// PKG_ADD: autoload ("psi_1_int", which ("gsl_sf"));
DEFUN_DLD(psi_1_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} psi_1_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} psi_1_int (@dots{})\n\
\n\
Computes the Trigamma function \\psi(n) for positive\n\
integer n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_PSI_1_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_1_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_psi_1_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_PSI_1_INT_E undefined

  error ("GSL function gsl_sf_psi_1_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_PSI_1_INT_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_cyl_reg", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_cyl_reg, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_cyl_reg (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_cyl_reg (@dots{})\n\
\n\
Computes the Regular Cylindrical Conical Function\n\
@math{P^{-m}_{-1/2 + i \\lambda}(x)}, for @math{x > -1}, @math{m} @geq{} @math{-1}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_CYL_REG_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_conicalP_cyl_reg_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_conicalP_cyl_reg_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_CYL_REG_E undefined

  error ("GSL function gsl_sf_conicalP_cyl_reg_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_CYL_REG_E
}


// PKG_ADD: autoload ("gsl_sf_conicalP_sph_reg", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_conicalP_sph_reg, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_conicalP_sph_reg (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_conicalP_sph_reg (@dots{})\n\
\n\
Computes the Regular Spherical Conical Function\n\
@math{P^{-1/2-l}_{-1/2 + i \\lambda}(x)}, for @math{x > -1}, @math{l} @geq{} @math{-1}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CONICALP_SPH_REG_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_conicalP_sph_reg_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_conicalP_sph_reg_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CONICALP_SPH_REG_E undefined

  error ("GSL function gsl_sf_conicalP_sph_reg_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CONICALP_SPH_REG_E
}


// PKG_ADD: autoload ("gsl_sf_gegenpoly_n", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gegenpoly_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gegenpoly_n (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_gegenpoly_n (@dots{})\n\
\n\
These functions evaluate the Gegenbauer polynomial @math{C^{(\\lambda)}_n(x)}\n\
for n, lambda, x subject to @math{\\lambda > -1/2}, @math{n} @geq{} @math{0}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_GEGENPOLY_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_gegenpoly_n_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_gegenpoly_n_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_GEGENPOLY_N_E undefined

  error ("GSL function gsl_sf_gegenpoly_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GEGENPOLY_N_E
}


// PKG_ADD: autoload ("gsl_sf_laguerre_n", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_laguerre_n, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_laguerre_n (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_laguerre_n (@dots{})\n\
\n\
Computes the generalized Laguerre polynomial @math{L^a_n(x)} for\n\
@math{a > -1} and @math{n} @geq{} @math{0}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LAGUERRE_N_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_laguerre_n_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_laguerre_n_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LAGUERRE_N_E undefined

  error ("GSL function gsl_sf_laguerre_n_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LAGUERRE_N_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_ce", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_ce, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_ce (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_ce (@dots{})\n\
\n\
This routine computes the angular Mathieu function ce_n(q,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_CE_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_ce_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_ce_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_CE_E undefined

  error ("GSL function gsl_sf_mathieu_ce_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_CE_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_se", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_se, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_se (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_se (@dots{})\n\
\n\
This routine computes the angular Mathieu function se_n(q,x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_SE_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_se_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_se_e (x1, arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_SE_E undefined

  error ("GSL function gsl_sf_mathieu_se_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_SE_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_U_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_U_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_U_int (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_U_int (@dots{})\n\
\n\
Secondary Confluent Hypergoemetric U function A&E 13.1.3\n\
m and n are integers.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_U_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hyperg_U_int_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hyperg_U_int_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_U_INT_E undefined

  error ("GSL function gsl_sf_hyperg_U_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_U_INT_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_1F1_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_1F1_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_1F1_int (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_1F1_int (@dots{})\n\
\n\
Primary Confluent Hypergoemetric U function A&E 13.1.3\n\
m and n are integers.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_1F1_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hyperg_1F1_int_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hyperg_1F1_int_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_1F1_INT_E undefined

  error ("GSL function gsl_sf_hyperg_1F1_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_1F1_INT_E
}


// PKG_ADD: autoload ("gsl_sf_legendre_Plm", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Plm, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_Plm (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_legendre_Plm (@dots{})\n\
\n\
Computes the associated Legendre polynomial P_l^m(x)\n\
for m >= 0, l >= m, |x| <= 1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PLM_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Plm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Plm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_PLM_E undefined

  error ("GSL function gsl_sf_legendre_Plm_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PLM_E
}


// PKG_ADD: autoload ("legendre_Plm", which ("gsl_sf"));
DEFUN_DLD(legendre_Plm, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} legendre_Plm (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} legendre_Plm (@dots{})\n\
\n\
Computes the associated Legendre polynomial P_l^m(x)\n\
for m >= 0, l >= m, |x| <= 1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PLM_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Plm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_Plm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_PLM_E undefined

  error ("GSL function gsl_sf_legendre_Plm_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PLM_E
}


// PKG_ADD: autoload ("gsl_sf_legendre_sphPlm", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_sphPlm, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_sphPlm (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_legendre_sphPlm (@dots{})\n\
\n\
Computes the normalized associated Legendre polynomial\n\
$\\sqrt@{(2l+1)/(4\\pi)@} \\sqrt@{(l-m)!/(l+m)!@} P_l^m(x)$ suitable for use\n\
in spherical harmonics. The parameters must satisfy m >= 0, l >= m,\n\
|x| <= 1. Theses routines avoid the overflows that occur for the\n\
standard normalization of P_l^m(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_SPHPLM_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_sphPlm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_sphPlm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_SPHPLM_E undefined

  error ("GSL function gsl_sf_legendre_sphPlm_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_SPHPLM_E
}


// PKG_ADD: autoload ("legendre_sphPlm", which ("gsl_sf"));
DEFUN_DLD(legendre_sphPlm, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} legendre_sphPlm (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} legendre_sphPlm (@dots{})\n\
\n\
Computes the normalized associated Legendre polynomial\n\
$\\sqrt@{(2l+1)/(4\\pi)@} \\sqrt@{(l-m)!/(l+m)!@} P_l^m(x)$ suitable for use\n\
in spherical harmonics. The parameters must satisfy m >= 0, l >= m,\n\
|x| <= 1. Theses routines avoid the overflows that occur for the\n\
standard normalization of P_l^m(x).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LEGENDRE_SPHPLM_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_sphPlm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_legendre_sphPlm_e (x1, x2, arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LEGENDRE_SPHPLM_E undefined

  error ("GSL function gsl_sf_legendre_sphPlm_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_SPHPLM_E
}


// PKG_ADD: autoload ("gsl_sf_hydrogenicR", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hydrogenicR, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hydrogenicR (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hydrogenicR (@dots{})\n\
\n\
This routine computes the n-th normalized hydrogenic bound state\n\
radial wavefunction,\n\
@tex\n\
R_n := 2 (Z^{3/2}/n^2) \\sqrt{(n-l-1)!/(n+l)!} \\exp(-Z r/n)\n\
         (2Zr/n)^l L^{2l+1}_{n-l-1}(2Zr/n).\n\
@end tex\n\
@ifnottex\n\
\n\
@example\n\
@group\n\
          Z^(3/2)     ----------------    -Z r/n          l    2l+1\n\
R_n := 2 --------  \\ / (n-l-1)!/(n+l)! . e       . (2Zr/n)  . L      (2Zr/n).\n\
            n^2     v                                          n-l-1\n\
@end group\n\
@end example\n\
\n\
@end ifnottex\n\
where @math{L^a_b(x)} is the generalized Laguerre polynomial.\n\
The normalization is chosen such that the wavefunction \\psi\n\
is given by @math{\\psi(n,l,r) = R_n Y_{lm}}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYDROGENICR_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hydrogenicR_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_hydrogenicR_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYDROGENICR_E undefined

  error ("GSL function gsl_sf_hydrogenicR_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYDROGENICR_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_Mc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_Mc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_Mc (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_Mc (@dots{})\n\
\n\
This routine computes the radial j-th kind Mathieu function\n\
@math{Mc_n^{(j)}(q,x)} of order n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_MC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_Mc_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_Mc_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_MC_E undefined

  error ("GSL function gsl_sf_mathieu_Mc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_MC_E
}


// PKG_ADD: autoload ("gsl_sf_mathieu_Ms", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_mathieu_Ms, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_mathieu_Ms (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_mathieu_Ms (@dots{})\n\
\n\
This routine computes the radial j-th kind Mathieu function\n\
@math{Ms_n^{(j)}(q,x)} of order n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_MATHIEU_MS_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_Ms_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_mathieu_Ms_e (x1, x2, arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_MATHIEU_MS_E undefined

  error ("GSL function gsl_sf_mathieu_Ms_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_MATHIEU_MS_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_zero_Jnu", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_zero_Jnu, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_zero_Jnu (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_bessel_zero_Jnu (@dots{})\n\
\n\
Computes the location of the n-th positive zero of the\n\
Bessel function J_x().\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BESSEL_ZERO_JNU_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_Jnu_e (arg1_data[i1], x2, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_bessel_zero_Jnu_e (arg1_data[i1], x2, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BESSEL_ZERO_JNU_E undefined

  error ("GSL function gsl_sf_bessel_zero_Jnu_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_ZERO_JNU_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_U", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_U, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_U (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_U (@dots{})\n\
\n\
Secondary Confluent Hypergoemetric U function A&E 13.1.3\n\
All inputs are double as is the output.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_U_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_U_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_U_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_U_E undefined

  error ("GSL function gsl_sf_hyperg_U_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_U_E
}


// PKG_ADD: autoload ("hyperg_U", which ("gsl_sf"));
DEFUN_DLD(hyperg_U, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hyperg_U (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hyperg_U (@dots{})\n\
\n\
Secondary Confluent Hypergoemetric U function A&E 13.1.3\n\
All inputs are double as is the output.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_U_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_U_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_U_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_U_E undefined

  error ("GSL function gsl_sf_hyperg_U_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_U_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_1F1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_1F1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_1F1 (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_1F1 (@dots{})\n\
\n\
Primary Confluent Hypergoemetric U function A&E 13.1.3\n\
All inputs are double as is the output.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_1F1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_1F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_1F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_1F1_E undefined

  error ("GSL function gsl_sf_hyperg_1F1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_1F1_E
}


// PKG_ADD: autoload ("hyperg_1F1", which ("gsl_sf"));
DEFUN_DLD(hyperg_1F1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hyperg_1F1 (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hyperg_1F1 (@dots{})\n\
\n\
Primary Confluent Hypergoemetric U function A&E 13.1.3\n\
All inputs are double as is the output.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_1F1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_1F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_1F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_1F1_E undefined

  error ("GSL function gsl_sf_hyperg_1F1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_1F1_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_2F0", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_2F0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_2F0 (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_2F0 (@dots{})\n\
\n\
Computes the hypergeometric function 2F0(a,b,x).\n\
The series representation is a divergent hypergeometric series.\n\
However, for x < 0 we have 2F0(a,b,x) = (-1/x)^a U(a,1+a-b,-1/x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_2F0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_2F0_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_2F0_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_2F0_E undefined

  error ("GSL function gsl_sf_hyperg_2F0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_2F0_E
}


// PKG_ADD: autoload ("hyperg_2F0", which ("gsl_sf"));
DEFUN_DLD(hyperg_2F0, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hyperg_2F0 (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hyperg_2F0 (@dots{})\n\
\n\
Computes the hypergeometric function 2F0(a,b,x).\n\
The series representation is a divergent hypergeometric series.\n\
However, for x < 0 we have 2F0(a,b,x) = (-1/x)^a U(a,1+a-b,-1/x)\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_2F0_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_2F0_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_hyperg_2F0_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_2F0_E undefined

  error ("GSL function gsl_sf_hyperg_2F0_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_2F0_E
}


// PKG_ADD: autoload ("gsl_sf_beta_inc", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_beta_inc, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_beta_inc (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_beta_inc (@dots{})\n\
\n\
Computes the normalized incomplete Beta function\n\
\n\
  I_x(a,b)=B_x(a,b)/B(a,b)\n\
\n\
where @math{B_x(a,b) = \\int_0^x t^(a-1) (1-t)^(b-1) dt}\n\
for @math{0 \\le x \\le 1}.\n\
\n\
For a > 0, b > 0 the value is computed using a continued fraction\n\
expansion. For all other values it is computed using the relation\n\
  I_x(a,b,x) = (1/a) x^a 2F1(a,1-b,a+1,x)/B(a,b).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_BETA_INC_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_beta_inc_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3)
        {
          gsl_sf_beta_inc_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_BETA_INC_E undefined

  error ("GSL function gsl_sf_beta_inc_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BETA_INC_E
}


// PKG_ADD: autoload ("gsl_sf_hyperg_2F1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hyperg_2F1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hyperg_2F1 (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hyperg_2F1 (@dots{})\n\
\n\
Computes the Gauss hypergeometric function\n\
2F1(a,b,c,x) = F(a,b,c,x) for |x| < 1.\n\
If the arguments (a,b,c,x) are too close to a singularity then\n\
the function can return the error code GSL_EMAXITER when the\n\
series approximation converges too slowly.\n\
This occurs in the region of x=1, c - a - b = m for integer m.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HYPERG_2F1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          gsl_sf_hyperg_2F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4)
        {
          gsl_sf_hyperg_2F1_e (arg1_data[i1], arg2_data[i2], arg3_data[i3], arg4_data[i4], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HYPERG_2F1_E undefined

  error ("GSL function gsl_sf_hyperg_2F1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HYPERG_2F1_E
}


// PKG_ADD: autoload ("gsl_sf_fact", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_fact, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_fact (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_fact (@dots{})\n\
\n\
Computes the factorial n!. The factorial is related to the Gamma\n\
function by n! = \\Gamma(n+1). The maximum value of n such that n! is\n\
not considered an overflow is 170.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_FACT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fact_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_fact_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_FACT_E undefined

  error ("GSL function gsl_sf_fact_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_FACT_E
}


// PKG_ADD: autoload ("gsl_sf_doublefact", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_doublefact, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_doublefact (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_doublefact (@dots{})\n\
\n\
Compute the double factorial n!! = n(n-2)(n-4)\\dots. The maximum value\n\
of n such that n!! is not considered an overflow is 297.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_DOUBLEFACT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_doublefact_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_doublefact_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_DOUBLEFACT_E undefined

  error ("GSL function gsl_sf_doublefact_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_DOUBLEFACT_E
}


// PKG_ADD: autoload ("gsl_sf_lnfact", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lnfact, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lnfact (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lnfact (@dots{})\n\
\n\
Computes the logarithm of the factorial of n,\n\
\\log(n!). The algorithm is faster than computing \\ln(\\Gamma(n+1)) via\n\
gsl_sf_lngamma for n < 170, but defers for larger n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNFACT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lnfact_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lnfact_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNFACT_E undefined

  error ("GSL function gsl_sf_lnfact_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNFACT_E
}


// PKG_ADD: autoload ("gsl_sf_lndoublefact", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lndoublefact, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lndoublefact (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lndoublefact (@dots{})\n\
\n\
Computes the logarithm of the double factorial of n, \\log(n!!).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNDOUBLEFACT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lndoublefact_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lndoublefact_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNDOUBLEFACT_E undefined

  error ("GSL function gsl_sf_lndoublefact_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNDOUBLEFACT_E
}


// PKG_ADD: autoload ("gsl_sf_choose", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_choose, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_choose (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_choose (@dots{})\n\
\n\
Computes the combinatorial factor n choose m = n!/(m!(n-m)!).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_CHOOSE_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i1];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to unsigned int
          double t2 = arg2_data[i2];
          unsigned int x2 = static_cast<unsigned int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_choose_e (x1, x2, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i1];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to unsigned int
          double t2 = arg2_data[i2];
          unsigned int x2 = static_cast<unsigned int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_choose_e (x1, x2, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_CHOOSE_E undefined

  error ("GSL function gsl_sf_choose_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_CHOOSE_E
}


// PKG_ADD: autoload ("gsl_sf_lnchoose", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_lnchoose, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_lnchoose (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_lnchoose (@dots{})\n\
\n\
Computes the logarithm of n choose m. This is equivalent to\n\
\\log(n!) - \\log(m!) - \\log((n-m)!).\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_LNCHOOSE_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i1];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to unsigned int
          double t2 = arg2_data[i2];
          unsigned int x2 = static_cast<unsigned int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lnchoose_e (x1, x2, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          // Convert argument #1 to unsigned int
          double t1 = arg1_data[i1];
          unsigned int x1 = static_cast<unsigned int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to unsigned int
          double t2 = arg2_data[i2];
          unsigned int x2 = static_cast<unsigned int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_lnchoose_e (x1, x2, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_LNCHOOSE_E undefined

  error ("GSL function gsl_sf_lnchoose_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LNCHOOSE_E
}


// PKG_ADD: autoload ("gsl_sf_coupling_3j", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_coupling_3j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_coupling_3j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_coupling_3j (@dots{})\n\
\n\
computes the Wigner 3-j coefficient,\n\
\n\
@example\n\
@group\n\
(ja jb jc\n\
 ma mb mc)\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{ma = two_ma/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_3J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 6;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_3j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_3j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_3J_E undefined

  error ("GSL function gsl_sf_coupling_3j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_3J_E
}


// PKG_ADD: autoload ("coupling_3j", which ("gsl_sf"));
DEFUN_DLD(coupling_3j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} coupling_3j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} coupling_3j (@dots{})\n\
\n\
computes the Wigner 3-j coefficient,\n\
\n\
@example\n\
@group\n\
(ja jb jc\n\
 ma mb mc)\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{ma = two_ma/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_3J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 6;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_3j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_3j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_3J_E undefined

  error ("GSL function gsl_sf_coupling_3j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_3J_E
}


// PKG_ADD: autoload ("gsl_sf_coupling_6j", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_coupling_6j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_coupling_6j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_coupling_6j (@dots{})\n\
\n\
computes the Wigner 6-j coefficient,\n\
\n\
@example\n\
@group\n\
@{ja jb jc\n\
 jd je jf@}\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_6J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 6;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_6j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_6j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_6J_E undefined

  error ("GSL function gsl_sf_coupling_6j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_6J_E
}


// PKG_ADD: autoload ("coupling_6j", which ("gsl_sf"));
DEFUN_DLD(coupling_6j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} coupling_6j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} coupling_6j (@dots{})\n\
\n\
computes the Wigner 6-j coefficient,\n\
\n\
@example\n\
@group\n\
@{ja jb jc\n\
 jd je jf@}\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_6J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 6;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_6j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_6j_e (x1, x2, x3, x4, x5, x6, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_6J_E undefined

  error ("GSL function gsl_sf_coupling_6j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_6J_E
}


// PKG_ADD: autoload ("gsl_sf_coupling_9j", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_coupling_9j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_coupling_9j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6}, @var{arg7}, @var{arg8}, @var{arg9})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_coupling_9j (@dots{})\n\
\n\
computes the Wigner 9-j coefficient,\n\
\n\
@example\n\
@group\n\
@{ja jb jc\n\
 jd je jf\n\
 jg jh ji@}\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_9J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 9;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();
  NDArray arg7 = args(6).array_value();
  NDArray arg8 = args(7).array_value();
  NDArray arg9 = args(8).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);
  bool arg7_scalar = check_arg_dim<NDArray> (arg7, dim, numel, conformant);
  bool arg8_scalar = check_arg_dim<NDArray> (arg8, dim, numel, conformant);
  bool arg9_scalar = check_arg_dim<NDArray> (arg9, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;
  octave_idx_type i7 = 0;
  octave_idx_type i8 = 0;
  octave_idx_type i9 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);
  octave_idx_type inc7 = (arg7_scalar ? 0 : 1);
  octave_idx_type inc8 = (arg8_scalar ? 0 : 1);
  octave_idx_type inc9 = (arg9_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();
  const double *arg7_data = arg7.data ();
  const double *arg8_data = arg8.data ();
  const double *arg9_data = arg9.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6, i7 += inc7, i8 += inc8, i9 += inc9)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #7 to int
          double t7 = arg7_data[i7];
          int x7 = static_cast<int> (t7);
          if ((static_cast<double> (x7)) != t7)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #8 to int
          double t8 = arg8_data[i8];
          int x8 = static_cast<int> (t8);
          if ((static_cast<double> (x8)) != t8)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #9 to int
          double t9 = arg9_data[i9];
          int x9 = static_cast<int> (t9);
          if ((static_cast<double> (x9)) != t9)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_9j_e (x1, x2, x3, x4, x5, x6, x7, x8, x9, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6, i7 += inc7, i8 += inc8, i9 += inc9)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #7 to int
          double t7 = arg7_data[i7];
          int x7 = static_cast<int> (t7);
          if ((static_cast<double> (x7)) != t7)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #8 to int
          double t8 = arg8_data[i8];
          int x8 = static_cast<int> (t8);
          if ((static_cast<double> (x8)) != t8)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #9 to int
          double t9 = arg9_data[i9];
          int x9 = static_cast<int> (t9);
          if ((static_cast<double> (x9)) != t9)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_9j_e (x1, x2, x3, x4, x5, x6, x7, x8, x9, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_9J_E undefined

  error ("GSL function gsl_sf_coupling_9j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_9J_E
}


// PKG_ADD: autoload ("coupling_9j", which ("gsl_sf"));
DEFUN_DLD(coupling_9j, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} coupling_9j (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4}, @var{arg5}, @var{arg6}, @var{arg7}, @var{arg8}, @var{arg9})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} coupling_9j (@dots{})\n\
\n\
computes the Wigner 9-j coefficient,\n\
\n\
@example\n\
@group\n\
@{ja jb jc\n\
 jd je jf\n\
 jg jh ji@}\n\
@end group\n\
@end example\n\
\n\
where the arguments are given in half-integer units,\n\
@code{ja = two_ja/2}, @code{jd = two_jd/2}, etc.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_COUPLING_9J_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 9;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();
  NDArray arg3 = args(2).array_value();
  NDArray arg4 = args(3).array_value();
  NDArray arg5 = args(4).array_value();
  NDArray arg6 = args(5).array_value();
  NDArray arg7 = args(6).array_value();
  NDArray arg8 = args(7).array_value();
  NDArray arg9 = args(8).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);
  bool arg3_scalar = check_arg_dim<NDArray> (arg3, dim, numel, conformant);
  bool arg4_scalar = check_arg_dim<NDArray> (arg4, dim, numel, conformant);
  bool arg5_scalar = check_arg_dim<NDArray> (arg5, dim, numel, conformant);
  bool arg6_scalar = check_arg_dim<NDArray> (arg6, dim, numel, conformant);
  bool arg7_scalar = check_arg_dim<NDArray> (arg7, dim, numel, conformant);
  bool arg8_scalar = check_arg_dim<NDArray> (arg8, dim, numel, conformant);
  bool arg9_scalar = check_arg_dim<NDArray> (arg9, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;
  octave_idx_type i3 = 0;
  octave_idx_type i4 = 0;
  octave_idx_type i5 = 0;
  octave_idx_type i6 = 0;
  octave_idx_type i7 = 0;
  octave_idx_type i8 = 0;
  octave_idx_type i9 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);
  octave_idx_type inc3 = (arg3_scalar ? 0 : 1);
  octave_idx_type inc4 = (arg4_scalar ? 0 : 1);
  octave_idx_type inc5 = (arg5_scalar ? 0 : 1);
  octave_idx_type inc6 = (arg6_scalar ? 0 : 1);
  octave_idx_type inc7 = (arg7_scalar ? 0 : 1);
  octave_idx_type inc8 = (arg8_scalar ? 0 : 1);
  octave_idx_type inc9 = (arg9_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();
  const double *arg3_data = arg3.data ();
  const double *arg4_data = arg4.data ();
  const double *arg5_data = arg5.data ();
  const double *arg6_data = arg6.data ();
  const double *arg7_data = arg7.data ();
  const double *arg8_data = arg8.data ();
  const double *arg9_data = arg9.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6, i7 += inc7, i8 += inc8, i9 += inc9)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #7 to int
          double t7 = arg7_data[i7];
          int x7 = static_cast<int> (t7);
          if ((static_cast<double> (x7)) != t7)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #8 to int
          double t8 = arg8_data[i8];
          int x8 = static_cast<int> (t8);
          if ((static_cast<double> (x8)) != t8)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #9 to int
          double t9 = arg9_data[i9];
          int x9 = static_cast<int> (t9);
          if ((static_cast<double> (x9)) != t9)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_9j_e (x1, x2, x3, x4, x5, x6, x7, x8, x9, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2, i3 += inc3, i4 += inc4, i5 += inc5, i6 += inc6, i7 += inc7, i8 += inc8, i9 += inc9)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i1];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #2 to int
          double t2 = arg2_data[i2];
          int x2 = static_cast<int> (t2);
          if ((static_cast<double> (x2)) != t2)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #3 to int
          double t3 = arg3_data[i3];
          int x3 = static_cast<int> (t3);
          if ((static_cast<double> (x3)) != t3)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #4 to int
          double t4 = arg4_data[i4];
          int x4 = static_cast<int> (t4);
          if ((static_cast<double> (x4)) != t4)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #5 to int
          double t5 = arg5_data[i5];
          int x5 = static_cast<int> (t5);
          if ((static_cast<double> (x5)) != t5)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #6 to int
          double t6 = arg6_data[i6];
          int x6 = static_cast<int> (t6);
          if ((static_cast<double> (x6)) != t6)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #7 to int
          double t7 = arg7_data[i7];
          int x7 = static_cast<int> (t7);
          if ((static_cast<double> (x7)) != t7)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #8 to int
          double t8 = arg8_data[i8];
          int x8 = static_cast<int> (t8);
          if ((static_cast<double> (x8)) != t8)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          // Convert argument #9 to int
          double t9 = arg9_data[i9];
          int x9 = static_cast<int> (t9);
          if ((static_cast<double> (x9)) != t9)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_coupling_9j_e (x1, x2, x3, x4, x5, x6, x7, x8, x9, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_COUPLING_9J_E undefined

  error ("GSL function gsl_sf_coupling_9j_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_COUPLING_9J_E
}


// PKG_ADD: autoload ("gsl_sf_bessel_jl_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_jl_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_jl_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_jl_array (@dots{})\n\
\n\
Computes the values of the regular spherical Bessel functions j_l(x)\n\
for l from 0 to lmax inclusive for lmax >= 0 and x >= 0. The values\n\
are computed using recurrence relations for efficiency, and therefore\n\
may differ slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_JL_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_bessel_jl_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_JL_ARRAY undefined

  error ("GSL function gsl_sf_bessel_jl_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JL_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_jl_steed_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_jl_steed_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_jl_steed_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_jl_steed_array (@dots{})\n\
\n\
This routine uses Steed’s method to compute the values of the regular\n\
spherical Bessel functions j_l(x) for l from 0 to lmax inclusive for\n\
lmax >= 0 and x >= 0.  The Steed/Barnett algorithm is described in\n\
Comp. Phys. Comm. 21, 297 (1981). Steed’s method is more stable than\n\
the recurrence used in the other functions but is also slower.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_JL_STEED_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_bessel_jl_steed_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_JL_STEED_ARRAY undefined

  error ("GSL function gsl_sf_bessel_jl_steed_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JL_STEED_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_il_scaled_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_il_scaled_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_il_scaled_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_il_scaled_array (@dots{})\n\
\n\
This routine computes the values of the scaled regular modified\n\
spherical Bessel functions \\exp(-|x|) i_l(x) for l from 0 to lmax\n\
inclusive for lmax >= 0. The values are computed using recurrence\n\
relations for efficiency, and therefore may differ slightly from the\n\
exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_IL_SCALED_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_bessel_il_scaled_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_IL_SCALED_ARRAY undefined

  error ("GSL function gsl_sf_bessel_il_scaled_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IL_SCALED_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_kl_scaled_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_kl_scaled_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_kl_scaled_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_kl_scaled_array (@dots{})\n\
\n\
This routine computes the values of the scaled irregular modified\n\
spherical Bessel functions \\exp(x) k_l(x) for l from 0 to lmax\n\
inclusive for lmax >= 0 and x>0. The values are computed using\n\
recurrence relations for efficiency, and therefore may differ slightly\n\
from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_KL_SCALED_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_bessel_kl_scaled_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_KL_SCALED_ARRAY undefined

  error ("GSL function gsl_sf_bessel_kl_scaled_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KL_SCALED_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_yl_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_yl_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_yl_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_yl_array (@dots{})\n\
\n\
This routine computes the values of the irregular spherical Bessel\n\
functions y_l(x) for l from 0 to lmax inclusive for lmax >= 0. The\n\
values are computed using recurrence relations for efficiency, and\n\
therefore may differ slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_YL_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_bessel_yl_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_YL_ARRAY undefined

  error ("GSL function gsl_sf_bessel_yl_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YL_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_Pl_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Pl_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_Pl_array (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_legendre_Pl_array (@dots{})\n\
\n\
These functions compute arrays of Legendre polynomials P_l(x) and\n\
derivatives dP_l(x)/dx, for l = 0, \\dots, lmax, |x| <= 1.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PL_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_Pl_array (arg1, arg2, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_LEGENDRE_PL_ARRAY undefined

  error ("GSL function gsl_sf_legendre_Pl_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PL_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_gegenpoly_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_gegenpoly_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_gegenpoly_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_gegenpoly_array (@dots{})\n\
\n\
This function computes an array of Gegenbauer polynomials\n\
@math{C^{(\\lambda)}_n(x)} for n = 0, 1, 2, \\dots, nmax, subject to\n\
@math{\\lambda > -1/2}, @math{nmax} @geq{} @math{0}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_GEGENPOLY_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2 = args(1).scalar_value ();

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_gegenpoly_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_GEGENPOLY_ARRAY undefined

  error ("GSL function gsl_sf_gegenpoly_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_GEGENPOLY_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_In_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_In_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_In_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_In_array (@dots{})\n\
\n\
his routine computes the values of the regular modified cylindrical\n\
Bessel functions I_n(x) for n from nmin to nmax inclusive. The start\n\
of the range nmin must be positive or zero. The values are computed\n\
using recurrence relations for efficiency, and therefore may differ\n\
slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_In_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_IN_ARRAY undefined

  error ("GSL function gsl_sf_bessel_In_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_In_scaled_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_In_scaled_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_In_scaled_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_In_scaled_array (@dots{})\n\
\n\
This routine computes the values of the scaled regular cylindrical\n\
Bessel functions \\exp(-|x|) I_n(x) for n from nmin to nmax\n\
inclusive. The start of the range nmin must be positive or zero. The\n\
values are computed using recurrence relations for efficiency, and\n\
therefore may differ slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_IN_SCALED_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_In_scaled_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_IN_SCALED_ARRAY undefined

  error ("GSL function gsl_sf_bessel_In_scaled_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_IN_SCALED_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_Jn_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Jn_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Jn_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_Jn_array (@dots{})\n\
\n\
This routine computes the values of the regular cylindrical Bessel\n\
functions J_n(x) for n from nmin to nmax inclusive. The values are\n\
computed using recurrence relations for efficiency, and therefore may\n\
differ slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_JN_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_Jn_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_JN_ARRAY undefined

  error ("GSL function gsl_sf_bessel_Jn_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_JN_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_Kn_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Kn_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn_array (@dots{})\n\
\n\
This routine computes the values of the irregular modified cylindrical\n\
Bessel functions K_n(x) for n from nmin to nmax inclusive. The start\n\
of the range nmin must be positive or zero. The domain of the function\n\
is x>0. The values are computed using recurrence relations for\n\
efficiency, and therefore may differ slightly from the exact values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_Kn_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_KN_ARRAY undefined

  error ("GSL function gsl_sf_bessel_Kn_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_Kn_scaled_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Kn_scaled_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn_scaled_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_Kn_scaled_array (@dots{})\n\
\n\
This routine computes the values of the scaled irregular cylindrical\n\
Bessel functions \\exp(x) K_n(x) for n from nmin to nmax inclusive. The\n\
start of the range nmin must be positive or zero. The domain of the\n\
function is x>0. The values are computed using recurrence relations\n\
for efficiency, and therefore may differ slightly from the exact\n\
values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_KN_SCALED_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_Kn_scaled_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_KN_SCALED_ARRAY undefined

  error ("GSL function gsl_sf_bessel_Kn_scaled_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_KN_SCALED_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_bessel_Yn_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_bessel_Yn_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_bessel_Yn_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_bessel_Yn_array (@dots{})\n\
\n\
This routine computes the values of the irregular cylindrical Bessel\n\
functions Y_n(x) for n from nmin to nmax inclusive. The domain of the\n\
function is x>0. The values are computed using recurrence relations\n\
for efficiency, and therefore may differ slightly from the exact\n\
values.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_BESSEL_YN_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg2-arg1+1);

  // Run the calculation
  gsl_sf_bessel_Yn_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_BESSEL_YN_ARRAY undefined

  error ("GSL function gsl_sf_bessel_Yn_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_BESSEL_YN_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_Plm_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Plm_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_Plm_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_legendre_Plm_array (@dots{})\n\
\n\
Compute arrays of Legendre polynomials P_l^m(x) for m >= 0, l = |m|,\n\
..., lmax, |x| <= 1.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
\n\
@strong{DEPRECATION WARNING}: gsl_sf_legendre_Plm_array has been deprecated in GSL 2.0\n\
\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PLM_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_Plm_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_LEGENDRE_PLM_ARRAY undefined

  error ("GSL function gsl_sf_legendre_Plm_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PLM_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_Plm_deriv_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_Plm_deriv_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_Plm_deriv_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_Plm_deriv_array (@dots{})\n\
\n\
Compute arrays of Legendre polynomials P_l^m(x) and derivatives\n\
dP_l^m(x)/dx for m >= 0, l = |m|, ..., lmax, |x| <= 1.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
\n\
@strong{DEPRECATION WARNING}: gsl_sf_legendre_Plm_deriv_array has been deprecated in GSL 2.0\n\
\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_PLM_DERIV_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);
  RowVector y2 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_Plm_deriv_array (arg1, arg2, arg3, y1.fortran_vec (), y2.fortran_vec ());

  octave_value_list retval;
  retval(0) = octave_value (y1);
  retval(1) = octave_value (y2);
  return retval;

#else // HAVE_GSL_SF_LEGENDRE_PLM_DERIV_ARRAY undefined

  error ("GSL function gsl_sf_legendre_Plm_deriv_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_PLM_DERIV_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_sphPlm_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_sphPlm_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_sphPlm_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_legendre_sphPlm_array (@dots{})\n\
\n\
Computes an array of normalized associated Legendre functions\n\
@iftex\n\
@tex\n\
$\\\\sqrt{(2l+1)/(4\\\\pi)} \\\\sqrt{(l-m)!/(l+m)!} P_l^m(x)$\n\
for $m >= 0, l = |m|, ..., lmax, |x| <= 1.0$\n\
@end tex\n\
@end iftex\n\
@ifinfo\n\
sqrt((2l+1)/(4*pi)) * sqrt((l-m)!/(l+m)!) Plm (x)\n\
for m >= 0, l = |m|, ..., lmax, |x| <= 1.0\n\
@end ifinfo\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
\n\
@strong{DEPRECATION WARNING}: gsl_sf_legendre_sphPlm_array has been deprecated in GSL 2.0\n\
\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_sphPlm_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY undefined

  error ("GSL function gsl_sf_legendre_sphPlm_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY
}


// PKG_ADD: autoload ("legendre_sphPlm_array", which ("gsl_sf"));
DEFUN_DLD(legendre_sphPlm_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} legendre_sphPlm_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {@var{z} =} legendre_sphPlm_array (@dots{})\n\
\n\
Computes an array of normalized associated Legendre functions\n\
@iftex\n\
@tex\n\
$\\\\sqrt{(2l+1)/(4\\\\pi)} \\\\sqrt{(l-m)!/(l+m)!} P_l^m(x)$\n\
for $m >= 0, l = |m|, ..., lmax, |x| <= 1.0$\n\
@end tex\n\
@end iftex\n\
@ifinfo\n\
sqrt((2l+1)/(4*pi)) * sqrt((l-m)!/(l+m)!) Plm (x)\n\
for m >= 0, l = |m|, ..., lmax, |x| <= 1.0\n\
@end ifinfo\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
\n\
@strong{DEPRECATION WARNING}: gsl_sf_legendre_sphPlm_array has been deprecated in GSL 2.0\n\
\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_sphPlm_array (arg1, arg2, arg3, y1.fortran_vec ());

  return octave_value (y1);

#else // HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY undefined

  error ("GSL function gsl_sf_legendre_sphPlm_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_SPHPLM_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_sphPlm_deriv_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_sphPlm_deriv_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_sphPlm_deriv_array (@var{arg1}, @var{arg2}, @var{arg3})\n\
@deftypefnx {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_sphPlm_deriv_array (@dots{})\n\
\n\
Computes an array of normalized associated Legendre functions\n\
@iftex\n\
@tex\n\
$\\\\sqrt{(2l+1)/(4\\\\pi)} \\\\sqrt{(l-m)!/(l+m)!} P_l^m(x)$\n\
for $m \\geq 0, l = |m|, ..., lmax, |x| <= 1.0$\n\
@end tex\n\
@end iftex\n\
@ifinfo\n\
sqrt((2l+1)/(4*pi)) * sqrt((l-m)!/(l+m)!) Plm (x)\n\
for m >= 0, l = |m|, ..., lmax, |x| <= 1.0\n\
@end ifinfo\n\
and their derivatives.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
\n\
@strong{DEPRECATION WARNING}: gsl_sf_legendre_sphPlm_deriv_array has been deprecated in GSL 2.0\n\
\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_SPHPLM_DERIV_ARRAY

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 3;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get the value of input argument #1
  double arg1_dbl = args(0).scalar_value ();
  if (arg1_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #1 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg1_dbl < 0)
    {
      error ("Input argument #1 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg1 = static_cast<int> (arg1_dbl);
  if ((static_cast<double> (arg1)) != arg1_dbl)
    {
      error ("Input argument #1 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<int>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type int: %lu.", std::numeric_limits<int>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  int arg2 = static_cast<int> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (arg1+1);
  RowVector y2 (arg1+1);

  // Run the calculation
  gsl_sf_legendre_sphPlm_deriv_array (arg1, arg2, arg3, y1.fortran_vec (), y2.fortran_vec ());

  octave_value_list retval;
  retval(0) = octave_value (y1);
  retval(1) = octave_value (y2);
  return retval;

#else // HAVE_GSL_SF_LEGENDRE_SPHPLM_DERIV_ARRAY undefined

  error ("GSL function gsl_sf_legendre_sphPlm_deriv_array was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_SPHPLM_DERIV_ARRAY
}


// PKG_ADD: autoload ("gsl_sf_legendre_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_legendre_array (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {@var{z} =} gsl_sf_legendre_array (@dots{})\n\
\n\
Calculate all normalized associated Legendre polynomials for 0 <= l <=\n\
lmax and 0 <= m <= l for |x| <= 1. The norm parameter specifies which\n\
normalization is used. The array index of P_l^m is given by\n\
l(l+1)/2+m. To include or exclude the Condon-Shortley phase factor of\n\
(-1)^m, set the fourth parameter to either -1 or 1, respectively.\n\
\n\
\n\
The argument @var{arg1} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_SF_LEGENDRE_SCHMIDT\n\
      Computation of the Schmidt semi-normalized associated Legendre\n\
      polynomials S_l^m(x).\n\
@item 1 = GSL_SF_LEGENDRE_SPHARM\n\
      Computation of the spherical harmonic associated Legendre\n\
      polynomials Y_l^m(x).\n\
@item 2 = GSL_SF_LEGENDRE_FULL\n\
      Computation of the fully normalized associated Legendre\n\
      polynomials N_l^m(x).\n\
@item 3 = GSL_SF_LEGENDRE_NONE\n\
      Computation of the unnormalized associated Legendre polynomials\n\
      P_l^m(x).\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_ARRAY_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the Legendre normalization parameter is valid
  double legnorm_tmp = args(0).double_value ();
  gsl_sf_legendre_t legnorm;
  if (legnorm_tmp == 0.0)
    legnorm = GSL_SF_LEGENDRE_SCHMIDT;
  else if (legnorm_tmp == 1.0)
    legnorm = GSL_SF_LEGENDRE_SPHARM;
  else if (legnorm_tmp == 2.0)
    legnorm = GSL_SF_LEGENDRE_FULL;
  else if (legnorm_tmp == 3.0)
    legnorm = GSL_SF_LEGENDRE_NONE;
  else
   {
      error ("The 'legnorm' argument must be 0, 1, 2 or 3.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<size_t>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type size_t: %lu.", std::numeric_limits<size_t>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  size_t arg2 = static_cast<size_t> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Get the value of input argument #4
  double arg4 = args(3).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (gsl_sf_legendre_array_n (arg2));

  // Run the calculation
  gsl_sf_legendre_array_e (legnorm, arg2, arg3, arg4, y1.fortran_vec ());

  y1.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  return octave_value (y1);

#else // HAVE_GSL_SF_LEGENDRE_ARRAY_E undefined

  error ("GSL function gsl_sf_legendre_array_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_ARRAY_E
}


// PKG_ADD: autoload ("gsl_sf_legendre_deriv_array", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_legendre_deriv_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_deriv_array (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {( z1, z2 ) =} gsl_sf_legendre_deriv_array (@dots{})\n\
\n\
Calculate all normalized associated Legendre polynomials and their\n\
first derivatives for 0 <= l <= lmax and 0 <= m <= l for |x| <= 1. The\n\
norm parameter specifies which normalization is used. The array index\n\
of P_l^m is given by l(l+1)/2+m. To include or exclude the\n\
Condon-Shortley phase factor of (-1)^m, set the fourth parameter to\n\
either -1 or 1, respectively.\n\
\n\
The argument @var{arg1} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_SF_LEGENDRE_SCHMIDT\n\
      Computation of the Schmidt semi-normalized associated Legendre\n\
      polynomials S_l^m(x).\n\
@item 1 = GSL_SF_LEGENDRE_SPHARM\n\
      Computation of the spherical harmonic associated Legendre\n\
      polynomials Y_l^m(x).\n\
@item 2 = GSL_SF_LEGENDRE_FULL\n\
      Computation of the fully normalized associated Legendre\n\
      polynomials N_l^m(x).\n\
@item 3 = GSL_SF_LEGENDRE_NONE\n\
      Computation of the unnormalized associated Legendre polynomials\n\
      P_l^m(x).\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_DERIV_ARRAY_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the Legendre normalization parameter is valid
  double legnorm_tmp = args(0).double_value ();
  gsl_sf_legendre_t legnorm;
  if (legnorm_tmp == 0.0)
    legnorm = GSL_SF_LEGENDRE_SCHMIDT;
  else if (legnorm_tmp == 1.0)
    legnorm = GSL_SF_LEGENDRE_SPHARM;
  else if (legnorm_tmp == 2.0)
    legnorm = GSL_SF_LEGENDRE_FULL;
  else if (legnorm_tmp == 3.0)
    legnorm = GSL_SF_LEGENDRE_NONE;
  else
   {
      error ("The 'legnorm' argument must be 0, 1, 2 or 3.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<size_t>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type size_t: %lu.", std::numeric_limits<size_t>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  size_t arg2 = static_cast<size_t> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Get the value of input argument #4
  double arg4 = args(3).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (gsl_sf_legendre_array_n (arg2));
  RowVector y2 (gsl_sf_legendre_array_n (arg2));

  // Run the calculation
  gsl_sf_legendre_deriv_array_e (legnorm, arg2, arg3, arg4, y1.fortran_vec (), y2.fortran_vec ());

  y1.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  y2.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  octave_value_list retval;
  retval(0) = octave_value (y1);
  retval(1) = octave_value (y2);
  return retval;

#else // HAVE_GSL_SF_LEGENDRE_DERIV_ARRAY_E undefined

  error ("GSL function gsl_sf_legendre_deriv_array_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_DERIV_ARRAY_E
}


// PKG_ADD: autoload ("legendre_deriv2_array", which ("gsl_sf"));
DEFUN_DLD(legendre_deriv2_array, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {( z1, z2, z3 ) =} legendre_deriv2_array (@var{arg1}, @var{arg2}, @var{arg3}, @var{arg4})\n\
@deftypefnx {Loadable Function} {( z1, z2, z3 ) =} legendre_deriv2_array (@dots{})\n\
\n\
Calculate all normalized associated Legendre polynomials and their\n\
first and second derivatives for 0 <= l <= lmax and 0 <= m <= l for |x| <= 1. The\n\
norm parameter specifies which normalization is used. The array index\n\
of P_l^m is given by l(l+1)/2+m. To include or exclude the\n\
Condon-Shortley phase factor of (-1)^m, set the fourth parameter to\n\
either -1 or 1, respectively.\n\
\n\
The argument @var{arg1} must be an integer corresponding to\n\
\n\
@table @asis\n\
@item 0 = GSL_SF_LEGENDRE_SCHMIDT\n\
      Computation of the Schmidt semi-normalized associated Legendre\n\
      polynomials S_l^m(x).\n\
@item 1 = GSL_SF_LEGENDRE_SPHARM\n\
      Computation of the spherical harmonic associated Legendre\n\
      polynomials Y_l^m(x).\n\
@item 2 = GSL_SF_LEGENDRE_FULL\n\
      Computation of the fully normalized associated Legendre\n\
      polynomials N_l^m(x).\n\
@item 3 = GSL_SF_LEGENDRE_NONE\n\
      Computation of the unnormalized associated Legendre polynomials\n\
      P_l^m(x).\n\
@end table\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n\
")
{
#ifdef HAVE_GSL_SF_LEGENDRE_DERIV2_ARRAY_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 4;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that all input arguments are scalar
  for (int i = 0; i < nb_args; i++)
    if (! args(i).is_scalar_type ())
      {
        error ("Input argument #%d is not scalar. "
               "All input arguments are expected to be scalar.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Check that the Legendre normalization parameter is valid
  double legnorm_tmp = args(0).double_value ();
  gsl_sf_legendre_t legnorm;
  if (legnorm_tmp == 0.0)
    legnorm = GSL_SF_LEGENDRE_SCHMIDT;
  else if (legnorm_tmp == 1.0)
    legnorm = GSL_SF_LEGENDRE_SPHARM;
  else if (legnorm_tmp == 2.0)
    legnorm = GSL_SF_LEGENDRE_FULL;
  else if (legnorm_tmp == 3.0)
    legnorm = GSL_SF_LEGENDRE_NONE;
  else
   {
      error ("The 'legnorm' argument must be 0, 1, 2 or 3.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #2
  double arg2_dbl = args(1).scalar_value ();
  if (arg2_dbl > std::numeric_limits<size_t>::max ())
    {
      error ("Input argument #2 exceeds the upper limit "
             "for type size_t: %lu.", std::numeric_limits<size_t>::max ());
      print_usage ();
      return octave_value ();
    }
  if (arg2_dbl < 0)
    {
      error ("Input argument #2 has a negative value. "
             "A non-negative value was expected.");
      print_usage ();
      return octave_value ();
    }
  size_t arg2 = static_cast<size_t> (arg2_dbl);
  if ((static_cast<double> (arg2)) != arg2_dbl)
    {
      error ("Input argument #2 has a non-integer value. "
             "An integer value was expected.");
      print_usage ();
      return octave_value ();
    }

  // Get the value of input argument #3
  double arg3 = args(2).scalar_value ();

  // Get the value of input argument #4
  double arg4 = args(3).scalar_value ();

  // Declare the array(s) where the results are stored
  RowVector y1 (gsl_sf_legendre_array_n (arg2));
  RowVector y2 (gsl_sf_legendre_array_n (arg2));
  RowVector y3 (gsl_sf_legendre_array_n (arg2));

  // Run the calculation
  gsl_sf_legendre_deriv2_array_e (legnorm, arg2, arg3, arg4, y1.fortran_vec (), y2.fortran_vec (), y3.fortran_vec ());

  y1.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  y2.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  y3.resize (gsl_sf_legendre_array_index (arg2, arg2) + 1);
  octave_value_list retval;
  retval(0) = octave_value (y1);
  retval(1) = octave_value (y2);
  retval(2) = octave_value (y3);
  return retval;

#else // HAVE_GSL_SF_LEGENDRE_DERIV2_ARRAY_E undefined

  error ("GSL function gsl_sf_legendre_deriv2_array_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_LEGENDRE_DERIV2_ARRAY_E
}


// PKG_ADD: autoload ("gsl_sf_zeta", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_zeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_zeta (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_zeta (@dots{})\n\
\n\
Computes the Riemann zeta function \\zeta(s) for\n\
arbitrary s, s \\ne 1.\n\
\n\
The Riemann zeta function is defined by the infinite sum\n\
\\zeta(s) = \\sum_@{k=1@}^\\infty k^@{-s@}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETA_E undefined

  error ("GSL function gsl_sf_zeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETA_E
}


// PKG_ADD: autoload ("gsl_zt_zeta", which ("gsl_sf"));
DEFUN_DLD(gsl_zt_zeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_zt_zeta (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_zt_zeta (@dots{})\n\
\n\
Computes the Riemann zeta function \\zeta(s) for\n\
arbitrary s, s \\ne 1.\n\
\n\
The Riemann zeta function is defined by the infinite sum\n\
\\zeta(s) = \\sum_@{k=1@}^\\infty k^@{-s@}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETA_E undefined

  error ("GSL function gsl_sf_zeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETA_E
}


// PKG_ADD: autoload ("zeta", which ("gsl_sf"));
DEFUN_DLD(zeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} zeta (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} zeta (@dots{})\n\
\n\
Computes the Riemann zeta function \\zeta(s) for\n\
arbitrary s, s \\ne 1.\n\
\n\
The Riemann zeta function is defined by the infinite sum\n\
\\zeta(s) = \\sum_@{k=1@}^\\infty k^@{-s@}.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zeta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETA_E undefined

  error ("GSL function gsl_sf_zeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETA_E
}


// PKG_ADD: autoload ("gsl_sf_zeta_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_zeta_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_zeta_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_zeta_int (@dots{})\n\
\n\
Computes the Riemann zeta function \\zeta(n) for\n\
integer n, n \\ne 1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETA_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zeta_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zeta_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETA_INT_E undefined

  error ("GSL function gsl_sf_zeta_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETA_INT_E
}


// PKG_ADD: autoload ("zeta_int", which ("gsl_sf"));
DEFUN_DLD(zeta_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} zeta_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} zeta_int (@dots{})\n\
\n\
Computes the Riemann zeta function \\zeta(n) for\n\
integer n, n \\ne 1.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETA_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zeta_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zeta_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETA_INT_E undefined

  error ("GSL function gsl_sf_zeta_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETA_INT_E
}


// PKG_ADD: autoload ("gsl_sf_zetam1", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_zetam1, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_zetam1 (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_zetam1 (@dots{})\n\
\n\
Computes \\zeta(s) - 1 for arbitrary s, s \\ne 1, where \\zeta\n\
denotes the Riemann zeta function.\n\
\n\
@seealso{gsl_sf_zeta}\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETAM1_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zetam1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_zetam1_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETAM1_E undefined

  error ("GSL function gsl_sf_zetam1_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETAM1_E
}


// PKG_ADD: autoload ("gsl_sf_zetam1_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_zetam1_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_zetam1_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_zetam1_int (@dots{})\n\
\n\
Computes \\zeta(s) - 1 for integer n, n \\ne 1, where \\zeta\n\
denotes the Riemann zeta function.\n\
\n\
@seealso{gsl_sf_zetam1, gsl_sf_zeta, gsl_sf_zeta_int}\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ZETAM1_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zetam1_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_zetam1_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ZETAM1_INT_E undefined

  error ("GSL function gsl_sf_zetam1_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ZETAM1_INT_E
}


// PKG_ADD: autoload ("gsl_sf_eta", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_eta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_eta (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_eta (@dots{})\n\
\n\
Computes the eta function \\eta(s) for arbitrary s.\n\
\n\
The eta function is defined by \\eta(s) = (1-2^@{1-s@}) \\zeta\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_eta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_eta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ETA_E undefined

  error ("GSL function gsl_sf_eta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ETA_E
}


// PKG_ADD: autoload ("eta", which ("gsl_sf"));
DEFUN_DLD(eta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} eta (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} eta (@dots{})\n\
\n\
Computes the eta function \\eta(s) for arbitrary s.\n\
\n\
The eta function is defined by \\eta(s) = (1-2^@{1-s@}) \\zeta\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_eta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          gsl_sf_eta_e (arg1_data[i], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ETA_E undefined

  error ("GSL function gsl_sf_eta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ETA_E
}


// PKG_ADD: autoload ("gsl_sf_eta_int", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_eta_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_eta_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_eta_int (@dots{})\n\
\n\
Computes the eta function \\eta(n) for integer n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ETA_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_eta_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_eta_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ETA_INT_E undefined

  error ("GSL function gsl_sf_eta_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ETA_INT_E
}


// PKG_ADD: autoload ("eta_int", which ("gsl_sf"));
DEFUN_DLD(eta_int, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} eta_int (@var{arg1})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} eta_int (@dots{})\n\
\n\
Computes the eta function \\eta(n) for integer n.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_ETA_INT_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 1;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input argument as NDArray object
  NDArray arg1 = args(0).array_value();

  // Get the dimension of the array
  dim_vector dim (arg1.dims ());
  octave_idx_type numel = dim.numel ();

  // Get a pointer to the data of the input array
  const double *arg1_data = arg1.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_eta_int_e (x1, &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++)
        {
          // Convert argument #1 to int
          double t1 = arg1_data[i];
          int x1 = static_cast<int> (t1);
          if ((static_cast<double> (x1)) != t1)
            {
              y.xelem(i) = lo_ieee_nan_value ();
              err.xelem(i) = lo_ieee_nan_value ();
              continue;
            }

          gsl_sf_eta_int_e (x1, &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_ETA_INT_E undefined

  error ("GSL function gsl_sf_eta_int_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_ETA_INT_E
}


// PKG_ADD: autoload ("gsl_sf_hzeta", which ("gsl_sf"));
DEFUN_DLD(gsl_sf_hzeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} gsl_sf_hzeta (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} gsl_sf_hzeta (@dots{})\n\
\n\
Computes the Hurwitz zeta function \\zeta(s,q)\n\
for s > 1, q > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HZETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hzeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hzeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HZETA_E undefined

  error ("GSL function gsl_sf_hzeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HZETA_E
}


// PKG_ADD: autoload ("hzeta", which ("gsl_sf"));
DEFUN_DLD(hzeta, args, nargout, "\
  -*- texinfo -*-\n\
@deftypefn {Loadable Function} {@var{z} =} hzeta (@var{arg1}, @var{arg2})\n\
@deftypefnx {Loadable Function} {[@var{z}, @var{err}] =} hzeta (@dots{})\n\
\n\
Computes the Hurwitz zeta function \\zeta(s,q)\n\
for s > 1, q > 0.\n\
\n\
@var{err} contains an estimate of the absolute error in the value @var{z}.\n\
\n\
This function is from the GNU Scientific Library,\n\
see @url{http://www.gnu.org/software/gsl/} for documentation.\n\
@end deftypefn\n")
{
#ifdef HAVE_GSL_SF_HZETA_E

  gsl_set_error_handler (octave_gsl_errorhandler);

  // Expected number of input arguments
  const int nb_args = 2;

  // Check the actual number of input arguments
  if (args.length() != nb_args)
    {
      print_usage ();
      return octave_value ();
    }

  // Check that all input arguments are real
  for (int i = 0; i < nb_args; i++)
    if (! ISREAL(args(i)))
      {
        error ("Input argument #%d has a complex value. "
               "A real value was expected.", i + 1);
        print_usage ();
        return octave_value ();
      }

  // Get input arguments as NDArray objects
  NDArray arg1 = args(0).array_value();
  NDArray arg2 = args(1).array_value();

  // Get the dimensions of the arrays
  dim_vector dim (1, 1);
  octave_idx_type numel = 1;
  bool conformant = true;
  bool arg1_scalar = check_arg_dim<NDArray> (arg1, dim, numel, conformant);
  bool arg2_scalar = check_arg_dim<NDArray> (arg2, dim, numel, conformant);

  // Error in case of non-conformant arguments
  if (! conformant)
    {
      error ("Non-scalar input arguments must all have the same size.");
      return octave_value ();
    }

  // Create one separate index for each argument
  octave_idx_type i1 = 0;
  octave_idx_type i2 = 0;

  // Create one separate increment for each argument
  octave_idx_type inc1 = (arg1_scalar ? 0 : 1);
  octave_idx_type inc2 = (arg2_scalar ? 0 : 1);

  // Get pointers to the data of the input arrays
  const double *arg1_data = arg1.data ();
  const double *arg2_data = arg2.data ();

  // Array of output values
  NDArray y (dim);

  // GSL structure that will hold each individual result
  gsl_sf_result result;

  if (nargout < 2)
    {
      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hzeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
        }

      return octave_value (y);
    }
  else
    {
      // Array of error estimates
      NDArray err (dim);

      // Run the calculation
      for (octave_idx_type i = 0; i < numel; i++, i1 += inc1, i2 += inc2)
        {
          gsl_sf_hzeta_e (arg1_data[i1], arg2_data[i2], &result);
          y.xelem(i) = result.val;
          err.xelem(i) = result.err;
        }

      octave_value_list retval (2);
      retval(0) = octave_value (y);
      retval(1) = octave_value (err);
      return retval;
    }

#else // HAVE_GSL_SF_HZETA_E undefined

  error ("GSL function gsl_sf_hzeta_e was found missing when "
         "the gsl package for octave was built.");
  return octave_value ();

#endif // HAVE_GSL_SF_HZETA_E
}
